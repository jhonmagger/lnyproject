

<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-settings"></i>
					<span class="caption-subject bold uppercase">Charge / Commision</span>
				</div>
			</div>
			<div class="portlet-body form">
				<form class="form-horizontal" action="<?php echo e(url('admin/charges')); ?>/<?php echo e($charges->id); ?>" method="POST" role="form">
					<?php echo e(csrf_field()); ?>

					<?php echo e(method_field('put')); ?>


					<div class="row">
						<div class="col-md-4 col-md-offset-1">
							<h5>Money Transfer Charge(Fixed)</h5>
							<div class="form-body">
								<div class="form-group">
									
									<div class="input-group mb15">
										<input class="form-control input-lg" name="trancharge" value="<?php echo e($charges->trancharge); ?>" type="text">
										<span class="input-group-addon"><?php echo e($gset->curSymbol); ?></span>
									</div>
								</div>
							</div>                    

						</div>

						<div class="col-md-4 col-md-offset-1">
								<h5>Money Transfer Charge (Percentage) </h5>
							<div class="form-body">
								<div class="form-group">
									<div class="input-group mb15">
										<input class="form-control input-lg" name="trncrgp" value="<?php echo e($charges->trncrgp); ?>" type="text">
										<span class="input-group-addon">%</span>
									</div>
								</div>  
							</div>
						</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 col-md-offset-1">
							<h5>Base Price</h5>
							<div class="form-body">
								<div class="form-group">
									
									<div class="input-group mb15">
										<input class="form-control input-lg" name="basep" value="<?php echo e($charges->basep); ?>" type="text">
										<span class="input-group-addon">$</span>
									</div>
								</div>
							</div>                    

						</div>

						<div class="col-md-4 col-md-offset-1">
								<h5>Variable Price</h5>
							<div class="form-body">
								<div class="form-group">
									<div class="input-group mb15">
										<input class="form-control input-lg" name="varp" value="<?php echo e($charges->varp); ?>" type="text">
										<span class="input-group-addon">%</span>
									</div>
								</div>  
							</div>
						</div>
						</div>
						<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<h5>Covertion Charge</h5>
							<div class="form-body">
								<div class="form-group">									
									<div class="input-group mb15">
										<input class="form-control input-lg" name="convcrg" value="<?php echo e($charges->convcrg); ?>" type="text">
										<span class="input-group-addon">%</span>
									</div>
								</div>
							</div>                    

						</div>
						</div>

			
	<div class="row">
						<hr/>
						<div class="col-md-4 col-md-offset-4">
							<button class="btn btn-lg btn-success btn-block">Update</button>
						</div>
					</div>
				</form>
			</div>
	</div>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>