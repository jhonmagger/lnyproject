

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-list font-blue"></i>
                        <span class="caption-subject font-green bold uppercase">Contac Information</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                    <form role="form" method="POST" action="<?php echo e(route('contac.update',$contac->id)); ?>" enctype="multipart/form-data">
                            <?php echo e(csrf_field()); ?>

                            <?php echo e(method_field('put')); ?>

                            <div class="form-body">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><h3>Email</h3></label>
                                            <input type="email" class="form-control input-lg" value="<?php echo e($contac->email); ?>" name="email" >
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><h3>Mobile</h3></label>
                                            <input type="text" class="form-control input-lg" value="<?php echo e($contac->mobile); ?>" name="mobile" >
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><h3>Location</h3></label>
                                           <textarea name="location" class="form-control" rows="10">
                                                <?php echo e($contac-> location); ?>

                                            </textarea>
                                        </div>
                                    </div>
                            </div>
                            <div class="form-actions right">
                                <button type="submit" class="btn blue btn-lg">Update</button>
                            </div>
                        </form>
                     </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>