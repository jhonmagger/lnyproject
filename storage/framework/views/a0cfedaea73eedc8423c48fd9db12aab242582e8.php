
<?php $__env->startSection('content'); ?>



<div class="panel-lny-desk w50">
  <div class="col-md-12">
    
            <div style="text-align: center">
                <img src="https://leigonphy.co/assets/web/assets/images/newavatar.png" style="width: 180px;">
                <h3 class="text-bold text-center"><?php echo e($user->firstname); ?> <?php echo e($user->lastname); ?></h3><h5><?php echo e($user->username); ?></h5>
          </div>
  </div>
  <div class="row">
    <div class="col-md-6">

    <div class="block-lny">
      <div class="block-title-lny"><h4>Change Password</h4></div>
      <div class="block-info-lny">

<form method="POST" action="<?php echo e(route('changep')); ?>">
        <?php echo e(csrf_field()); ?>

              
        <div class="row">
          <div class="col-md-12">
            <div class="form-group <?php echo e($errors->has('passwordold') ? ' has-error' : ''); ?>">
              <label for="password" class="cols-sm-2">Old Password</label>
                  <input type="password" class="form-control input-sz" name="passwordold" id="passwordold" required />
                  <?php if($errors->has('passwordold')): ?>
                  <span class="help-block">
                    <strong><?php echo e($errors->first('passwordold')); ?></strong>
                  </span>
                  <?php endif; ?>
            </div>

            <div class="form-group <?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
              <label for="password" class="cols-sm-2">New Password</label>              
                  <input type="password" class="form-control input-sz" name="password" id="password" required />
                  <?php if($errors->has('password')): ?>
                  <span class="help-block">
                    <strong><?php echo e($errors->first('password')); ?></strong>
                  </span>
                  <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('password_confirmation') ? ' has-error' : ''); ?>">
               <label for="password-confirm" class="cols-sm-2">Confirm Password</label>    
                <input id="password-confirm" type="password" class="form-control input-sz" name="password_confirmation" required>
                     <?php if($errors->has('password')): ?>
                     <span class="help-block">
                       <strong><?php echo e($errors->first('password')); ?></strong>
                   </span>
                   <?php endif; ?>
           </div>
              <div class="form-group ">
                <button type="submit" class="btn btn-primary btn-block">Change Password</button>
              </div>
          </div>

        </div>
      </form>


      </div>
    </div>
</div>

    <div class="col-md-6">

    <div class="block-lny">
      <div class="block-title-lny"><h4>Profile Information</h4></div>
      <div class="block-info-lny" style="height: 292px;">
            <table class="table table-responsive table-striped">
              <tr class="text-center">
                <td colspan="2"><?php echo e($user->firstname); ?> <?php echo e($user->lastname); ?></td>
              </tr>
              <tr>
                <td>Email</td>
                <td><?php echo e($user->email); ?></td>
              </tr>
               <tr>
                <td>Username</td>
                <td><?php echo e($user->username); ?></td>
              </tr>
               <tr>
                <td>Phone</td>
                <td><?php echo e($user->mobile); ?></td>
              </tr>
              <tr>
                <td>City</td>
                <td><?php echo e($user->city); ?></td>
              </tr>
            </table>
      </div>
    </div>
  </div>
</div>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.layouts.admaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>