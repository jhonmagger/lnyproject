<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-list font-green"></i>
                        <span class="caption-subject font-green bold uppercase">ICO Agenda</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle  btn-primary"  data-toggle="modal" data-target="#addprice">
                           <i class="icon-plus"></i> ADD NEW
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Ico Agenda Name</th>
                                <th> Start Date </th>
                                <th> Start Time </th>
                                <th> Last Date </th>
                                <th> Last Time </th>
                                <th>Total EXA</th>
                                <th> Price (USD) </th>
                                <th> Limit </th>
                               
                                <th> Status </th>
                                <!--<th>Edit</th>-->
                                <th>Delete</th>
                                
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $icoagenda; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $icoagendas): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($icoagendas->round_name); ?> </td>
                                <td><?php echo e($icoagendas->start_date); ?> </td>
                                
                                <td><?php echo e($icoagendas->start_time); ?>  </td>
                                <td><?php echo e($icoagendas->last_date); ?>  </td>
                                 <td><?php echo e($icoagendas->last_time); ?>  </td>
                                <td><?php echo e($icoagendas->total_exa); ?>  </td>
                                <td><?php echo e($icoagendas->price); ?>  </td>
                                <td><?php echo e($icoagendas->limit); ?> tetra/occ  </td>
                                
                                <?php if($icoagendas->status==1){?>
                                 <td>Active</td>
                                   <?php } else{?>
                                <td>Inactive</td>
                                <?php } ;?>
                                
                              <!---<td>
                                <a class="btn btn-circle btn-icon-only btn-suceess"  href="<?php echo e(route('icoagenda.edit', $icoagendas)); ?>">
                                        <i class="fa fa-edit"></i>
                                    </a>
                               </td>--->
                                
                                 <td>
                                <a class="btn btn-circle btn-icon-only btn-danger"  href="<?php echo e(route('icoagenda.destroy', $icoagendas)); ?>" data-toggle="confirmation"  data-title="Are You Sure?" data-content="Delete This Price?">
                                        <i class="fa fa-trash"></i>
                                    </a>
                               </td>
                                
                            </tr>
                
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                         <?php echo $icoagenda->render(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
                <div id="addprice" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Add New ICO Agenda</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form" method="POST" action="<?php echo e(route('icoagenda.store')); ?>" enctype="multipart/form-data">
                                     <?php echo e(csrf_field()); ?>

                                    
                                    <div class="form-group">
                                            <label for="price">Ico Agenda Name</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="ico_name" name="ico_name" style="width:522px;" >
                                                
                                            </div>
                                            
                                        </div>
                                         <div class="form-group">
                                        <label for="price">Start_Date</label>
                                             
                                             <div class="input-group">
                            	  <input type="text" class="form-control  date-picker" readonly name="start_dates" value="">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                 </div>
                                              </div>
                                    
                                    <div class="form-group">
                                            <label for="price">Start_Time</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="time" name="start_times" >
                                                <span class="input-group-addon">Time</span>
                                            </div>
                                            
                                        </div>
                                    
                                   
                                    
                                    <div class="form-group">
                                        <label for="price">Last_Date</label>
                                             
                                             <div class="input-group">
                            	  <input type="text" class="form-control  date-picker" readonly name="last_dates" value="">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                 </div>
                                              </div>
                                    
                                    <div class="form-group">
                                            <label for="price">Last_Time</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="time" name="last_times" >
                                                <span class="input-group-addon">Time</span>
                                            </div>
                                            
                                        </div>
                                    
                                    
                                    
                                    
                                    <div class="form-group">
                                            <label for="price">Total Tetra</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="total_tetra" name="total_tetra" >
                                                <span class="input-group-addon">TETRA</span>
                                            </div>
                                            
                                        </div>
                                    
                                    <div class="form-group">
                                            <label for="price">Price(USD)</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="price" name="price" >
                                                <span class="input-group-addon">USD</span>
                                            </div>
                                            
                                        </div>
                                    
                                    <div class="form-group">
                                            <label for="price">Limit</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="limit" name="limit" >
                                                <span class="input-group-addon">TETRA/OCC</span>
                                            </div>
                                            
                                        </div>
                                    
                            
                                    
                                    
                                    
                                    <div class="form-group">
					<label for="status">Status</label>
					<select class="form-control" name="status">
						<option value="1">Active</option>
						
					</select>

				</div>
                                    
                                    
                                    
                                    
                                    
                                    
                                        <div class="form-group">
                                        <button type="submit" class="btn btn-lg btn-success btn-block" >Save</button>
                                    </div>
                                </form>                                   
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>