

<?php $__env->startSection('content'); ?>

<div class="row">
	<div class="col-md-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-red-sunglo">
					<i class="icon-settings font-red-sunglo"></i>
					<span class="caption-subject bold uppercase">General Settings</span>
				</div>
			</div>
			<div class="portlet-body form">
				<form role="form" method="POST" action="<?php echo e(url('admin/gsettings')); ?>/<?php echo e($gsettings->id); ?>">
					<?php echo e(csrf_field()); ?>

					<?php echo e(method_field('put')); ?>

					<div class="row">
						<div class="col-md-4">
							<h4>Website Title</h4>
							<input type="text" class="form-control input-lg" value="<?php echo e($gsettings->webTitle); ?>" name="webTitle" >
						</div>
						<div class="col-md-4">
							<h4>Website Sub-Title</h4>
							<input type="text" class="form-control input-lg" value="<?php echo e($gsettings->subtitle); ?>" name="subtitle" >
						</div>
						<div class="col-md-3">
							<h4>Website Start Date</h4>
                            <div class="input-group">
                            	<input type="text" class="form-control form-control-inline input-medium date-picker" readonly name="startdate" value="<?php echo e($gsettings->startdate); ?>">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                           
						</div>
					</div>
					<div class="row">
						<hr/>
						<div class="col-md-4">
							<h4>BASE COLOR CODE</h4>
							<input type="text" class="form-control input-lg "  value="<?php echo e($gsettings->colorCode); ?>" name="colorCode"  >
						</div>

						<div class="col-md-4">
							<h4>BASE CURRENCY TEXT</h4>
							<input type="text" class="form-control input-lg" value="<?php echo e($gsettings->curCode); ?>" name="curCode" >
						</div>
						<div class="col-md-4">
							<h4>BASE CURRENCY SYMBOL</h4>
							<input type="text" class="form-control input-lg" value="<?php echo e($gsettings->curSymbol); ?>" name="curSymbol" >
						</div>
					</div>

					<hr/>
					<div class="row">
						<hr/>
						<div class="col-md-4">
							<h4>Registration</h4>
							<input data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-width="100%" type="checkbox" value="1" name="registration" <?php echo e($gsettings->registration == "1" ? 'checked' : ''); ?>>
						</div>

						<div class="col-md-4">
							<h4>EMAIL VERIFICATION</h4>
							<input data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-width="100%" type="checkbox" value="1" name="emailVerify" <?php echo e($gsettings->emailVerify == "0" ? 'checked' : ''); ?>>
						</div>
						<div class="col-md-4">
							<h4>SMS VERIFICATION</h4>
							<input data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-width="100%" type="checkbox" value="1" name="smsVerify"  <?php echo e($gsettings->smsVerify == "0" ? 'checked' : ''); ?>>
						</div>
					</div>


					<div class="row">
						<hr/>
						<div class="col-md-4">
							<h4>DECIMAL AFTER POINT</h4>
							<input type="number" value="<?php echo e($gsettings->decimalPoint); ?>" name="decimalPoint" class="form-control input-lg" >
						</div>

						<div class="col-md-4">
							<h4>EMAIL NOTIFICATION</h4>
							<input data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-width="100%" type="checkbox" value="1" name="emailNotify"  <?php echo e($gsettings->emailNotify == "1" ? 'checked' : ''); ?>>
						</div>
						<div class="col-md-4">
							<h4>SMS NOTIFICATION</h4>
							<input data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-width="100%" type="checkbox" value="1" name="smsNotify" <?php echo e($gsettings->smsNotify == "1" ? 'checked' : ''); ?>>
						</div>

                </div>
            </div>
					</div>
					<div class="row">
						<hr/>
						<div class="col-md-4 col-md-offset-4">
							<button class="btn blue btn-block btn-lg">Update</button>
						</div>
					</div>
			</form>
		</div>
	</div>
</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>