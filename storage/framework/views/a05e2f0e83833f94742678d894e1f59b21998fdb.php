<div class="page-container">
            <!-- BEGIN SIDEBAR -->
<?php echo $__env->make('admin.layouts.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
<?php echo $__env->make('admin.layouts.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('admin.layouts.error', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- BEGIN PAGE HEADER-->
 <?php echo $__env->yieldContent('content'); ?>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
           
            <!-- END QUICK SIDEBAR -->
        </div>