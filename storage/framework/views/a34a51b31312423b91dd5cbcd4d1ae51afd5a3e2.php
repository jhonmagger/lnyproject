<!DOCTYPE html>
<html lang="">

<?php echo $__env->make('front.layouts.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<body >

<?php echo $__env->make('front.layouts.preloader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">

<?php echo $__env->make('front.layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make('front.layouts.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make('front.layouts.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div id="content" class="content">
<?php echo $__env->yieldContent('content'); ?>
</div>
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>
<?php echo $__env->yieldContent('scripts'); ?>

<!--jquery script load-->
<?php echo $__env->make('front.layouts.scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
   
</body>
</html>
