

<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Banned User List</span>
                </div>

            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover order-column">
                <thead>
                    <tr>
                        <th>
                            Name 
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Username
                        </th>
                        <th>
                             Phone
                        </th>
                       	<th>
                       		Balance
                       	</th>                       	
                        <th>
                            Details
                        </th>
                  	 </tr>
                </thead>
                <tbody>
		 	<?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <tr>
                     	<td>
                        	<?php echo e($user->firstname); ?> <?php echo e($user->lastname); ?>

                        </td>
                        <td>
                            <?php echo e($user->email); ?>      
                        </td> 
                        <td>
                            <?php echo e($user->username); ?>      
                        </td>
                        <td>
                            <?php echo e($user->mobile); ?>

                        </td>
                        <td>
                        	<?php echo e(number_format(floatval($user->balance), $gset->decimalPoint, '.', '')); ?> <?php echo e($gset-> curSymbol); ?>

                        </td>
                        <td>
                        	<a href="<?php echo e(route('user.single', $user->id)); ?>" class="btn btn-outline btn-circle btn-sm green">
                             <i class="fa fa-eye"></i> View </a>
                        </td>
                     </tr>
 			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
 			<tbody>
           </table>
            <?php echo $users->render(); ?>
        </div>
			
			</div><!-- row -->
			</div>
		</div>
	</div>		
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>