
<?php $__env->startSection('content'); ?> 
<div class="panel-lny-desk w80">
	<style>
	.loader {
		border: 5px solid #f3f3f3;
		border-radius: 50%;
		border-top: 5px solid #3498db;
		width: 30px;
		height: 30px;
		-webkit-animation: spin 2s linear infinite; /* Safari */
		animation: spin 2s linear infinite;
		margin: 0px auto;
	}

	/* Safari */
	@-webkit-keyframes spin {
		0% { -webkit-transform: rotate(0deg); }
		100% { -webkit-transform: rotate(360deg); }
	}

	@keyframes  spin {
		0% { transform: rotate(0deg); }
		100% { transform: rotate(360deg); }
	}
	.mainbutton{
		color: #fff;
		background: #348fe2;
		border-color: #348fe2;
		border-radius: 4px;
		padding: 4px 5px;  
		display: none;        
		position: absolute;
    width: 44%;
    z-index: 999;
    height: 38px;
	}
	.no-padding {
    padding: 0px 10px 0px 10px !important;
}
    .stats-info h4{
    font-size: 10pt;
    text-align: center;
    color: #bbb;
    margin-top: 0px;
    padding-top: 10px;
    font-family: 'Francois One', sans-serif;
}
}
</style>
<div class="row ">
	<div class="col-md-6 col-sm-6">
		<div class="block-lny">
			<div class="block-title-lny bgtimer" id="cointer" style="height: 80px">
				<div id="countdown">
					<div id="demo"></div>
					<div class="labels">
						<li>Days</li>
						<li>Hours</li>
						<li>Mins</li>
						<li>Secs</li>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6">
		<div class="block-lny balances" style="height: 80px">
			<img src="<?php echo e(asset('assets/images/coin/Lny-80x80.png')); ?>">
			<ul class="statslist">
				<li class="stats-info">
					<h4 style="">TOTAL <?php echo e($gset->curCode); ?></h4>
					<p id="totaltetra" style=" font-size: 23px;color: #348fe2;margin: 0px;"><?php echo $icoblance;?></p>
				</li>
				<li class="stats-info" style="border-left: 1px solid; border-right: 1px solid">
					<h4 style="">AVAILABLE <?php echo e($gset->curCode); ?></h4>
					<p  id="avltetra" style=" font-size: 23px;color: #348fe2;margin: 0px;"><?php echo $avilable;?></p>
				</li>
				<li class="stats-info">
					<h4 style="">SOLD <?php echo e($gset->curCode); ?></h4>
					<p id="soldtetra" style=" font-size: 23px;color: #348fe2; margin: 0px;"><?php echo $soldtetra;?></p>
				</li>
			</ul>
		</div>
	</div>

</div>

<div class="row">
	<div class="col-md-6">
		<div class="block-lny" >
			<div class="block-title-lny"><h4>Buy <?php echo e($gset->curCode); ?></h4></div>
			<div class="block-info-lny"  style="height: 310px">

				<form role="form" method="post" action="<?php echo e(route('coinbuy.confirm')); ?>" >
					<?php echo e(csrf_field()); ?>

					<div class="form-group">
						<div class="input-group fieldslny">
							<input type="hidden" id="balance" name="balance" value="">
							<input type="hidden" id="icoagenda" name="icoagenda" value="">
							<input type="number" class="form-control" id="amountcoin" name="amount" value="" required autocomplete="off">
							<span class="input-group-addon"><?php echo e($gset->curSymbol); ?></span>
						</div>
						<div class="input-group fieldslny">
							<label type="number" style="" class="form-control" id="valuedata" ></label>
							<span class="input-group-addon">BTC</span>
						</div>
					</div>
					<div id="maxcoinss" style="color:red;"></div>
					<div id="maxcoins"style="color:red;"></div>
					<!--- <p class="alert alert-danger warnMinMax" style="display: none;">Your Per day purcahse limit is <span id="perday_user_bals"><?php echo $perday_user_bal;?></span>.Min Balance  1 Tera is requried (Your Day Limit i over)</p>-->

					<p class="alert alert-danger warnMinMax" style="display: none;">    The amount is higher than available <?php echo e($gset->curCode); ?> And (Minimum 1 <?php echo e($gset->curCode); ?> required to purchase coin)</p>

					<span class="msg-error error"></span>
					<div id="recaptcha" class="g-recaptcha" data-sitekey="6Leu00sUAAAAAKBPo4F4qae7Yj7imLb0HTyrgF9p"></div>
					<br/>

					<div class="mainbutton" id="loderbutton"><div class="loader"></div></div> 
					<button type="submit" class="btn btn-lg btn-primary btn-block" id="nextbutton" disabled style="display: inline-block;
					width: 48%;
					float: left;">Buy Coin</button>

				</form>

				<button type="button" class="btn btn-primary btn-lg btn-block" style="display: inline-block;
				width: 48%;
				float: right;" id="buyP">Buy All</button>

				<div id="purchase_ico" style="visibility:hidden;"><?php echo $hiddenbtcblance;?></div>
				<div id="total_limit_agenda" style="visibility:hidden;"><?php echo $totals;?></div>
				<div id="agendaid" style="visibility:hidden;"><?php echo $ids;?></div>
				<div id="remaining_ico_agenda" style="visibility:hidden;"></div>
				<div id="perday_limit" style="visibility:hidden;"><?php echo $limit;?></div>
				<div id="roundname" style="visibility:hidden;"><?php echo $ico_name;?></div>
				<div id="perday_user_bal" style="visibility:hidden;"><?php echo $perday_user_bal;?></div>
				<div id="statuscomplted" style="visibility:hidden;"><?php echo $statuscomplted;?></div>
				<?php if(isset($maindstart_date_time)){ ?>
				<!-- <div id="start_date_time" style="visibility:hidden;"><?php echo $maindstart_date_time;?></div> -->
				<div id="start_date_time" style="visibility:hidden;">Jul 20, 2018 4:00:00</div>
				<div id="last_date_time" style="visibility:hidden;"><?php echo $maindlast_date_time;?></div>
				<div id="current_date_time" style="visibility:hidden;"><?php echo $maincurrentdate;?></div>
				<?php }else{ ?>
				<!-- <div id="start_date_time" style="visibility:hidden;"><?php ?></div> -->
				<div id="start_date_time" style="visibility:hidden;">Jul 20, 2018 4:00:00</div>
				<div id="last_date_time" style="visibility:hidden;"><?php ?></div>
				<div id="current_date_time" style="visibility:hidden;"><?php ?></div>
				<?php } ?>
      			<div id="coinvalue" style="visibility:hidden;"><?php echo $totaltetracoins;?></div>



			</div>
		</div>
	</div>




	<div class="col-md-6" >
		<div class="block-lny" >
			<div class="block-title-lny"><h4>Your Account Balance Info</h4></div>
			<div class="block-info-lny"  style="height: 310px">
				<div class="well lny-color">
					<div class="tablecell">My <?php echo e($gset->curCode); ?> Balance</div>
					<div class="tablecell text-right"> <?php echo $coinblances;?> </div>
				</div>
				<div class="well btc-color">
					<div class="tablecell">My Bitcoin Balance</div>
					<div class="tablecell text-right"><?php echo $mainbtcbal;?>&nbsp;(&nbsp; $<?php echo round($mainbtcbal*$currentRate,2);?>&nbsp; )</div>
				</div>
				<div class="well ref-color">
					<div class="tablecell">Your Buy Limit</div>
					<div class="tablecell text-right"><?php echo $perday_user_bal;?></div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row" >
	<div class="col-md-6">
		<div class="block-lny" style="height: 350px; border-left: 1px solid;overflow-y: scroll;">
			<div class="block-title-lny"><h4>User Transactions Log</h4></div>
			<table class="table table-responsive table-striped"  >
				<thead>
					<tr>
						<th>#</th>
						<th>AMOUNT</th>
						<th>BTC AMOUNT</th>
						<th>STATUS</th>
						<th>HASH</th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<?php
						$i=1;
						$aux = count($btc_address); 
						$newarray = array();
						foreach($btc_address as $btc_addresss){
							$newarray[$aux] = $btc_addresss;
							$aux--;
						}

						foreach(array_reverse($newarray) as $btc_addresss){

							?>

							<td><?php echo $i;?></td>
							<td><?php echo $btc_addresss->amount;?></td>
							<td><?php echo round($btc_addresss->inusd,8);?></td>
							<td>
								<?php $value= $btc_addresss->status;
								if($value ==0){
									echo "Pending";
								}else{
									echo "Confirmed";
								}
								?>
							</td>
							<td><?php echo $btc_addresss->trxid;?></td>
						</tr>
						<?php $i++; }
						?>

					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-6">
			<div class="block-lny" style="height: 350px; border-left: 1px solid;">
				<div class="block-title-lny"><h4>Global Transactions Log</h4></div>
				<table class="table table-responsive table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>USER</th>
							<th>TRANS. ID</th>
							<th>AMOUNT</th>
							<th>TIME</th>
						</tr>
					</thead>

         <tbody>
          <tr>
            <?php
            $i=1;
            $aux = count($translog); 
            $newarray = array();
            foreach($translog as $logs){
              $newarray[$aux] = $logs;
              $aux--;
            }

            foreach(array_reverse($newarray) as $logs){

              ?>

              <td><?php echo $i;?></td>
              <td>LNY<?php echo $logs->user_id; ?></td>
              <td><?php echo $logs->trxid; ?></td>
              <td><?php echo $logs->amount; ?></td>
              <td><?php echo $logs->created_at; ?></td>
            </tr>
            <?php $i++; if($i > 8){break;} }
            ?>

          </tbody>
					</table>
				</div>
		</div>
	</div>
	<div class="row">
  <?php foreach($icoagendasss as $icoagendas){ ?>
  <div class="col-md-2 no-padding">
    <div class="block-lny">
      <div class="block-title-lny">
        <?php if($icoagendas->statuscomplted == 2): ?>
        <h4> <?php echo $icoagendas->round_name;?><span class="label label-danger" style="right: 25px;">completed</span></h4>
        <?php elseif($icoagendas->statuscomplted == 3): ?>
        <h4> <?php echo $icoagendas->round_name;?><span class="label label-warning" style="right: 25px;">upcoming</span></h4>
        <?php else: ?>
        <h4><?php echo $icoagendas->round_name;?><span class="label label-primary" style="right: 25px;">progress</span> </h4>
        <?php endif; ?>
      </div>
      <div class="block-info-lny zero-padding">
        <ul class="list-group text-center">
          <?php if($icoagendas->start_date == $icoagendas->last_date): ?>
          <li class="list-group-item">
            <p>
              <span>
                (<?php echo $icoagendas->start_time;?> - <?php echo $icoagendas->last_time;?>)
                UTC
              </span>
              <span class="newdate"><?php echo $icoagendas->start_date;?></span>
            </p>
          </li>
          <?php else: ?>
	          <li class="list-group-item"><p><span> (<?php echo $icoagendas->start_time;?> - <?php echo $icoagendas->last_time;?>) UTC  </span>
	            <span class="newdate"><?php echo $icoagendas->start_date;?> - <?php echo $icoagendas->last_date;?></span>
	          </p>
	        </li>
	        <?php endif; ?>
        <li class="list-group-item">
          <div><?php echo $icoagendas->total_exa;?> <span><?php echo e($gset->curCode); ?> On Sale</span></div>
          <div>$<?php echo $icoagendas->price;?> <span>&nbsp;per&nbsp;<?php echo e($gset->curCode); ?></span></div>
          <div><?php echo $icoagendas->limit;?> <span><?php echo e($gset->curCode); ?> per user</span></div>
        </li> 
        <li class="list-group-item">
         <?php if($icoagendas->statuscomplted == 2): ?>
         <a href="#"> <button type="button" class="btn btn-danger btn-lg fullwidth">closed</button></a>
         <?php elseif($icoagendas->statuscomplted == 3): ?>
         <a href="#">  <button type="button" class="btn btn-warning btn-lg fullwidth">upcoming</button></a>
         <?php else: ?>
         <a href="#">   <button type="button" class="btn btn-primary btn-lg fullwidth">purchase</button></a>
         <?php endif; ?>
       </li>
     </ul>
   </div>
 </div>
</div>

<?php  }  ?>
</div>
</div>
</div>
</div>
</div>






	<script>
		$("#nextbutton").on( 'click', function (e) {

			var $captcha = $( '#recaptcha' ),
			response = grecaptcha.getResponse();

			if (response.length === 0) {
				if( !$captcha.hasClass( "error" ) ){
					alert("please Check google capthca");
      //$('#nextbutton').prop( "disabled", true);
      e.preventDefault();

  }
} else {


	$("#nextbutton").hide(); 
	$("#loderbutton").show();  

	$( '.msg-error' ).text('');
	$captcha.removeClass( "error" );
     //alert("ok");

    //$('#nextbutton').prop( "disabled", false);
    




}
});
</script>    

<script> 
	// $(document).ready(function(){

	// 	setInterval(function(){

	// 		$.ajax({
	// 			url: "https://leigonphy.co/home/iconhistory",
	// 			success: function(res) {
	// 				document.getElementById("completedagenda").innerHTML = res;
	// 			}
	// 		});
	// 	},1500);
	// });

</script>


<script>
// Set the date we're counting down to

	var countDownDate = new Date("Jul 20, 2018 16:00:00").getTime();
// Update the count down every 1 second
var x = setInterval(function() {

	var start_date_time=   $("#start_date_time").html();
	var last_date_time=    $("#last_date_time").html();      
	var roundname = $("#roundname").html();

	// var countDownDate = new Date(start_date_time).getTime();  
	var countUpDate = new Date(last_date_time).getTime(); 
	var current_date_time =    $("#current_date_time").html();
	var status = $("#statuscomplted").html();

 //alert(diff);
    // Get todays date and time


    var now = new Date().getTime();

    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    


    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // If the count down is over, write some text 
    
    if(( status ==1) || (now > countDownDate && now < countUpDate)  ){


     document.getElementById("countdown").innerHTML =" <h5>Start " +roundname+"</h5>" ;

    	$("#availbale").show(); 
    	$("#contik").show();
    	$("#agendawhite").hide();
    	$('#buyP').prop( "disabled", false); 

    }else if(( status ==3) || (now < countDownDate && now < countUpDate) ){

    	    document.getElementById("demo").innerHTML = "<span>" + days + "</span><span>" + hours + "</span><span>"
    + minutes + "</span><span>" + seconds + "</span>";

    	$("#availbale").hide(); 
    	$("#contik").hide();
    	$("#agendawhite").show();
    	$('#nextbutton').prop( "disabled", true);
    	$('#buyP').prop( "disabled", true);   
    }else {

   document.getElementById("countdown").innerHTML = "<h5>There is no agenda</h5>";
       //alert("else");
       $("#availbale").hide(); 
       $("#contik").hide();
       $("#agendawhite").show();
       $('#nextbutton').prop( "disabled", true);
       $('#buyP').prop( "disabled", true);
   }
   
   
}, 1000);

$("#buyP").click(function(){

	onclickico();

});

$("#amountcoin").keyup(function() { 
	keypressico();
});

function onclickico(){


	var coin = $("#coinvalue").html();
	var coin_amt = parseInt(coin);
	var purchase_ico = $("#purchase_ico").html();
	var total_limit_agenda = $("#total_limit_agenda").html();
	var agenda_id =$("#agendaid").html();
	var perday_limit =$("#perday_limit").html();
	var perday_user_bal1 =$("#perday_user_bal").html();    
	var roundname =$("#roundname").html();
	var icopriceajax = $("#icopriceajax").html();
	var btcpriceajax = $("#btcpriceajax").html();
	var cal = parseFloat(icopriceajax/btcpriceajax);
	var bcoin = parseFloat(perday_user_bal1*cal);
	var n = bcoin.toFixed(8); 

	$("#valuedata").html(n);
	$("#balance").val(bcoin);
	$("#icoagenda").val(agenda_id); 
	var avabiletetra1 = $("#avltetra").html();    

	var avabiletetra = parseInt(avabiletetra1);
	var perday_user_bal = parseInt(perday_user_bal1);

	if(perday_user_bal > 0 && avabiletetra > 0 && coin_amt > 0 && avabiletetra  > perday_user_bal){


		$('#nextbutton').prop("disabled", false);
		$('.warnMinMax').hide();
		$("#amountcoin").attr("value", perday_user_bal1);   

	}else if(perday_user_bal > 0 && avabiletetra > 0 && coin_amt > 0 &&  avabiletetra  < perday_user_bal){



		$('#nextbutton').prop("disabled", false);
		$('.warnMinMax').hide();
		$("#amountcoin").attr("value", avabiletetra1);  

	}else{




		if(perday_user_bal1 == 0){
			$('.warnMinMax').show(); 
			$('#nextbutton').prop("disabled", true);
			$("#amountcoin").attr("value", perday_user_bal1);    

		}else{
			$('.warnMinMax').hide(); 
			$('#nextbutton').prop("disabled", false);
			$("#amountcoin").attr("value", perday_user_bal1); 
		}

	}  





}

function keypressico(){
	var amt = $("#amountcoin").val();
	var amt_ico = parseInt(amt);
	var coin = $("#coinvalue").html();
	var coin_amt = parseInt(coin);
	var purchase_ico = $("#purchase_ico").html();
	var total_limit_agenda = $("#total_limit_agenda").html();
	var agenda_id =$("#agendaid").html();
	var perday_limit =$("#perday_limit").html();
	var perday_user_bal =$("#perday_user_bal").html();    
	var roundname =$("#roundname").html();
	var icopriceajax = $("#icopriceajax").html();
	var btcpriceajax = $("#btcpriceajax").html();
	var cal = parseFloat(icopriceajax/btcpriceajax);
	var bcoin = parseFloat(amt_ico*cal);
	var n = bcoin.toFixed(8); 

	$("#valuedata").html(n);

	$("#balance").val(bcoin);
	$("#icoagenda").val(agenda_id); 
	var avabiletetra = $("#avltetra").html();    
	$('#nextbutton').prop("disabled", true);  
	var start_date_time=   $("#start_date_time").html();  
	var countDownDate = new Date(start_date_time).getTime();   
	var current_date_time =    $("#current_date_time").html(); 
	var now = new Date(current_date_time).getTime();
	if(amt_ico > 0 && amt_ico <= perday_user_bal && avabiletetra  > 0 && coin_amt > 0 && avabiletetra  >= amt_ico && now>=countDownDate){

		$('#nextbutton').prop("disabled", false);
		$('.warnMinMax').hide();
		$("#amountcoin").attr("value", amt_ico);
	}else{

		$('.warnMinMax').show();
		$('#nextbutton').prop("disabled", true);

	}

}



</script>



<style>
div#captcha_container {
	margin-top: 18px;
	margin-bottom: 25px;
	margin-left: 4px;
}
msg-error {
	color: #c65848;
}
.g-recaptcha.error {
	border: solid 2px #c64848;
	padding: .2em;
	width: 19em;
}
</style>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.admaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>