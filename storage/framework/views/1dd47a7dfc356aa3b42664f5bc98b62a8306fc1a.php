<?php $__env->startSection('content'); ?>
<div class="panel-lny-desk w50">


<div class="row">
    <div class="col-md-6 col-sm-6">
    <div class="block-lny balances" style="height: 80px">
      <img src="<?php echo e(asset('assets/images/coin/bitcoin-icon.png')); ?>">
      <h4>BALANCE</h4>
      <?php $mainbal = $btcblance-$coinblance;
      $mainbals  = sprintf('%f', (float)$mainbal); ?>
      <p> <?php echo $mainbals; ?> </p>  
    </div>
  </div>
    <div class="col-md-6">
    <div class="block-lny">
      <div class="block-title-lny"><h4>Actions</h4></div>
      <div class="block-info-lny">
              <button type="button" class="btn btn-warning btn-lg" data-toggle="modal" data-target="#btcaddress" style="padding:6px 10px !important;font-size:16px !important; width: 30%;   border: #fff !important;">Deposit</button>
              <button type="button" class="btn btn-warning btn-lg" data-toggle="modal" data-target="#" style="padding:6px 10px !important;font-size:16px !important; width: 30%;     opacity: 0.4;   border: #fff !important;">Withdraw</button>
      </div>
    </div>
  </div>
      </div>

        <div class="row">
     <div class="col-md-12">
    <div class="block-lny" style="height: 350px; overflow-y: unset;border-left: 1px solid;">
      <div class="block-title-lny"><h4>Coin Transaction Log</h4></div>
      <table class="table" style="margin-bottom: 0px;">
              <thead>
               <tr>
                              <th>#</th>
                        <th>ADDRESS</th>
                        <th>AMOUNT</th>
                              <th>STATUS</th>
                        <th>HASH</th>
                      </tr>
                    </thead>
              </thead>
              
  <tbody>
                      <tr>
                        <?php
                                 $i=1; 
                        
                
                                  foreach($sql as $sqls){
                                   
                                    ?>
                                  <tr>
                                  <td><?php echo $i;?></td>
                                  <td><?php echo $sqls->address;?></td>
                                  <td><?php echo $sqls->value;?></td>
                                 
                                  <td>
                                     <?php $value= $sqls->main;
                                      if($value ==0){
                                       echo "Unconfirm";
                                      }else{
                                       echo "Confirmed";
                                      }
                                        ?>
                                    </td>
                                  <td><a target='_blank' href='https://live.blockcypher.com/btc-testnet/tx/<?php echo $sqls->tx_hash;?>'><?php echo $sqls->tx_hash;?></a></td> </tr>

                                 <?php $i++; }
                                  ?>
                                               
                        <!-- <td colspan="4">No Ethereum/Autobot transaction found</td> -->
                      </tr>
                    </tbody>
             
            </div>
          </div>
        </div>

      </div>

</div>

</div>

<div id="btcaddress" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header block-title-lny">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">BTC ADDRESS</h4>
      </div>
      <div class="modal-body block-info-lny text-center">
<h2 style="font-size: 25px;">
       <?php
         
          echo $btc_address[0]->bitcoin_address;
          ?>
        </h2>
      </div>
      
    </div>

  </div>
</div>
<style>
.flet{
float: left;
    width: 24%;
    margin-left: 18%;
margin-top: 10px;
}
.right{

margin-top: 10px;
}
</style>
</div>
<?php $__env->stopSection(); ?>


      
<?php echo $__env->make('front.layouts.admaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>