

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-list font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Price Timeline</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle  btn-primary"  href="<?php echo e(route('timeline.add')); ?>">
                           <i class="icon-plus"></i> New Price Timeline
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th> Timeline Title </th>
                                <th> Timeline Details</th>
                                <th> Timeline Date </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $times; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $time): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($time->title); ?></td>
                                <td> <?php echo $time->desc; ?> </td>
                                <td> <?php echo e($time->date); ?> </td>
                                <td>
                                  <a class="btn btn-circle btn-icon-only btn-warning" href="<?php echo e(route('timeline.edit', $time->id)); ?>">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a class="btn btn-circle btn-icon-only btn-danger"  href="<?php echo e(route('timeline.destroy', $time)); ?>" data-toggle="confirmation"  data-title="Are You Sure?" data-content="Delete This Timeline?">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
                  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>