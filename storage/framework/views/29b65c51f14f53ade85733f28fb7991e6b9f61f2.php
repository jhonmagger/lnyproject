<!--footer area start-->
<footer id="contact" class="footer-area">


         <div class="footer-bottom">
             <div class="container">
                     <div class="row">
                         <div class="col-md-4 col-sm-12">
                                 <?php echo e($footer->heading); ?>                
                         </div>

                         <div class="col-md-8 text-right hidden-sm hidden-xs">
                            <?php echo $footer->text; ?>

                         </div>

                         <div class="col-sm-12 hidden-md hidden-lg">
                             <?php echo $footer->text; ?>

                         </div>




                 </div>
             </div>
         </div>
         <div id="back-to-top" class="scroll-top back-to-top" data-original-title="" title="" >
                <i class="fa fa-angle-up"></i>
            </div>
</footer>