

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-list font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Referral Level 2 Price </span>
                    </div>
                  
                </div>
 <span class="caption-subject font-green uppercase"></span>
                <div class="portlet-body">

                    <div class="table-scrollable">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                
                                <th>S.No </th>
                                <th> Price</th>
                                <th> Action </th>
                            </tr>
                            </thead>
                            <tbody>
                          
                            <?php $__currentLoopData = $leveltwo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $leveltwos): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                               
                                <td> <?php echo e($leveltwos->id); ?>  </td>
                                <td> <?php echo e($leveltwos->leveltwo); ?> </td>
                                <td>
                                    
<a href="" class="btn btn-outline btn-circle btn-sm purple" data-toggle="modal" data-target="#Modal<?php echo e($leveltwos->id); ?>">
<i class="fa fa-edit"></i> Edit </a>
                                        
                                    </a>
                                </td>
                            </tr>




 <div id="Modal<?php echo e($leveltwos->id); ?>" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Edit Price  Information</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form" method="POST" action="<?php echo e(url('admin/leveltwo')); ?>/<?php echo e($leveltwos->id); ?>" enctype="multipart/form-data">
                                     <?php echo e(csrf_field()); ?>

                                     <?php echo e(method_field('put')); ?>

                                         <div class="form-group">
                                            <label for="price">Levelone Price</label>
                                            <div class="input-group">
                                                <input type="number" class="form-control" id="leveltwo" name="leveltwo" value="<?php echo e($leveltwos->leveltwo); ?>" >
                                                <span class="input-group-addon">%</span>
                                            </div>
                                            
                                        </div>
                                        <div class="form-group">
                                        <button type="submit" class="btn btn-lg btn-success btn-block" >Update</button>
                                    </div>
                                </form>                                   
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>










                
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                         
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
               
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>