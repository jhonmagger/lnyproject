<?php $__env->startSection('content'); ?>
<style type="text/css">.form-group{display: block;}</style>
        <div id="header" class="banner banner-full d-flex align-items-center">

            <div class="container">
                <div class="banner-content no-padding">

                    <div class="row align-items-center mobile-center">
                        <div class="col-lg-12 col-md-12 ">
                            <div class="header-txt">
                                <div class="countdown-box text-center">



                <h2>Reset Password</h2>
                </div>
                  <section  class="circle-section section-padding section-background">
                    <div class="container">
                      <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6 register-lny">
                          <div class="login-admin login-admin1">
                            <div class="login-header text-center">
                              
                            </div>
                            <div class="login-form text-center input-fullwidth">
                    <form class="form-horizontal" method="POST" action="<?php echo e(route('password.request')); ?>">
                        <?php echo e(csrf_field()); ?>


                        <input type="hidden" name="token" value="<?php echo e($token); ?>">

                        <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                            <label for="email" class="col-md-12 control-label">E-Mail Address</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control input-sz" name="email" value="<?php echo e(isset($email) ? $email : old('email')); ?>" required autofocus>

                                <?php if($errors->has('email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                            <label for="password" class="col-md-12 control-label">Password</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control input-sz" name="password" required>

                                <?php if($errors->has('password')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group<?php echo e($errors->has('password_confirmation') ? ' has-error' : ''); ?>">
                            <label for="password-confirm" class="col-md-12 control-label">Confirm Password</label>
                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control input-sz" name="password_confirmation" required>

                                <?php if($errors->has('password_confirmation')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('password_confirmation')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 ">
                                <button type="submit" class="btn btn-primary">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                                         </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
</div>
                        </div><!-- .col  -->
                    </div><!-- .row  -->
                </div><!-- .banner-content  -->
            </div><!-- .container  -->
        </div>
        <!-- End Banner/Slider -->
        </div><!-- .container  -->
        </div><!-- .header-partners  -->
    </header>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>