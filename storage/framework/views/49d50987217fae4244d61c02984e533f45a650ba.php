

<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Dcument Verification Requests</span>
                </div>
                 <div class="actions">
                      <form method="POST" action="<?php echo e(route('tran.limit')); ?>" class="form-inline">
                        <?php echo e(csrf_field()); ?>

                        <?php echo e(method_field('put')); ?>

                        <div class="form-group">
                          <label>Transaction Limit for Unverified User</label>
                          <div class="input-group">
                            <input type="text" name="coin" class="form-control" value="<?php echo e($tranl->coin); ?>">
                            <span class="input-group-addon">Coin</span>
                          </div>
                          <div class="form-group">
                            <button type="submit" class="btn btn-success">Save</button>
                          </div>
                          
                        </div>
                      </form>
                  </div>

            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                      	<th>
                            User ID
                        </th>
                        <th>
                            Username
                        </th>
                        <th>
                            Document Name
                        </th>
                         <th>
                            Requested Time
                        </th>
                        <th>
                        	Action
                        </th>
                  	 </tr>
                </thead>
                <tbody>
		 	<?php $__currentLoopData = $docs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $doc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <tr>
                     	<td>
                        	<?php echo e($doc->user_id); ?>  	
                        </td>
                        <td>
                             <?php echo e($doc->user->username); ?>

                        </td> 
                        <td>
                             <?php echo e($doc->name); ?>

                        </td>
                         <td>
                             <?php echo e($doc->created_at); ?>

                        </td>
                  
                        <td>
                        	<a href="" class="btn btn-outline btn-circle btn-sm green" data-toggle="modal" data-target="#Modal<?php echo e($doc->id); ?>">
                             <i class="fa fa-eye"></i> View </a>
                        </td>

                     </tr>
 			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
 			<tbody>
           </table>
           <?php echo $docs->render(); ?>
        </div>
			
				</div><!-- row -->
			</div>
		</div>
	</div>		
</div>

<?php $__currentLoopData = $docs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $doc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <div class="modal fade" id="Modal<?php echo e($doc->id); ?>" role="dialog">
                <div class="modal-dialog">
                
                  <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Document of <?php echo e($doc->user->username); ?></h4>
          </div>
          
            <div class="modal-body">
              <table class="table-striped table table-hover">
                  <tr>
                    <td>Username:</td>
                    <td><?php echo e($doc->user->username); ?></td>
                  </tr>   
                  <tr>
                    <td>Document Name:</td>
                    <td><?php echo e($doc->name); ?></td>
                  </tr> 
                  <tr>
                    <td>Document Photo:</td>
                    <td> 
                      <img src="<?php echo e(asset('assets/images/document')); ?>/<?php echo e($doc->photo); ?>" class="img-responsive" style="max-width: 70%; padding: 5px;"> 
                    </td>
                  </tr>   
                  <tr>
                    <td>Document Details:</td>
                    <td> 
                      <?php echo $doc->details; ?>

                    </td>
                  </tr>   
              </table>


              <form role="form" method="POST" action="<?php echo e(route('document.approve', $doc->user->id)); ?>" >
                  <?php echo e(csrf_field()); ?>

                  <?php echo e(method_field('put')); ?>

              <div class="form-group">
                <label>Approve</label>
                <input data-toggle="toggle" data-onstyle="success" data-on="Approved" data-off="Not Approved" data-offstyle="danger" data-width="100%" type="checkbox" value="1" name="docv" <?php echo e($doc->user->docv == "1" ? 'checked' : ''); ?>>
              </div>
              <div class="form-group">
                <button class="btn btn-primary btn-lg btn-block" type="submit">
                  Update
                </button>
              </div>
          
          </form>
           </div>
           <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            </div>
          
        
        </div>
                       
                    
                    </div>
                </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>