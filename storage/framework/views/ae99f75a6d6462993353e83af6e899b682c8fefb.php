

<?php $__env->startSection('content'); ?>
        <div id="header" class="banner banner-full d-flex align-items-center">

            <div class="container">
                <div class="banner-content no-padding">

                    <div class="row align-items-center mobile-center">
                        <div class="col-lg-12 col-md-12 \">
                            <div class="header-txt">
                                <div class="countdown-box text-center">

                  <h2 class="section-title">Activate Your Account</h2>
                </div>



  <section  class="circle-section section-padding section-background verify">
      <div class="container">
        <div class="row">
  <div class="col-md-4"></div>
  <div class="col-md-4 register-lny text-center">
       <?php if(Auth::user()->status != '1'): ?>
                <div class="panel panel-danger text-center">
                  <div class="panel-heading">
                    <h3 style="color: #cc0000;">Your account is Deactivated</h3>
                  </div>                   
                </div>   
       <?php elseif(Auth::user()->emailv != '1'): ?>
        <div class="panel panel-primary">
          <div class="panel-heading">Please verify your Email</div>
          <div class="panel-body">
             <p>Your Email address:</p>
            <h3 class="text-wrap"><?php echo e(Auth::user()->email); ?></h3>
            <form action="<?php echo e(route('sendemailver')); ?>" method="POST">

              <?php echo e(csrf_field()); ?>

              <button type="submit" class="btn btn-sm btn-block btn-primary">Send Verification Code</button>
              <span>Click once to send you verification code, check your spam inbox.</span>
            </form>
          </div>
        </div>
        <hr>
         <div class="panel panel-primary">
          <div class="panel-heading">Verify Code</div>
          <div class="panel-body">
            <form action="<?php echo e(route('emailverify')); ?>" method="POST">
              <?php echo e(csrf_field()); ?>

              <div class="form-group fullwidth">
                                        <span class="input-group-addon"><i class="fa fa-key fa" aria-hidden="true"></i></span>
                <input type="text"  name="code" placeholder="Enter Verification Code" class="fullwidth">
              </div>
               <div class="form-group fullwidth">
                <button type="submit" class="btn btn-sm btn-block btn-success fullwidth">Verify</button>
              </div>
            </form>
          </div>
        </div>
       <?php elseif(Auth::user()->smsv != '1'): ?>
        <div class="panel panel-primary">
          <div class="panel-heading">Please verify your Mobile</div>
          <div class="panel-body">
             <p>Your Mobile no:</p>
            <h3><?php echo e(Auth::user()->mobile); ?></h3>
            <form action="<?php echo e(route('sendsmsver')); ?>" method="POST">
              <?php echo e(csrf_field()); ?>

              <button type="submit" class="btn btn-lg btn-block btn-primary">Send Verification Code</button>
            </form>
          </div>
        </div>
         <div class="panel panel-primary">
          <div class="panel-heading">Verify Code</div>
          <div class="panel-body">
            <form action="<?php echo e(route('smsverify')); ?>" method="POST">
              <?php echo e(csrf_field()); ?>

              <div class="form-group">
                <input type="text"  name="code" placeholder="Enter Verification Code">
              </div>
               <div class="form-group">
                <button type="submit" class="btn btn-block btn-lg btn-success">Verify</button>
              </div>
            </form>
          </div>
        </div>
       <?php elseif(Auth::user()->tfav != '1'): ?>
         <div class="panel panel-primary">
          <div class="panel-body">
            <form action="<?php echo e(route('go2fa.verify')); ?>" method="POST">
              <?php echo e(csrf_field()); ?>

              <div class="form-group col-md-12">
                <input type="text" name="code" style="width: 100%;" placeholder="Enter Google Authenticator Code"> 
              </div>
               <div class="form-group col-md-12">
                <button type="submit" style="padding: 0px 35px;" class="btn btn-lg btn-success btn-block">Verify</button>
              </div>
            </form>
          </div>
        </div>
          <?php endif; ?>
</div>
</div>
</div>
</section>
</div>
                        </div><!-- .col  -->
                    </div><!-- .row  -->
                </div><!-- .banner-content  -->
            </div><!-- .container  -->
        </div>
        <!-- End Banner/Slider -->
        </div><!-- .container  -->
        </div><!-- .header-partners  -->
    </header>
<?php $__env->stopSection(); ?>
         
            
         
<?php echo $__env->make('front.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>