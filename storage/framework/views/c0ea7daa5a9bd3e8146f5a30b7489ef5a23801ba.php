<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> User Transaction Log</span>
                </div>

            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover order-column">
                    <thead>
                        <tr>
                         <th>
                            User
                        </th>
                       <th>
                            Email
                        </th>
                        <th>
                            Transaction ID
                        </th>
                        <th>
                            Amount (Token)
                        </th>
                        <th>
                          Symbol
                      </th>
                      <th>
                          Amount (BTC)
                      </th>
                      <th>
                        Processed on
                    </th>                           
                    <th>
                        Date
                    </th>
                </tr>
            </thead>
            <tbody>
             <?php foreach($userlogs as $log){?>

     <tr>
                <td>
                    <a href="<?php echo e(route('user.single', $log->id)); ?>">
                      <?php echo e($log->username); ?>   
                  </a>
              </td>
              <td>
                <?php echo e($log->email); ?>      
            </td> 
            <td>
               <?php echo e($log->trxid); ?>  
            </td>
            <td>
             <?php echo e($log->amount); ?>  
         </td>
 <td>
             Token 
         </td>
         <td>
             <?php echo e($log->inusd); ?>        
        </td> 
        <td>
            <?php echo e($log->status == "1" ? 'Complete' : 'Process'); ?>

        </td>
        <td>
            <?php echo e($log->created_at); ?>

        </td>
   <?php  }?>
</tbody>
</table>
<?php //echo $userlogs->render(); ?>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>




<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>