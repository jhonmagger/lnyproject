<!DOCTYPE html>

<html lang="en">
  
<?php echo $__env->make('admin.layouts.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

<?php echo $__env->make('admin.layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
 
   
        <div class="clearfix"> </div>
       
<?php echo $__env->make('admin.layouts.container', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>      
    
 <?php echo $__env->make('admin.layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>            


<?php echo $__env->make('admin.layouts.scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>            

    </body>

</html>