<?php $__env->startSection('content'); ?>
            <!-- BEGIN LOGIN FORM -->
                <form class="login-form" role="form" method="POST" action="<?php echo e(url('/admin/login')); ?>">
                        <?php echo e(csrf_field()); ?>

                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <input id="username" type="text" class="form-control form-control-solid placeholder-no-fix" placeholder="Username" name="username" value="<?php echo e(old('username')); ?>" autofocus>
                </div>
                 <?php if($errors->has('username')): ?>
        <span class="help-block">
          <strong><?php echo e($errors->first('username')); ?></strong>
      </span>
      <?php endif; ?>
                <div class="form-group" >
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                   <input id="password" type="password" class="form-control form-control-solid placeholder-no-fix" name="password" placeholder="Password"></div>
                 <?php if($errors->has('password')): ?>
        <span class="help-block">
          <strong><?php echo e($errors->first('password')); ?></strong>
      </span>
      <?php endif; ?>
                <div class="form-actions">
                    <button type="submit" class="btn green uppercase">Login</button>
                </div>

            </form>
            <!-- END LOGIN FORM -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>