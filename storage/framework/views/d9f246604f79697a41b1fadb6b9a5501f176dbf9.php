
<?php $__env->startSection('content'); ?>
<div class="panel-lny-desk row-margin-botttom">
<style>
#chartdiv {
  width : 100%;
  height  : 300px;
}

</style>

<!-- Chart code -->
<script>

  var chartData = updateData();

  var chart = AmCharts.makeChart("chartdiv", {
    "type": "serial",
    "theme": "light",
    "marginRight": 80,
    "dataProvider": chartData,
    "valueAxes": [{
      "position": "left",
      "title": "Price History" 
    }],
    "graphs": [{
      "id": "g1",
      "fillAlphas": 0.4,
      "valueField": "visits",
      "balloonText": "<div style='margin:5px; font-size:19px;'><b>$ [[value]]</b></div>"
    }],
    "chartScrollbar": {
      "graph": "g1",
      "scrollbarHeight": 20,
      "backgroundAlpha": 0,
      "selectedBackgroundAlpha": 0.1,
      "selectedBackgroundColor": "#888888",
      "graphFillAlpha": 0,
      "graphLineAlpha": 0.5,
      "selectedGraphFillAlpha": 0,
      "selectedGraphLineAlpha": 1,
      "autoGridCount": true,
      "color": "#333"
    },
    "chartCursor": {
      "categoryBalloonDateFormat": "JJ:NN, DD MMMM",
      "cursorPosition": "mouse"
    },
    "categoryField": "date",
    "categoryAxis": {
      "minPeriod": "mm",
      "parseDates": true
    },
    "export": {
      "enabled": true,

      "dateFormat": "YYYY-MM-DD HH:NN:SS"
    }
  });


  chart.ignoreZoomed = false;
  chart.addListener("zoomed", function(event) {
    if (chart.ignoreZoomed) {
      chart.ignoreZoomed = false;
      return;
    }
    chart.zoomStartDate = event.startDate;
    chart.zoomEndDate = event.endDate;
  });

  chart.addListener("dataUpdated", function(event) {
    chart.zoomToDates(chart.zoomStartDate, chart.zoomEndDate);
  });

  function updateData() {
   var chartData = [];

   <?php $__currentLoopData = $allprice; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pri): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>




   var newDate = new Date('<?php echo e($pri->created_at); ?>');


   chartData.push({
    date: newDate,
    visits: <?php echo e($pri->price); ?>

  });

   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
   return chartData;
 }
</script>
<div class="row">
  <!-- begin col-3 -->
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-4 col-sm-4">
    <div class="block-lny">
      <div class="block-title-lny bgtimer" id="cointer" style="height: 80px">
        <div id="countdown">
          <div id="demo"></div>
          <div class="labels">
            <li>Days</li>
            <li>Hours</li>
            <li>Mins</li>
            <li>Secs</li>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4 col-sm-4">
    <div class="block-lny" style="height: 80px">
      <div class="block-title-lny"><h4>Referral Link <span class="label copy label-primary" id="copybutton" style="right:23px;cursor: pointer;" onclick="copyToClipboard('#myRefLink')">copy</span></h4></div>
      <div class="block-info-lny text-center" style="padding-top: 13px;" id="myRefLink"><?php echo e(url('/')); ?>/register?refer=<?php echo $refer[0]->r_link;?></div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-2 col-sm-2">
    <div class="block-lny balances" style="height: 80px">
      <img src="<?php echo e(asset('assets/images/coin/bitcoin-icon.png')); ?>">
      <h4>BALANCE</h4>
      <?php $mainbal = $btcblance-$coinblance;
      $mainbals  = sprintf('%f', (float)$mainbal); ?>
      <p> <?php echo $mainbals; ?> </p>  
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-2 col-sm-2">
    <div class="block-lny balances" style="height: 80px">
      <img src="<?php echo e(asset('assets/images/coin/Lny-80x80.png')); ?>">
      <h4>BALANCE</h4>
      <p><?php echo round($coinblances,2); ?> </p>
    </div>
  </div>
  <!-- end col-3 -->
</div>
<div class="row">
  <div class="col-md-4">
    <div class="block-lny">
      <div class="block-title-lny"><h4>Your Account Balance Info</h4></div>
      <div class="block-info-lny">
        <div class="well lny-color">
          <div class="tablecell">My <?php echo e($gset->curCode); ?> Balance</div>
          <div class="tablecell text-right"> <?php echo round($coinblances,2); ?></div>
        </div>
        <?php
        $btcamount = $mainbals * $currentRate; 
        $balances = sprintf('%f', (float)$btcamount);
        ?>
        <div class="well btc-color">
          <div class="tablecell">My Bitcoin Balance</div>
          <div class="tablecell text-right"><?php echo $mainbals;?> &nbsp;(&nbsp; $<?php echo round($balances,2);?>&nbsp; )</div>
        </div>
        <div class="well ref-color">
          <div class="tablecell">LNY Referral Rewards</div>
          <div class="tablecell text-right"><?php echo sprintf('%f', (float)$refer[0]->point);?></div>
        </div>
      </div>
    </div>
    <div class="block-lny">
      <div class="block-title-lny"><h4>Social Media</h4></div>
      <div class="block-info-lny">
        <ul class="socialicons">
        <li><a href="https://www.facebook.com/Leigonphy.Official/" target="_blank"><i class="fa fa-facebook"></i></a></li>
        <li><a href="https://t.me/LeigonphyLNY" target="_blank"><i class="fa fa-telegram"></i></a></li>
        <li><a href="https://twitter.com/leigonphy" target="_blank"><i class="fa fa-twitter"></i></a></li>
        <li><a href="https://www.instagram.com/leigonphy.official/" target="_blank"><i class="fa fa-instagram"></i></a></li>
        <li><a href="https://www.youtube.com/channel/UCeDpMZgeSnwNeAW89Hv9PIg" target="_blank"><i class="fa fa-youtube"></i></a></li>
        </ul>
      </div>
    </div>

  </div>

    <div class="col-md-8">
      <div class="block-lny" style="height: 350px; border-left: 1px solid;">
        <div class="block-title-lny"><h4>Global Transactions Log</h4></div>
        <table class="table table-responsive table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>USER</th>
              <th>TRANS. ID</th>
              <th>AMOUNT</th>
              <th>TIME</th>
            </tr>
          </thead>

         <tbody>
          <tr>
            <?php
            $i=1;
            $aux = count($translog); 
            $newarray = array();
            foreach($translog as $logs){
              $newarray[$aux] = $logs;
              $aux--;
            }

            foreach(array_reverse($newarray) as $logs){

              ?>

              <td><?php echo $i;?></td>
              <td>LNY<?php echo $logs->user_id; ?></td>
              <td><?php echo $logs->trxid; ?></td>
              <td><?php echo $logs->amount; ?></td>
              <td><?php echo $logs->created_at; ?></td>
            </tr>
            <?php $i++;if($i > 8){break;}  }
            ?>

          </tbody>
          </table>
        </div>
    </div>
</div>

<div class="row" style="margin-bottom: 0px;">
  <?php foreach($icoagenda as $icoagendas){ ?>
  <div class="col-md-2 no-padding">
    <div class="block-lny">
      <div class="block-title-lny">
        <?php if($icoagendas->statuscomplted == 2): ?>
        <h4> <?php echo $icoagendas->round_name;?><span class="label label-danger">completed</span></h4>
        <?php elseif($icoagendas->statuscomplted == 3): ?>
        <h4> <?php echo $icoagendas->round_name;?><span class="label label-warning">upcoming</span></h4>
        <?php else: ?>
        <h4><?php echo $icoagendas->round_name;?><span class="label label-primary">progress</span> </h4>
        <?php endif; ?>
      </div>
      <div class="block-info-lny zero-padding">
        <ul class="list-group text-center">
          <?php if($icoagendas->start_date == $icoagendas->last_date): ?>
          <li class="list-group-item">
            <p>
              <span>
                (<?php echo $icoagendas->start_time;?> - <?php echo $icoagendas->last_time;?>)
                UTC
              </span>
              <span class="newdate"><?php echo $icoagendas->start_date;?></span>
            </p>
          </li>
          <?php else: ?>
          <li class="list-group-item"><p><span> (<?php echo $icoagendas->start_time;?> - <?php echo $icoagendas->last_time;?>) UTC  </span>
            <span class="newdate"><?php echo $icoagendas->start_date;?> - <?php echo $icoagendas->last_date;?></span>
          </p>
        </li>
        <?php endif; ?>
        <li class="list-group-item">
          <div><?php echo $icoagendas->total_exa;?> <span><?php echo e($gset->curCode); ?> On Sale</span></div>
          <div>$<?php echo $icoagendas->price;?> <span>&nbsp;per&nbsp;<?php echo e($gset->curCode); ?></span></div>
          <div><?php echo $icoagendas->limit;?> <span><?php echo e($gset->curCode); ?> per user</span></div>
        </li> 
        <li class="list-group-item">
         <?php if($icoagendas->statuscomplted == 2): ?>
         <a href="#"> <button type="button" class="btn btn-danger btn-lg fullwidth">closed</button></a>
         <?php elseif($icoagendas->statuscomplted == 3): ?>

         <a href="#">  <button type="button" class="btn btn-warning btn-lg fullwidth">upcoming</button></a>
         <?php else: ?>

         <a href="#">   <button type="button" class="btn btn-primary btn-lg fullwidth">purchase</button></a>
         <?php endif; ?>
       </li>
     </ul>
   </div>
 </div>
</div>

<?php  }  ?>
</div> 

        <div id="purchase_ico" style="visibility:hidden;"><?php echo $nuevaVariable["hiddenbtcblance"];?></div>
        <div id="total_limit_agenda" style="visibility:hidden;"><?php echo $nuevaVariable["totals"];?></div>
        <div id="agendaid" style="visibility:hidden;"><?php echo $nuevaVariable["ids"];?></div>
        <div id="remaining_ico_agenda" style="visibility:hidden;"></div>
        <div id="perday_limit" style="visibility:hidden;"><?php echo $nuevaVariable["limit"];?></div>
        <div id="roundname" style="visibility:hidden;"><?php echo $nuevaVariable["ico_name"];?></div>
        <div id="perday_user_bal" style="visibility:hidden;"><?php echo $nuevaVariable["perday_user_bal"];?></div>
        <div id="statuscomplted" style="visibility:hidden;"><?php echo $nuevaVariable["statuscomplted"];?></div>
        <?php if(isset($nuevaVariable[0]->maindstart_date_time)){ ?>
        <!-- <div id="start_date_time" style="visibility:hidden;"><?php echo $nuevaVariable["maindstart_date_time"];?></div> -->

        <div id="start_date_time" style="visibility:hidden;">Jul 20, 2018 4:00:00</div>
        <div id="last_date_time" style="visibility:hidden;"><?php echo $nuevaVariable["maindlast_date_time"];?></div>
        <div id="current_date_time" style="visibility:hidden;"><?php echo $nuevaVariable["maincurrentdate"];?></div>
        <?php }else{ ?>
        <!-- <div id="start_date_time" style="visibility:hidden;"><?php ?></div> -->

        <div id="start_date_time" style="visibility:hidden;">Jul 20, 2018 4:00:00</div>
        <div id="last_date_time" style="visibility:hidden;"><?php ?></div>
        <div id="current_date_time" style="visibility:hidden;"><?php ?></div>
        <?php } ?>
            <div id="coinvalue" style="visibility:hidden;"><?php echo $nuevaVariable["totaltetracoins"];?></div>



<!--Copy Data -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script>

  var currentRate = $("#btcpriceajax").html();

  var tetracoinprice = $("#icopriceajaxs").html(); 

  var tetracoinbtcs =  tetracoinprice/ currentRate ;

  var tetracoinbtcsss =  tetracoinbtcs.toFixed(7);

  if(tetracoinbtcsss == "Infinity"){
    $("#onetetravalue").html("0.00");   
  }else{
   $("#onetetravalue").html(tetracoinbtcsss);    
 }

// Set the date we're counting down to

  var countDownDate = new Date("Jul 20, 2018 16:00:00").getTime();
// Update the count down every 1 second
var x = setInterval(function() {

  var start_date_time=   $("#start_date_time").html();
  var last_date_time=    $("#last_date_time").html();      
  var roundname = $("#roundname").html();

  var countDownDate = new Date(start_date_time).getTime();  
  var countUpDate = new Date(last_date_time).getTime(); 
  var current_date_time =    $("#current_date_time").html();
  var status = $("#statuscomplted").html();

 //alert(diff);
    // Get todays date and time


    var now = new Date().getTime();

    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    


    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // If the count down is over, write some text 
    
    if(( status ==1) || (now > countDownDate && now < countUpDate)  ){


     document.getElementById("countdown").innerHTML =" <h5>Start " +roundname+"</h5>" ;

     $("#availbale").show(); 
     $("#contik").show();
     $("#agendawhite").hide();
     $('#buyP').prop( "disabled", false); 
     
   }else if(( status ==3) || (now < countDownDate && now < countUpDate) ){

    document.getElementById("demo").innerHTML = "<span>" + days + "</span><span>" + hours + "</span><span>"
    + minutes + "</span><span>" + seconds + "</span>";

    $("#availbale").hide(); 
    $("#contik").hide();
    $("#agendawhite").show();
    $('#nextbutton').prop( "disabled", true);
    $('#buyP').prop( "disabled", true);   
  }else {

   document.getElementById("countdown").innerHTML = "<h5>There is no agenda</h5>";
       //alert("else");
       $("#availbale").hide(); 
       $("#contik").hide();
       $("#agendawhite").show();
       $('#nextbutton').prop( "disabled", true);
       $('#buyP').prop( "disabled", true);
     }


   }, 1000);   

function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
  $("#copybutton").innerHTML = "copied";
}


</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.admaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>