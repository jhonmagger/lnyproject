

<?php $__env->startSection('content'); ?>
	<div class="row">
		<div class="col-md-12">
			<div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-envelope font-blue-sharp"></i>
                                        <span class="caption-subject font-blue-sharp bold uppercase">Send Email</span>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <form role="form" method="POST" action="<?php echo e(route('send.email')); ?>" enctype="multipart/form-data">
                                    	<?php echo e(csrf_field()); ?>

                                        <div class="form-body">
                                            <div class="form-group">
                                                <label>To</label>
                                                <input type="email" name="emailto" class="form-control input-lg" value="<?php echo e($user->email); ?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" name="reciver" class="form-control input-lg" value="<?php echo e($user->firstname); ?> <?php echo e($user->lastname); ?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Subject</label>
                                                <input type="text" name="subject" class="form-control input-lg" value="">
                                            </div>
                                            <div class="form-group">
                                                <label>Email Message</label>
                                                <textarea class="form-control" name="emailMessage" rows="10">
                                                	
                                                </textarea>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <button type="submit" class="submit-btn btn btn-primary btn-lg btn-block login-button">Send Email</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
		</div>
	</div>
	
	<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>