<?php $__env->startSection('content'); ?>
<div class="panel-lny-desk w50">

<div class="row">
        <!-- begin col-3 -->
  <div class="col-md-6 col-sm-6">
    <div class="block-lny balances" style="height: 80px">
      <img src="<?php echo e(asset('assets/images/coin/Lny-80x80.png')); ?>">
      <h4>BALANCE</h4>
      <p><?php print_r($coinblances); ?> </p>
    </div>
  </div>

  <div class="col-md-6">
    <div class="block-lny">
      <div class="block-title-lny"><h4>Actions</h4></div>
      <div class="block-info-lny">
        <button type="button" class="btn btn-inverse btn-lg" data-toggle="modal" data-target="#" style="padding:6px 10px !important;font-size:16px !important; background:#348fe2 !important;     opacity: 0.4; width: 30%;  border: #fff !important;">Deposit</button>
       <button type="button" class="btn btn-inverse btn-lg" data-toggle="modal" data-target="#" style="padding:6px 10px !important;font-size:16px !important;background:#348fe2 !important; width: 30%;     opacity: 0.4;  border: #fff !important;">Withdraw</button>
      </div>
    </div>
  </div>
</div>

        <div class="row">
     <div class="col-md-12">
    <div class="block-lny" style="height: 350px; overflow-y: unset;border-left: 1px solid;">
      <div class="block-title-lny"><h4>Coin Transaction Log</h4></div>
      <table class="table" style="margin-bottom: 0px;">
              <thead>
               <tr>
                             <th>#</th>
                        <th>AMOUNT</th>
                        <th>BTC</th>
                              <th>STATUS</th>
                        <th>HASH</th>
                      </tr>
                    </thead>
              </thead>
              
  <tbody>
                      <tr>
                        <?php
                                 $i=1; 
                        
                              $aux = count($btc_address); 
                          $newarray = array();
                          foreach($btc_address as $btc_addresss){
                            $newarray[$aux] = $btc_addresss;
                            $aux--;
                          }
                                  foreach(array_reverse($newarray) as $btc_addresss){
                                   
                                    ?>
                                  <tr>
                                  <td><?php echo $i;?></td>
                                  <td><?php echo $btc_addresss->amount;?></td>
                                  <td><?php echo round($btc_addresss->inusd,8);?></td>
                                    
                                 
                                  <td>
                                     <?php $value= $btc_addresss->status;
                                      if($value ==0){
                                       echo "Unconfirm";
                                      }else{
                                       echo "Confirmed";
                                      }
                                        ?>
                                    </td>
                                  <td><?php echo $btc_addresss->trxid;?></a></td> </tr>

                                 <?php $i++; }
                                  ?>
                                               
                        <!-- <td colspan="4">No Ethereum/Autobot transaction found</td> -->
                      </tr>
                    </tbody>
             
            </div>
          </div>
        </div>

      </div>

</div>

</div>


<style>
.flet{
float: left;
    width: 24%;
    margin-left: 18%;
margin-top: 10px;
}
.right{

margin-top: 10px;
}
</style>
</div>

<?php $__env->stopSection(); ?>


      
<?php echo $__env->make('front.layouts.admaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>