<html lang="en">
<head>
    <meta charset="utf-8" />
    <title><?php echo e($gset->webTitle); ?> | Dashboard</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    
  
    
    <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
    <link href="<?php echo e(asset('assets/user/plugins/jquery-jvectormap/jquery-jvectormap.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/user/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/user/plugins/gritter/css/jquery.gritter.css')); ?>" rel="stylesheet" />
    <!-- ================== END PAGE LEVEL STYLE ================== -->
    
    <!-- ================== BEGIN BASE JS ================== -->
    <!---<script src="<?php echo e(asset('assets/user/plugins/pace/pace.min.js')); ?>"></script>-->
    <script src="<?php echo e(asset('assets/user/css/custom.css')); ?>"></script>

<!-- ================== Chart JS ================== -->
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
    <style>
      .amcharts-export-menu li {
        display: none;
    }
    </style>
    <script src="<?php echo e(asset('assets/js/jquery.min.js')); ?>"></script>

     <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
 <script src="https://www.google.com/recaptcha/api.js"></script>
<script src="<?php echo e(asset('assets/user/js/dashboard.min.js')); ?>"></script>

<script src="<?php echo e(asset('assets/user/js/livicons-1.4.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/user/js/raphael-min.js')); ?>"></script>

    
      <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Francois+One|Roboto" rel="stylesheet">
    <link href="<?php echo e(asset('assets/user/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/user/plugins/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/user/plugins/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/user/css/animate.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/user/css/style.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/user/css/style-responsive.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/user/css/theme/default.css')); ?>" rel="stylesheet" id="theme" />
    <link href="<?php echo e(asset('assets/web/assets/css/dashboard_lny.css')); ?>" rel="stylesheet" />
    <!-- ================== END BASE CSS STYLE ================== -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117102859-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117102859-1');
</script>

</head>




