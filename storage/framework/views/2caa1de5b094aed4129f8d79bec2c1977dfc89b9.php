
<?php $__env->startSection('content'); ?>
<style>

</style>
<div class="panel-lny-desk w50">

    <div class="row">
        <div class="col-md-12">

            <div style="text-align: center">
                <img src="https://leigonphy.co/assets/web/assets/images/newavatar.png" style="width: 180px;">
          </div>
            <div class="block-lny" style="height: 80px">
              <div class="block-title-lny" style="position: relative;"><h4>Referral Link <span class="label copy label-primary" id="copybutton" style="right:23px;cursor: pointer;" onclick="copyToClipboard('#myRefLink')">copy</span></h4></div>
              <div class="block-info-lny text-center" style="padding-top: 13px;" id="myRefLink"><?php echo e(url('/')); ?>/register?refer=<?php echo $refer[0]->r_link;?></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12" >
            <div class="block-lny" style="border-left: 1px solid;">
              <div class="block-title-lny"><h4>Referral Rewards Structure</h4></div>
              <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="main_tops">Level</th>
                        <th class="main_tops">Reward</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="main_topss">1</td>
                        <td class="main_topss"><?php echo $refer_percetage[0]->levelone; ?> %</td>
                    </tr>
                    <tr>
                        <td class="main_topss">2</td>
                        <td class="main_topss"><?php echo $refer_percetage[0]->leveltwo; ?>% + (50 bonus coins)</td>
                  </tr>
                </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="block-lny" style="height: 80px">
              <div class="block-title-lny"><h4> Current Level</h4></div>
              <div class="block-info-lny text-center" style="padding-top: 13px;" >Level 1</div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="block-lny" style="height: 80px">
              <div class="block-title-lny"><h4> Total Referral Coins</h4></div>
              <div class="block-info-lny text-center" style="padding-top: 13px;" id="myRefLink"><?php echo sprintf('%f', (float)$refer[0]->point);?></div>
            </div>
        </div>
    </div>
</div>
<script>
    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        $temp.remove();
    }
</script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.layouts.admaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>