

<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Referral List</span>
                </div>

            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover order-column">
                <thead>
                    <tr>
                        <th>
                            S.No 
                        </th>
                        <th>
                            Username
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Point
                        </th>
                        <th>
                        Add Coin
                        </th>
                  	 </tr>
                </thead>
                <tbody>
                    <?php $i=1;?>
		 	<?php $__currentLoopData = $refer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $refers): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <tr>
                     	<td>
                        <?php echo $i;?>	
                        </td>
                        <td>
                            <?php echo e($refers->username); ?>    
                        </td>
                         <td>
                            <?php echo e($refers->email); ?>    
                        </td>
                         <td>
                            <?php echo e($refers->point); ?>       
                        </td> 
                        
                       
                           <td>
                               <a href="<?php echo e(route('addcoin.referal', $refers->user_id)); ?>" class="btn btn-outline btn-circle btn-sm green">
                             <i class="fa fa-eye"></i> Add Coin </a>
                               
                               </td>
                         
                           
                     </tr>
                     <?php $i++;?>
 			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   
 			<tbody>
           </table>
           <?php echo $refer->render(); ?>
        </div>
			
			</div><!-- row -->
			</div>
		</div>
	</div>		
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>