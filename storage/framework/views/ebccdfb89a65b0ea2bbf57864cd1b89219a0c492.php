

<?php $__env->startSection('content'); ?>
<div class="row">
		<div class="col-md-12">
			<h2>Email Template Settings</h2>
			<hr/>
		</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-bookmark"></i>Short Code</div>

				</div>
				<div class="portlet-body">
					<div class="table-scrollable">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th> # </th>
									<th> CODE </th>
									<th> DESCRIPTION </th>
								</tr>
							</thead>
							<tbody>


								<tr>
									<td> 1 </td>
									<td> <pre>&#123;&#123;message&#125;&#125;</pre> </td>
									<td> Details Text From Script</td>
								</tr>

								<tr>
									<td> 2 </td>
									<td> <pre>&#123;&#123;name&#125;&#125;</pre> </td>
									<td> Users Name. Will Pull From Database and Use in EMAIL text</td>
								</tr>



							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-envelope font-blue-sharp"></i>
                                        <span class="caption-subject font-blue-sharp bold uppercase">Email Template</span>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <form role="form" method="POST" action="<?php echo e(url('admin/gsettings/email')); ?>/<?php echo e($gsettings->id); ?>" enctype="multipart/form-data">
                                    	<?php echo e(csrf_field()); ?>

										<?php echo e(method_field('put')); ?>

                                        <div class="form-body">
                                            <div class="form-group">
                                                <label>Email Sent Form</label>
                                                <input type="email" name="emailSender" class="form-control input-lg" value="<?php echo e($gsettings->emailSender); ?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Email Message</label>
                                                <textarea class="form-control" name="emailMessage" rows="10">
                                                	<?php echo e($gsettings->emailMessage); ?>

                                                </textarea>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <button type="submit" class="btn green btn-lg">Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
		</div>
	</div>
	
	<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>