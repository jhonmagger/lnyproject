

<?php $__env->startSection('content'); ?>
      
        <div id="header" class="banner banner-full d-flex align-items-center">

            <div class="container">
                <div class="banner-content no-padding">

                    <div class="row align-items-center mobile-center">
                        <div class="col-lg-12 col-md-12 \">
                            <div class="header-txt">
                                <div class="countdown-box text-center">
                  <h2 class="section-title">404 Not Found</h2>
                <div class="error-details">
                    Sorry, an error has occured, Requested page not found!
                </div>
                <div class="error-actions">
                    <a href="<?php echo e(route('index')); ?>" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>
                        Take Me Home </a>
                </div>
                </div>
                </div>
                        </div><!-- .col  -->
                    </div><!-- .row  -->
                </div><!-- .banner-content  -->
            </div><!-- .container  -->
        </div>
        <!-- End Banner/Slider -->
        </div><!-- .container  -->
        </div><!-- .header-partners  -->
    </header>





<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>