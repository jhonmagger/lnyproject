<div class="page-sidebar-wrapper">

<div class="page-sidebar navbar-collapse collapse">
 
    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
   
        <li class="sidebar-toggler-wrapper hide">
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <div class="sidebar-toggler"> </div>
            <!-- END SIDEBAR TOGGLER BUTTON -->
        </li>
    
        <li class="nav-item  <?php if(request()->path() == 'admin/home'): ?> active open <?php endif; ?>">
            <a href="<?php echo e(url('admin/home')); ?>" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span class="title">Dashboard</span>
                <span class="selected"></span>
            </a>
        </li>
         <li class="nav-item
            <?php if(request()->path() == 'admin/manage/userlog'): ?> active open
                <?php elseif(request()->path() == 'admin/manage/users'): ?> active open
                <?php elseif(request()->path() == 'admin/banned/users'): ?> active open
                <?php elseif(request()->path() == 'admin/packages'): ?> active open
                <?php elseif(request()->path() == 'admin/broadcast'): ?> active open
                <?php elseif(request()->path() == 'admin/documents'): ?> active open
            <?php endif; ?>">
            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-users"></i>
                <span class="title">User Management</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item <?php if(request()->path() == 'admin/manage/users'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(route('withdraw.users')); ?>" class="nav-link ">
                        <i class="fa fa-users"></i>
                        <span class="title">Users</span>
                    </a>
                </li> 
                
                <li class="nav-item <?php if(request()->path() == 'admin/manage/userlog'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(route('withdraw.userlog')); ?>" class="nav-link ">
                        <i class="fa fa-money"></i>
                        <span class="title">Users Transaction Log</span>
                    </a>
                </li>
                 <li class="nav-item <?php if(request()->path() == 'admin/broadcast'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(route('broadcast')); ?>" class="nav-link ">
                        <i class="icon-envelope"></i>
                        <span class="title">Broadcast Email</span>
                    </a>
                </li> 
                <li class="nav-item <?php if(request()->path() == 'admin/documents'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(route('document.requests')); ?>" class="nav-link ">
                        <i class="icon-docs"></i>
                        <span class="title">Documents</span>
                    </a>
                </li> 
                <li class="nav-item <?php if(request()->path() == 'admin/banned/users'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(route('new.users')); ?>" class="nav-link ">
                        <i class="fa fa-times"></i>
                        <span class="title">Banned Users</span>
                    </a>
                </li> 
               
            </ul>
        </li>

        <!---<li class="nav-item
            <?php if(request()->path() == 'admin/withdraw/requests'): ?> active open
              <?php elseif(request()->path() == 'admin/withdraw/lists'): ?> active open
              <?php elseif(request()->path() == 'admin/withdraw/refunded'): ?> active open
              <?php elseif(request()->path() == 'admin/wmethod'): ?> active open
            <?php endif; ?>">
            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-id-card-o"></i>
                <span class="title">Withdraw</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item <?php if(request()->path() == 'admin/withdraw/requests'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(route('withdraw.requests')); ?>" class="nav-link ">
                        <i class="fa fa-money"></i>
                        <span class="title">Withdraw Requests</span>
                    </a>
                </li>
                <li class="nav-item <?php if(request()->path() == 'admin/withdraw/lists'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(route('withdraw.lists')); ?>" class="nav-link ">
                        <i class="fa fa-list"></i>
                        <span class="title">Withdraw Log</span>
                    </a>
                </li>
                <li class="nav-item <?php if(request()->path() == 'admin/withdraw/refunded'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(route('withdraw.refundlog')); ?>" class="nav-link ">
                        <i class="fa fa-share"></i>
                        <span class="title">Refund Log</span>
                    </a>
                </li>
            </ul>
        </li>-->

         

        <li class="nav-item
            <?php if(request()->path() == 'admin/gateway'): ?> active open
                <?php elseif(request()->path() == 'admin/deposits'): ?> active open 
                <?php elseif(request()->path() == 'admin/gatewaytoken'): ?> active open             
                                  
            <?php endif; ?>">
            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-credit-card"></i>
                <span class="title">Deposit</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item <?php if(request()->path() == 'admin/gateway'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(url('admin/gateway')); ?>" class="nav-link ">
                        <i class="fa fa-credit-card"></i>
                        <span class="title">Cold Wallet Address</span>
                    </a>
                </li>
               <li class="nav-item <?php if(request()->path() == 'admin/gatewaytoken'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(url('admin/gatewaytoken')); ?>" class="nav-link ">
                        <i class="fa fa-credit-card"></i>
                        <span class="title">Blockcyper token Address</span>
                    </a>
                </li> 
                   
                <li class="nav-item <?php if(request()->path() == 'admin/deposits'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(route('deposits')); ?>" class="nav-link ">
                        <i class="fa fa-indent"></i>
                        <span class="title">Deposit List</span>
                    </a>
                </li>                   
            </ul>
        </li>
        <li class="nav-item 
            <?php if(request()->path() == 'admin/gsettings'): ?> active open
                <?php elseif(request()->path() == 'admin/gsettings/email'): ?> active open
                <?php elseif(request()->path() == 'admin/gsettings/sms'): ?> active open
            <?php endif; ?>">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-cogs"></i>
                <span class="title">Website Control</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item <?php if(request()->path() == 'admin/gsettings'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(url('admin/gsettings')); ?>" class="nav-link ">
                        <i class="fa fa-cog"></i>
                        <span class="title">General Settings</span>
                    </a>
                </li>
                <li class="nav-item <?php if(request()->path() == 'admin/gsettings/email'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(url('admin/gsettings/email')); ?>" class="nav-link ">
                        <i class="fa fa-envelope-o"></i>
                        <span class="title">Email Settings</span>
                    </a>
                </li>
                 <li class="nav-item <?php if(request()->path() == 'admin/gsettings/sms'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(url('admin/gsettings/sms')); ?>" class="nav-link ">
                        <i class="fa fa-envelope-o"></i>
                        <span class="title">SMS Settings</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item
            <?php if(request()->path() == 'admin/menu'): ?> active open
               <?php elseif(request()->path() == 'admin/logo'): ?> active open
               <?php elseif(request()->path() == 'admin/slider'): ?> active open
               <?php elseif(request()->path() == 'admin/about'): ?> active open
               <?php elseif(request()->path() == 'admin/service'): ?> active open
               <?php elseif(request()->path() == 'admin/timeline'): ?> active open
               <?php elseif(request()->path() == 'admin/footer'): ?> active open
               <?php elseif(request()->path() == 'admin/social'): ?> active open
               <?php elseif(request()->path() == 'admin/contac'): ?> active open
            <?php endif; ?>">
            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-desktop"></i>
                <span class="title"> Interface Control</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item <?php if(request()->path() == 'admin/logo'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(route('logo')); ?>" class="nav-link ">
                        <i class="fa fa-picture-o"></i>
                        <span class="title">Logo and Icon</span>
                    </a>
                </li>
                <li class="nav-item <?php if(request()->path() == 'admin/slider'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(route('slider')); ?>" class="nav-link ">
                        <i class="fa fa-picture-o"></i>
                        <span class="title">Banner / Slider</span>
                    </a>
                </li>
                <li class="nav-item <?php if(request()->path() == 'admin/about'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(route('about')); ?>" class="nav-link ">
                        <i class="fa fa-picture-o"></i>
                        <span class="title">About Section</span>
                    </a>
                </li>
                 <li class="nav-item <?php if(request()->path() == 'admin/service'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(route('service')); ?>" class="nav-link ">
                        <i class="fa fa-picture-o"></i>
                        <span class="title">Service Section</span>
                    </a>
                </li>
                
                <li class="nav-item <?php if(request()->path() == 'admin/timeline'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(route('timeline')); ?>" class="nav-link ">
                        <i class="fa fa-tree"></i>
                        <span class="title">Timeline</span>
                    </a>
                </li>
                <li class="nav-item <?php if(request()->path() == 'admin/contac'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(route('contac')); ?>" class="nav-link ">
                        <i class="fa fa-id-card"></i>
                        <span class="title">Contact Information</span>
                    </a>
                </li>
                <li class="nav-item <?php if(request()->path() == 'admin/footer'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(route('footer')); ?>" class="nav-link ">
                        <i class="fa fa-list"></i>
                        <span class="title">Footer Content</span>
                    </a>
                </li>

            </ul>
        </li>
        <li class="nav-item
            <?php if(request()->path() == 'admin/charges'): ?> active open
                <?php elseif(request()->path() == 'admin/price'): ?> active open
               <?php elseif(request()->path() == 'admin/policy'): ?> active open
            <?php endif; ?>">
            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-id-card-o"></i>
                <span class="title">Company Policy</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item <?php if(request()->path() == 'admin/charges'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(url('admin/charges')); ?>" class="nav-link ">
                        <i class="fa fa-money"></i>
                        <span class="title">Charge / Commision</span>
                    </a>
                </li>
                 <li class="nav-item <?php if(request()->path() == 'admin/price'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(url('admin/price')); ?>" class="nav-link ">
                        <i class="fa fa-money"></i>
                        <span class="title">Price</span>
                    </a>
                </li>
                <li class="nav-item <?php if(request()->path() == 'admin/policy'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(url('admin/policy')); ?>" class="nav-link ">
                        <i class="icon-layers"></i>
                        <span class="title">Policy & Terms</span>
                    </a>
                </li>
                
            </ul>
        </li>
         <li class="nav-item  <?php if(request()->path() == 'admin/icoagenda'): ?> active open <?php endif; ?>">
            <a href="<?php echo e(url('admin/icoagenda')); ?>" class="nav-link nav-toggle">
                <i class="fa fa-cogs"></i>
                <span class="title">ICO Agenda</span>
                
            </a>
        </li>
        <li class="nav-item  <?php if(request()->path() == 'admin/addcoin'): ?> active open <?php endif; ?>">
            <a href="<?php echo e(url('admin/addcoin')); ?>" class="nav-link nav-toggle">
                <i class="fa fa-money"></i>
                <span class="title">Add Referral Coin</span>
                
            </a>
        </li>
        
        
       

       <li class="nav-item
            <?php if(request()->path() == 'admin/levelone'): ?> active open
                <?php elseif(request()->path() == 'admin/leveltwo'): ?> active open
               
            <?php endif; ?>">
            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-id-card-o"></i>
                <span class="title">Referral</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item <?php if(request()->path() == 'admin/levelone'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(url('admin/levelone')); ?>" class="nav-link ">
                        <i class="fa fa-money"></i>
                        <span class="title">Level One</span>
                    </a>
                </li>
                 <li class="nav-item <?php if(request()->path() == 'admin/leveltwo'): ?> active open <?php endif; ?>">
                    <a href="<?php echo e(url('admin/leveltwo')); ?>" class="nav-link ">
                        <i class="fa fa-money"></i>
                        <span class="title">Level Two</span>
                    </a>
                </li>
                
                
            </ul>
        </li>
        
        
        
 



    </ul>

                    <!-- END SIDEBAR MENU -->
                    <!-- END SIDEBAR MENU -->
    </div>
                <!-- END SIDEBAR -->
 </div>
<style>
.page-sidebar .page-sidebar-menu .sub-menu li>a, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu .sub-menu li>a{

  padding: 6px 4px 6px 43px !important;
}
.sub-menu>li>a {
    padding: 6px 6px 6px 43px !important;
}
</style>




