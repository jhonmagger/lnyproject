

<?php $__env->startSection('content'); ?>
    <div class="panel-lny-desk w50">
        <div class="row">
            <div class="col-md-12">
                <div class="block-lny">
                    <div class="block-title-lny"><h4>Support</h4></div>
                    <div class="block-info-lny">
                        <form method="post" class="form" role="form" action="<?php echo e(route('support')); ?>">
                            <?php ($user = auth()->user()); ?>
                            <?php echo e(csrf_field()); ?>

                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label>Name</label>
                                    <input type="text" id="name" name="name" value="<?php echo e($user->firstname); ?>" class="form-control  input-sz" required readonly/>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Email</label>
                                    <input type="email" id="email" name="email" value="<?php echo e($user->email); ?>" class="form-control  input-sz" required readonly/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Message</label>
                                <textarea id="message" name="message" rows="5" class="form-control  input-sz" required autofocus></textarea>
                            </div>
                            <div class="form-group text-right">
                                <button class="btn btn-primary" type="submit">Send</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.admaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>