<div id="sidebar" class="sidebar">
      <!-- begin sidebar scrollbar -->
      <div data-scrollbar="true" data-height="100%">
        <!-- begin sidebar user -->
        <ul class="nav">
          <li class="nav-profile">
            <div class="info">
              <?php echo e(Auth::user()->firstname); ?>

              <?php echo e(Auth::user()->lastname); ?>

              <small><?php echo e(Auth::user()->username); ?></small>
            </div>
          </li>
        </ul>
        <!-- end sidebar user -->
        <!-- begin sidebar nav -->
        <ul class="nav">
              <li>
                  <a href="<?php echo e(route('home')); ?>">
                      <i class="livicon" data-name="dashboard" data-size="24" data-c="#EF6F6C" data-hc="#242A30" data-loop="true"></i>
                      <span class="title">Dashboard</span> 
                  </a>
              </li>
           <li >
            <a href="<?php echo e(route('referral')); ?>">
              <i class="livicon" data-name="users" data-size="24" data-c="#67C5DF" data-hc="#242A30" data-loop="true"></i>
              <span>Referral</span>
            </a>
          </li>
          <li class="has-sub">
            <a href="javascript:;">
                <b class="caret pull-right"></b>
                <i class="livicon" data-name="responsive" data-size="24" data-c="#6CC66C" data-hc="#242A30" data-loop="true"></i>
                <span>Wallets</span>
            </a>
            <ul class="sub-menu">
              <li>
                <a href="<?php echo e(route('coinlog')); ?>"><img src="<?php echo e(asset('assets/web/assets/images/logo_coin.png')); ?>"  style="width:15%;"> <?php echo e($gset->curCode); ?> Wallet</a>
              </li>
              <li><a href="<?php echo e(route('bitlog')); ?>">
                    <img src="<?php echo e(asset('assets/web/assets/images/bitcoin_logo.png')); ?>"  style="width:15%;"> BTC Wallet</a>
              </li> 
            </ul>
          </li>
              <li><a href="<?php echo e(route('coinbuy')); ?>"><i class="livicon" data-name="credit-card" data-size="24" data-c="#F89A14" data-hc="#242A30" data-loop="true"></i> <span>ICO</span></a></li>
             <!---<li><a href="<?php echo e(route('deposit')); ?>"><i class="fa fa-credit-card" aria-hidden="true"></i> <span>Buy <?php echo e($gset->curCode); ?></span></a></li>--->
             <!---<li><a href="<?php echo e(route('sell.coin')); ?>"><i class="fa fa-money" aria-hidden="true"></i> <span>Sell <?php echo e($gset->curCode); ?></span></a></li>--->
             <li><a href="#"><i class="livicon" data-name="money" data-size="24" data-c="#1DA1F2" data-hc="#242A30" data-loop="true"></i><span class="text-gray">Internal Exchange</span></a></li>
              <li><a href="#"><i class="livicon" data-name="linechart" data-size="24" data-c="#6CC66C" data-hc="#242A30" data-loop="true"></i><span class="text-gray">Staking</span></a></li>
            <!---<li><a href="<?php echo e(route('convert')); ?>""><i class="fa fa-arrows" aria-hidden="true"></i> <span>Convert</span></a></li>-->
           
            <li>
            <li ><a href="<?php echo e(route('user.profile')); ?>"><i class="livicon" data-name="user" data-size="24" data-c="#E9573F" data-hc="#242A30" data-loop="true"></i> <span>Profile</span></a></li>
            <li>
                <a href="<?php echo e(route('changepass')); ?>"><i class="livicon" data-name="key" data-size="24" data-c="#6CC66C" data-hc="#242A30" data-loop="true"></i> <span>Password</span></a>
            </li>
            <li><a href="<?php echo e(route('go2fa')); ?>"><i class="livicon" data-name="lock" data-size="24" data-c="#F89A14" data-hc="#242A30" data-loop="true"></i> <span>Security</span></a></li>
            <li><a href="<?php echo e(route('support')); ?>"><i class="livicon" data-name="mail" data-size="24" data-c="#67C5DF" data-hc="#242A30" data-loop="true"></i> <span>Support</span></a></li>
        
            <?php if(Auth::user()->docv != '1'): ?>
            <!---<li ><a href="<?php echo e(route('document')); ?>"><i class="livicon" data-name="coin" data-size="24" data-c="#418BCA" data-hc="#418BCA" data-loop="true"></i> <span>Verify Document</span></a></li>-->
            <?php endif; ?>

            <li>
              <a href="<?php echo e(route('logout')); ?>"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();"><i class="livicon" data-name="rocket" data-size="24" data-c="#1DA1F2" data-hc="#242A30" data-loop="true"></i>
              <span>Logout</span>
          </a>

          <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
              <?php echo e(csrf_field()); ?>

          </form>
          </li>
                 
              <!-- begin sidebar minify button -->
          <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="livicon" data-name="angle-double-left" data-size="24" data-c="#EF6F6C" data-hc="#242A30" data-loop="true"></i></a></li>
              <!-- end sidebar minify button -->
        </ul>
        <!-- end sidebar nav -->
      </div>
      <!-- end sidebar scrollbar -->
    </div>
    <div class="sidebar-bg"></div>
    <!-- end #sidebar -->


