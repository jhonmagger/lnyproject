<?php if(session('success')): ?>
<script type="text/javascript">
        $(document).ready(function()){
        	swal("Success!", "<?php echo e(session('success')); ?>", "success");
        });
</script>
<?php endif; ?>
<?php if(session('alert')): ?>
<script type="text/javascript">
        $(document).ready(function(){
        	swal("Sorry!", "<?php echo e(session('alert')); ?>", "error");
        });
</script>
<?php endif; ?>
<?php if(session('error')): ?>
<script type="text/javascript">
        $(document).ready(function(){
        	swal("Sorry!", "<?php echo e(session('error')); ?>", "error");
        });
</script>
<?php endif; ?>