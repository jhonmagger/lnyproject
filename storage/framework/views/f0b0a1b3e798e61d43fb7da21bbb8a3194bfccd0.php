<?php $__env->startSection('content'); ?>
<div class="row">
<div class="col-md-12">
<!-- BEGIN SAMPLE FORM PORTLET-->
<div class="portlet light bordered">
<div class="portlet-title">
<div class="caption font-red-sunglo">
<i class="icon-settings font-red-sunglo"></i>
<span class="caption-subject bold uppercase">Cold Wallet Address</span>
</div>
</div>
<div class="portlet-body form">
<div class="row">
<div class="table-scrollable">
<table class="table table-striped table-bordered table-advance table-hover table-responsive">
<thead>
<tr>
<th>
<i class="fa fa-picture-o"></i> Logo 
</th>
<th>
<i class="fa fa-credit-card"></i> Gateway Name
</th>
<th>
Merchant Address
</th>
<th>
Status
</th>
<th>
Action
</th>
</tr>
</thead>
<tbody>
<?php $__currentLoopData = $gateways; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gateway): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<tr class="<?php echo e($gateway->id > 100 ? 'success' : ''); ?>">
<td>
<img src="<?php echo e(asset('assets/images/gateway')); ?>/<?php echo e($gateway->gateimg); ?>" width="50">
</td>
<td>
<?php echo e($gateway->name); ?>  	
</td> 
<td>
<?php echo e($gateway->val1); ?> 
</td>

<td>
<?php echo e($gateway->status == "1" ? 'Active' : 'Deactive'); ?>

</td>
<td>
<a href="" class="btn btn-outline btn-circle btn-sm purple" data-toggle="modal" data-target="#Modal<?php echo e($gateway->id); ?>">
<i class="fa fa-edit"></i> Edit </a>
</td>

</tr>
<!--Edit Modal -->
<div class="modal fade" id="Modal<?php echo e($gateway->id); ?>" role="dialog">
<div class="modal-dialog">

	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Edit <?php echo e($gateway->name); ?> Information</h4>
		</div>
		<form role="form" method="POST" action="<?php echo e(url('admin/gateway')); ?>/<?php echo e($gateway->id); ?>" enctype="multipart/form-data">
			<?php echo e(csrf_field()); ?>

			<?php echo e(method_field('put')); ?>

			<div class="modal-body">
				<div class="form-group">
					<span class="btn green fileinput-button">
						<i class="fa fa-plus"></i>
						<span> Upload Logo </span>
						<input type="file" name="gateimg" class="form-control input-lg"> 
					</span>
				</div>
				<div class="form-group">
					<label for="name">Name of Gateway</label>
					<input type="text" value="<?php echo e($gateway->name); ?>" class="form-control" id="name" name="name" >
				</div>

				<div class="form-group">
					<label for="val1">Payment Details</label>
					<input type="text" value="<?php echo e($gateway->val1); ?>" class="form-control" id="val1" name="val1" >
				</div>								
				
				<hr/>
				<div class="form-group">
					<label for="status">Status</label>
					<select class="form-control" name="status">
						<option value="1" <?php echo e($gateway->status == "1" ? 'selected' : ''); ?>>Active</option>
						<option value="0" <?php echo e($gateway->status == "0" ? 'selected' : ''); ?>>Deactive</option>
					</select>

				</div>
			</div>

			<div class="modal-footer">
				<button type="submit" class="btn btn-success btn-block">Update</button>
			</div>

		</form>
	</div>
</div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
<tbody>
</table>
</div>

</div><!-- row -->
</div>
</div>
</div>		
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>