<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bitcoinsend extends Model
{
   protected $table = 'bitcoinsend';
   protected $fillable = array( 'user_id', 'hash_id','balance','confirm','time');
}
