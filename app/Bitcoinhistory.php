<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bitcoinhistory extends Model
{
   protected $table = 'bitcoinhistory';
   protected $fillable = array( 'user_id', 'address','tx_hash','value','main','time');
}
