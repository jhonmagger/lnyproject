<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Icoagenda extends Model
{
   protected $table = 'icoagenda';
   protected $fillable = array( 'user_id','round_name','start_date','last_date','total_exa','avl_total_exa','price','limit','status','statuscomplted','start_time','last_time','start_timestamp','last_timestamp');
}
