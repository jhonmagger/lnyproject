<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referralprice extends Model
{
   protected $table = 'referral_price';
   protected $fillable = array( 'levelone','leveltwo','created_at','updated_at');
}
