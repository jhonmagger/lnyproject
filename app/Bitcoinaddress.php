<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bitcoinaddress extends Model
{
   protected $table = 'bitcoinaddress';
   protected $fillable = array( 'user_id', 'address_label','bitcoin_address','private_key','public','address_type');
}
