<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coinpayment extends Model
{
   protected $table = 'coin_payment';
   protected $fillable = array( 'gateway_id', 'user_id','trxid','amount','charge','inusd','details','status');

}
