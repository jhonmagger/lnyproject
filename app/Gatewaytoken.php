<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gatewaytoken extends Model
{
	protected $table = 'gatewaystoken';
   protected $fillable = array(
   								'name','gateimg', 'val2', 'status'
   	 						);
   	public function deposit()
    {
        return $this->hasMany('App\Deposit', 'id', 'user_id');
    }
}
