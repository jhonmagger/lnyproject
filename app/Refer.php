<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Refer extends Model
{
   protected $table = 'refer';
   protected $fillable = array( 'user_id', 'r_link','parent','point','level1','level2','status');
}
