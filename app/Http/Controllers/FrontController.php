<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use App\Menu;
use App\Statistic;
use App\About;
use App\Service;
use App\Sericon;
use App\Paymethod;
use App\Payintro;
use App\Testimonial;
use App\User;
use Auth;
use Carbon\Carbon;
use App\Contac;
use App\Gsetting;
use App\Charge;
use App\Price;
use App\Uwdlog;
use App\Timeline;
use Mail;
use App\Icoagenda;
use App\Lib\GoogleAuthenticator;
use DB;


class FrontController extends Controller
{
    public function index()
    {
        $all = file_get_contents("https://blockchain.info/ticker");
        $res = json_decode($all);
        $currentRate = $res->USD->last;
        $price = Price::latest()->first();
        $allprice = Price::orderBy('id', 'ASC')->get();

        $user = User::sum('balance');
        $mval = (40000000-$user)/1000000;

        $banner = Slider::first();
        $about = About::first();
        $items = Testimonial::all();
        $times = Timeline::all();
        $service = Service::pluck('heading')->first();
        $icoagenda = Icoagenda::orderBy('start_date', 'Desc')->take(4)->get();
		
		 $Date = date('m/d/Y');
     $time = date('H:i');
     $mindate =   strtotime("$Date $time");  
     $icoagenda = Icoagenda::get();
	 
	 if(!$icoagenda->isEmpty()){
    $crates =0;
    foreach($icoagenda as $ico){
    $agendastartdate= $ico['start_date'];
    $agendalastdate= $ico['last_date'];  
    $agendastarttime = $ico['start_time'];
    $agendalasttime = $ico['last_time']; 
      
    $dstart_date_time = strtotime("$agendastartdate $agendastarttime");
    $dlast_date_time = strtotime("$agendalastdate $agendalasttime"); 
     
    $id= $ico['id'];
	  
	  $coin_payment = DB::table("coin_payment")
        ->select(DB::raw("SUM(amount) as count"))
        ->where('details',$id)
        ->get();
		
				
				
	  $diff = $ico['avl_total_exa']; 
  
  
  
      if(($mindate > $dstart_date_time && $diff ==0) || ($mindate > $dstart_date_time && $mindate >= $dlast_date_time)   ) {
		 
        $update =  icoagenda::where('id', $id)->update(['statuscomplted' => 2]);
          
      }elseif(($dstart_date_time > $mindate && $coin_payment->isEmpty()||($dstart_date_time > $mindate && $dlast_date_time > $mindate))){
          
         
		  
           $update =  icoagenda::where('id', $id)->update(['statuscomplted' => 3]);
      }elseif(($mindate >= $dstart_date_time && $diff > 0) ||($mindate > $dstart_date_time && $mindate  <= $dlast_date_time) ){
      
	   $update =  icoagenda::where('id', $id)->update(['statuscomplted' => 1]);
      }
         
  }
	 }
        
    	return view('front.index', compact('banner','times','allprice','currentRate','price','mval','about','items','service','icoagenda'));
    }

    public function page($id)
    {
    	$single = Menu::findorFail($id);
        $page = $single->name;
    	return view('front.single', compact('single','page'));
    }

    public function contact()
    {
        return view('front.contact');
    }

     public function conmail(Request $request)
    {
         $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'comment' => 'required',

        ]);

        $name = $request->name;
        $email = $request->email;
        $msg = $request->comment;
        send_email($email, $name, 'Visitor Messeage', $msg);

        return back()->with('success', 'Your Message sent succesfully');
    }

    public function register($reference)
        
    {
        die();
        return view('auth.register',compact('reference'));
    }

    public function unauthorized()
    {
        
        if(Auth::user()->status == '1' && Auth::user()->emailv == 1 && Auth::user()->smsv == 1)
        {
            return redirect('home');
        }
        else
        {
          return view('auth.notauthor');
        }

    }

    public function sendemailver()
    {
        $user = User::find(Auth::id());
        $useremail =$user->email;

        $chktm = $user->vsent+0;
        if ($chktm >time())
         {
            $delay = $chktm-time();
           return back()->with('alert', 'Please Try after '.$delay.' Seconds');
        }
        else
        {
            $code = str_random(8);
            $msg = 'Your Verification code is: '.$code;
            $user['vercode'] = $code ;
            $user['vsent'] = time();
            $user->save();
          
            $data =array(
              'email' => $useremail,
              'username'=> $user->username,
              'code'=> $code,
              'body' => 'Send Successfully'
          
            );
           Mail::send('emails.email', $data, function($message) use ($data) {
           $message->from('info@leigonphy.co');
           $message->to($data['email']);
           $message->subject('verfication code');
           });

         //dd('Mail Send Successfully');
            //die();
            //send_email($user->email, $user->username, 'Verification Code', $msg);
            return back()->with('success', 'Email verification code sent succesfully');
        }

    }
     public function sendsmsver()
    {
        $user = User::find(Auth::id());
        $chktm = $user->vsent+1000;
        if ($chktm >time())
         {
            $delay = $chktm-time();
           return back()->with('alert', 'Please Try after '.$delay.' Seconds');
        }
        else
        {
            $code = str_random(8);
            $sms =  'Your Verification code is: '.$code;
            $user['vercode'] = $code;
            $user['vsent'] = time();
            $user->save();

            send_sms($user->mobile, $sms);
            return back()->with('success', 'SMS verification code sent succesfully');
        }


    }

    public function emailverify(Request $request)
    {

        $this->validate($request, [
            'code' => 'required'
        ]);
        $user = User::find(Auth::id());

        $code = $request->code;
        if ($user->vercode == $code)
        {
           $user['emailv'] = 1;
           $user['vercode'] = str_random(10);
           $user['vsent'] = 0;
           $user->save();

            return redirect('home')->with('success', 'Email Verified');
        }
        else
        {
             return back()->with('alert', 'Wrong Verification Code');
        }

    }

     public function smsverify(Request $request)
    {

        $this->validate($request, [
            'code' => 'required'
        ]);
        $user = User::find(Auth::id());

        $code = $request->code;
        if ($user->vercode == $code)
        {
           $user['smsv'] = 1;
           $user['vercode'] = str_random(10);
           $user['vsent'] = 0;
           $user->save();

            return redirect('home')->with('success', 'SMS Verified');
        }
        else
        {
             return back()->with('alert', 'Wrong Verification Code');
        }

    }

    public function verify2fa()
    {
        return redirect(URL()->previous());
    }

    public function search(Request $request)
    {
         $this->validate($request, [
            'search' => 'required'
        ]);

         if(strlen($request->search) == 32)
         {
            $logs = Uwdlog::where('toacc', $request->search)->where('flag', '1')->get();
            if ($logs == null) 
            {
                return back()->with('alert', 'Nothing Found');
            }
            else
            {
               return view('front.result', compact('logs')); 
            }
            
         }
         elseif(strlen($request->search) == 40)
         {
           $logs = Uwdlog::where('trxid', $request->search)->get();
            if ($logs == null) 
            {
                return back()->with('alert', 'Nothing Found');
            }
            else
            {
               return view('front.result', compact('logs')); 
            }
         }
         else
         {
            return back()->with('alert', 'Nothing Found');
         }

       
    }

    public function cron()
    {
        $user = User::sum('balance');
        $charge = Charge::first();
        $tm = time()-7*24*3600;
// for ($i=0; $i < 800 ; $i++) {
// $tm = $tm+900;
        $base = $charge->basep;
        $var = $charge->varp;
        $now = $base+(($var*$user)/100);
        $add = rand(1,100)/100;
        $rate = $now+$add;
        $final = number_format(floatval($rate) , 2, '.', '');

        $price['price'] = $final;
        $price['created_at'] = date("Y-m-d H:i:s", $tm);
        Price::create($price);
// 2017-11-09 10:45:51

        // echo $price['created_at'];
// }
    }
}
