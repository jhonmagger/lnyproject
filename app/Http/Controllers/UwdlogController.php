<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Uwdlog;
use App\User;
use App\Deposit;
use App\Uaccount;
use App\Bitcoinaddress;
use App\Bitcoinhistory;
use App\Bitcoinsend;
use App\Coinpayment;
use App\Price;
use Illuminate\Support\Facades\DB;


class UwdlogController extends Controller
{
	public function userlog()
    {   
         $userlogs=  DB::table('users')->select('users.id','users.username','users.email','coin_payment.trxid','coin_payment.amount','coin_payment.inusd','coin_payment.status','coin_payment.created_at')->join('coin_payment','coin_payment.user_id','=','users.id')->where('coin_payment.status','1')->orderBy('coin_payment.user_id', 'desc')->get();

    	return view('admin.userlog.userlog', compact('userlogs'));
    }

       public function users()
       {   
    	
       
         $users = User::orderBy('id', 'desc')->paginate(10); 

    	return view('admin.userlog.users', compact('users'));
    }

    public function newusers()
    {
    	$users = User::where('status', '0')->orderBy('id', 'desc')->paginate(10);
    	return view('admin.userlog.newusers', compact('users'));
    }
     public function single($id)
    {    
    	$user = User::findorFail($id);
        $deposits = Deposit::where('user_id', $user['id'] )->sum('amount');

         $all = file_get_contents("https://blockchain.info/ticker");
        $res = json_decode($all);
        $currentRate = $res->USD->last;
       

        $price = Price::latest()->first();
        $allprice = Price::orderBy('id', 'ASC')->get();
        $tetracoin= Price::find(4);
        $tetracoinprice =$tetracoin->price;
       
       $data = DB::table("bitcoinhistory")
        ->select(DB::raw("SUM(value) as count"))
        ->where('user_id', '=', $id)
        ->where('main', '=', '1')
        ->get();
     $datas = DB::table("coin_payment")
        ->select(DB::raw("SUM(inusd) as count"))
        ->where('user_id', '=', $id)
        ->where('status', '=', '1')
        ->get();
  $datasbal = DB::table("coin_payment")
        ->select(DB::raw("SUM(amount) as count"))
        ->where('user_id', '=', $id)
        ->where('status', '=', '1')
        ->get();
 if(empty($data[0]->count)){
      $btcblance =0.00;
      }else{
     $btcblance =$data[0]->count; 
     } 
if(empty($datas[0]->count)){
$coinblance =0.00;
}else{

$coinblance =$datas[0]->count;
}
if(empty($datasbal[0]->count)){
$coinblances =0.00;
}else{

$coinblances =$datasbal[0]->count;
}  


    	return view('admin.userlog.single', compact('user','trans', 'deposits','btcblance','coinblance','coinblances','tetracoinprice','currentRate'));
    }

    public function blupdate(Request $request,$id)
    {
        
         $ntrc = str_random(16);

          $this->validate($request,
            [
                'addcoin' => 'required',
            ]);
        
        
        
			$deposit['user_id'] = $id;
			$deposit['amount'] = $request->addcoin;
			$deposit['inusd'] = 0;
			$deposit['charge'] = "0";
			$deposit['gateway_id'] = 5;
			$deposit['trxid'] = $ntrc;
			$deposit['status'] = 1;
            $deposit['details'] = 0;
        if($request->addcoin <= 0)
        {
            return back()->with('alert','Amount Should be Positive Number');
            exit();
        }else{
            Coinpayment::create($deposit); 
        
        return back()->withSuccess('Balance Added Successfuly');
        }
        
            
        
        
        
        
        
        

    }
    
     public function statupdate(Request $request,$id)
    {
    	$user = User::find($id);

        $this->validate($request,
            [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'postcode' => 'required|string|max:255',
            'country' => 'required|string|max:255',
            'mobile' => 'required|string|max:255',
            ]);

        $user['firstname'] = $request->firstname ;
        $user['lastname'] = $request->lastname ;
        $user['address'] = $request->address ;
        $user['city'] = $request->city ;
        $user['postcode'] = $request->postcode ;
        $user['country'] = $request->country ;
        $user['mobile'] = $request->mobile;
        $user['status'] = $request->status =="1" ?1:0;
        $user['docv'] = $request->docv =="1" ?1:0;
         
         
         if($request->emailv == null){
           $user['emailv'] = 0; 
         }else{
           $user['emailv'] = 1;
         }
          
        $user['smsv'] = $request->smsv =="1" ?1:0;
        $user['gtfa'] = $request->gtfa =="1" ?1:0;

        $user->save();

     

    	return back()->withSuccess('User Profile Updated Successfuly');
    }


    public function email($id)
    {
        $user = User::findorFail($id);
        return view('admin.userlog.email',compact('user'));
    }

    public function broadcast()
    {
        return view('admin.userlog.broadcast');
    }

    public function sendemail(Request $request)
    {
         $this->validate($request,
            [
                'emailto' => 'required|email',
                'reciver' => 'required',
                'subject' => 'required',
                'emailMessage' => 'required'
            ]);
         $to = $request->emailto;
         $name = $request->reciver;
         $subject = $request->subject;
         $message = $request->emailMessage;

         send_email($to, $name, $subject, $message);

        return back()->withSuccess('Mail Sent Successfuly');

    }

    public function broadcastemail(Request $request)
    {
        $this->validate($request,
            [
                'subject' => 'required',
                'emailMessage' => 'required'
            ]);

        $users = User::where('status', '1')->get();

        foreach ($users as $user)
        {

         $to = $user->email;
         $name = $user->firstname;
         $subject = $request->subject;
         $message = $request->emailMessage;

         send_email($to, $name, $subject, $message);
        }

        return back()->withSuccess('Mail Sent Successfuly');
    }

}
