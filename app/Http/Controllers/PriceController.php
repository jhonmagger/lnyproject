<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Price;
use App\Referralprice;

class PriceController extends Controller
{
    public function index()
    {
        
    	$prices = Price::all();
        
        if($prices == null)
        {
          return view('admin.policy.prices');
        
        }
     else{
    return view('admin.policy.prices', compact('prices'));
   }     
}




    public function update(Request $request, $id)
    {
     $prices = Price::findorFail($id);

     $prices['price'] = $request->price;

       $prices->save();

        return back()->with('success','Price Updated successfully.');
   }
 

    public function store(Request $request)
    {
        $this->validate($request,
            [
                'price' => 'required',
            ]);

        $price['price'] = $request->price;
        Price::create($price);

        return back()->with('success', 'New Price Created Successfully!');
    }


   




    public function destroy(Price $price)
    {
        $price->delete();

        return back()->with('success', 'Price Deleted Successfully!');
    }
    
     public function levelone()
    {
        
    	$levelone = Referralprice::all();
        
        if($levelone == null)
        {
          return view('admin.levelone.levelone');
        
        }
     else{
    return view('admin.levelone.levelone', compact('levelone'));
   }     
}
   
    public function leveltwo()
    {
        
    	$leveltwo = Referralprice::all();
        
        if($leveltwo == null)
        {
          return view('admin.levelone.leveltwo');
        
        }
     else{
    return view('admin.levelone.leveltwo', compact('leveltwo'));
   }     
}
    
    
    public function updatelevelone(Request $request, $id){
       $levelone = Referralprice::findorFail($id);

        $levelone['levelone'] = $request->levelone;

       $levelone->save();

        return back()->with('success','Price Updated successfully.');
        
    }
    
     public function updateleveltwo(Request $request, $id){
       $leveltwo = Referralprice::findorFail($id);

        $leveltwo['leveltwo'] = $request->leveltwo;

       $leveltwo->save();

        return back()->with('success','Price Updated successfully.');
        
    }
    
    
    
    


}
