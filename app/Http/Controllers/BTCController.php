<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input as input;
use Illuminate\Support\Facades\DB;
use Auth;
use Session;
use Hash;
use App\Price;
use App\Icoagenda;
use App\Coinpayment;
use App\Lib\GoogleAuthenticator;

class BTCController extends Controller
{

  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', '2fa', 'ckstatus']);
    }
    
     public function btc()
     {
        $all = file_get_contents("https://blockchain.info/ticker");
        $res = json_decode($all);
         $currentRate1 = $res->USD->last;


 echo  $currentRate = number_format($currentRate1, 2, '.', '');
    } 


     public function icocoin()

{
     //date_default_timezone_set('Asia/Kolkata');
       
     $Date = date('m/d/Y');
     $time = date('H:i');
     $mindate =   strtotime("$Date $time");  
     $icoagenda = Icoagenda::get();
	
	 if(!empty($icoagenda)){
    $crates =0;
    foreach($icoagenda as $ico){
    $agendastartdate= $ico['start_date'];
    $agendalastdate= $ico['last_date'];  
    $agendastarttime = $ico['start_time'];
    $agendalasttime = $ico['last_time']; 
      
    $dstart_date_time = strtotime("$agendastartdate $agendastarttime");
     $dlast_date_time = strtotime("$agendalastdate $agendalasttime"); 
     
    $id= $ico['id'];
	  
	  $coin_payment = DB::table("coin_payment")
        ->select(DB::raw("SUM(amount) as count"))
        ->where('details',$id)
        ->get();
		
				
				
	  $diff = $ico['avl_total_exa']; 
       if($ico['last_timestamp']==0){
      
         if($diff == 0)  {
		 
        $update =  icoagenda::where('id', $id)->update(['statuscomplted' => 2]);
          
      }elseif(($dstart_date_time > $mindate && $coin_payment->isEmpty()||($dstart_date_time > $mindate && $dlast_date_time > $mindate))){
          
         
		  
           $update =  icoagenda::where('id', $id)->update(['statuscomplted' => 3]);
      }elseif(($mindate >= $dstart_date_time && $diff > 0) ||($mindate > $dstart_date_time && $mindate  <= $dlast_date_time) ){
      
	  $update =  icoagenda::where('id', $id)->update(['statuscomplted' => 1]);
        }
            
            
        
        }else{
           
             if(($mindate > $dstart_date_time && $diff == 0) || ($mindate > $dstart_date_time && $mindate >= $dlast_date_time)  ) {
		 
        $update =  icoagenda::where('id', $id)->update(['statuscomplted' => 2]);
          
      }elseif(($dstart_date_time > $mindate && $coin_payment->isEmpty()||($dstart_date_time > $mindate && $dlast_date_time > $mindate))){
          
         
		  
           $update =  icoagenda::where('id', $id)->update(['statuscomplted' => 3]);
      }elseif(($mindate >= $dstart_date_time && $diff > 0) ||($mindate > $dstart_date_time && $mindate  <= $dlast_date_time) ){
      
	  $update =  icoagenda::where('id', $id)->update(['statuscomplted' => 1]);
        }
        
        }
         
  }
  
   $complete= Icoagenda::where('statuscomplted','=','3')->orderBy('id', 'asc')->limit(1)->get();
   $progress= Icoagenda::where('statuscomplted','=','1')->orderBy('id', 'asc')->limit(1)->get();

if (!$progress->isEmpty()) {
echo  $crates = $progress[0]->price;
 }elseif(!$complete->isEmpty()){
echo $crates = $complete[0]->price;
}else{
echo $crates =0.00;
}
 
	} 
}	
	
    
    public function totalrate(){
        
        $datas = DB::table("icoagenda")
        ->select(DB::raw("SUM(total_exa) as count"))
        ->where('status', '=', '1')
        ->get();
        
        if(empty($datas[0]->count)){
          echo $coinblance =0.00;
        }else{
          $coinblance =$datas[0]->count;
           echo $icoblance =number_format($datas[0]->count,2);
         }
        
        
    } 
    
    public function soldtetra(){
        
          $data = DB::table("coin_payment")
        ->select(DB::raw("SUM(amount) as count"))
        ->where('details','!=','0')
        ->where('status', '=', '1')
        ->get();
        
        if(empty($data[0]->count)){
         echo $btcblance =0.000;
         }else{

echo $btcblance =$data[0]->count;

            
} 
        
    }
  
    
    
    
   public function avalibale(){
        
      
       
 $complete= Icoagenda::where('statuscomplted','=','3')->orderBy('id', 'asc')->limit(1)->get();
  $progress= Icoagenda::where('statuscomplted','=','1')->orderBy('id', 'asc')->limit(1)->get();

if (!$progress->isEmpty()) {
$ids=$progress[0]->id;      
}elseif(!$complete->isEmpty()){
 $ids=$complete[0]->id;         
}else{
 $ids =0.00;    
}
       
       
       
       if(!empty($ids)){
       
        $data = DB::table("icoagenda")
        ->select(DB::raw("SUM(avl_total_exa) as count"))
        ->where('id', '=', $ids)
        ->get();
        
        if(empty($data[0]->count)){
         echo $mainbalce =0;
         }else{

echo $mainbalce =$data[0]->count;

            
} 
           }else{
         echo $mainbalce =0;
     }
       
        
        
        
        
    } 
    
    
    public function btcpricedeposit(){
        
       $data = DB::table("bitcoinhistory")
        ->select(DB::raw("SUM(value) as count"))
        ->where('user_id', '=', Auth::user()->id)
        ->where('main', '=', '1')
        ->get();
       $datas = DB::table("coin_payment")
        ->select(DB::raw("SUM(inusd) as count"))
        ->where('user_id', '=', Auth::user()->id)
        ->where('status', '=', '1')
        ->get();
        
        if(empty($data[0]->count)){
         $btcblance =0.00;
         }else{

$btcblance =$data[0]->count;
} 
if(empty($datas[0]->count)){
$coinblance =0.00;
}else{

$coinblance =$datas[0]->count;
} 
        $mainbal = $btcblance-$coinblance; 
   echo $mainbals  = sprintf('%f', (float)$mainbal);
        
    }
    
    
    
     public function icopricedeposit(){
         
         
        
        $all = file_get_contents("https://blockchain.info/ticker");
        $res = json_decode($all);
        $currentRate = $res->USD->last;
         $complete= Icoagenda::where('statuscomplted','=','3')->orderBy('id', 'asc')->limit(1)->get();
  $progress= Icoagenda::where('statuscomplted','=','1')->orderBy('id', 'asc')->limit(1)->get();

if (!$progress->isEmpty()) {
 $price = $progress[0]->price;
}elseif(!$complete->isEmpty()){
 $price = $complete[0]->price;
}else{
 $price =0.00;
}
        
         $data = DB::table("bitcoinhistory")
        ->select(DB::raw("SUM(value) as count"))
        ->where('user_id', '=', Auth::user()->id)
        ->where('main', '=', '1')
        ->get();
       $datas = DB::table("coin_payment")
        ->select(DB::raw("SUM(inusd) as count"))
        ->where('user_id', '=', Auth::user()->id)
        ->where('status', '=', '1')
        ->get();
        
        if(empty($data[0]->count)){
         $btcblance =0.00;
         }else{

$btcblance =$data[0]->count;
} 
if(empty($datas[0]->count)){
$coinblance =0.00;
}else{

$coinblance =$datas[0]->count;
} 
        $mainbal = $btcblance-$coinblance; 
      $mainbals  = sprintf('%f', (float)$mainbal);
        
        
         $tetracoinprice =$price;
        
        $cal = $tetracoinprice/$currentRate;
   
         if($cal!=0){
   $mincoin= ($mainbals)/$cal; 
    echo $totaltetracoins  = sprintf('%f', (float)$mincoin);
   }else{
     $mincoin= 0; 
    echo $totaltetracoins  = sprintf('%f', (float)$mincoin);
   }  
         
         
   
        
        
        
    }
    
    
    public function limtcoin(){
     $complete= Icoagenda::where('statuscomplted','=','3')->orderBy('id', 'asc')->limit(1)->get();
     $progress= Icoagenda::where('statuscomplted','=','1')->orderBy('id', 'asc')->limit(1)->get();    
         
   if (!$progress->isEmpty()) {
 $totals= $progress[0]->total_exa;
 $ids=$progress[0]->id; 
 $limit = $progress[0]->limit; 
$ico_name = $progress[0]->round_name;
$statuscomplted =$progress[0]->statuscomplted;       
}elseif(!$complete->isEmpty()){
 $totals= $complete[0]->total_exa;
  $ids=$complete[0]->id;
       $limit = $complete[0]->limit; 
$ico_name = $complete[0]->round_name; 
$statuscomplted =$complete[0]->statuscomplted;       
}else{
 $totals= 0.00;
$ids =0.00;  
       $limit = 0.00; 
$ico_name = 0.00; 
$statuscomplted =2;       
}
        if(!empty($ids)){
            
            $data = DB::table("coin_payment")
        ->select(DB::raw("SUM(amount) as count"))
        ->where('details', '=', $ids)
        ->where('status', '=', '1')
        ->where('user_id', '=', Auth::user()->id)         
        ->get();
            
            if(empty($data[0]->count)){
                echo $btcblance =0.000;
            echo ",";            
echo $totals; 
echo ","; 
echo $ids;
echo ",";
echo $limit;
echo ",";            
echo $ico_name;
echo ",";                  
echo $statuscomplted ;               
            }else{
                echo $btcblance =$data[0]->count;
echo ",";            
echo $totals; 
echo ","; 
echo $ids;            
echo ",";
echo $limit;
echo ",";
echo $ico_name;
echo ",";                  
echo $statuscomplted ;                 
            }
            
        }else{
           echo $btcblance =0;
   echo ",";         
echo $totals=0;
            echo ",";
echo $ids=0;
            echo ",";
            echo $limit =0;
echo ",";
echo $ico_name=0; 
echo ",";                  
echo $statuscomplted =2;             
        }
         
        
        
        
    
    }
    
  public function iconhistory(){
        
        $icoagenda = Icoagenda::where('status','=','1')->orderBy('start_date', 'asc')->get();
        foreach($icoagenda as $icoagendas){
            if($icoagendas->statuscomplted == 2){?>

          <tr><td><?php echo $icoagendas->start_date;?></td><td><?php echo $icoagendas->last_date;?></td><td><?php echo $icoagendas->total_exa;?></td><td><?php echo $icoagendas->price;?></td> <td><?php echo $icoagendas->limit;?>/tetra</td><td><a href="#" style="background: red;padding: 7px 12px 7px 12px;border-radius: 4px;color: white;text-decoration: none;">Completed</a></td></tr>

         <?php }else if($icoagendas->statuscomplted == 3){?>

            <tr><td><?php echo $icoagendas->start_date;?></td><td><?php echo $icoagendas->last_date;?></td><td><?php echo $icoagendas->total_exa;?></td><td><?php echo $icoagendas->price;?></td> <td><?php echo $icoagendas->limit;?>/tetra</td><td><a href="#" style="background: #333;padding: 7px 12px 7px 12px;border-radius: 4px;color: white;text-decoration: none;">UpComing</a></td></tr>
        <?php }else{?>
 <tr><td><?php echo $icoagendas->start_date;?></td><td><?php echo $icoagendas->last_date;?></td><td><?php echo $icoagendas->total_exa;?></td><td><?php echo $icoagendas->price;?></td> <td><?php echo $icoagendas->limit;?>/tetra</td><td><a href="#" style="background: #333;padding: 7px 12px 7px 12px;border-radius: 4px;color: white;text-decoration: none;">Progress</a></td></tr>
            
        <?php }
        }
        
    } 
    
 public function timercontrol(){
        $datemain = date('m/d/Y');
        $timemain = date("h:i:s");
       $Date = date('m/d/Y H:i:s');
       $currentdate =   strtotime("$Date"); 
	 $complete= Icoagenda::where('statuscomplted','=','3')->orderBy('id', 'asc')->limit(1)->get();
     $progress= Icoagenda::where('statuscomplted','=','1')->orderBy('id', 'asc')->limit(1)->get();
	 
	 if (!$progress->isEmpty()) {
		 $start_date= $progress[0]->start_date;
         $start_time= $progress[0]->start_time;
		 $last_date= $progress[0]->last_date;
         $last_time= $progress[0]->last_time;
		 
		 
          		 
		  $dstart_date_time = strtotime("$start_date $start_time");
		  
		 
          echo $maindstart_date_time =date('M d, Y G:i:s',$dstart_date_time);
		  echo "#";
		if($last_date==NULL){
			 
			echo $maindlast_date_time=0;
			 			 
		 }else{
			   $dlast_date_time = strtotime("$last_date $last_time");
			   echo $maindlast_date_time =date('M d, Y G:i:s',$dlast_date_time);
		 }
		 
		  echo "#";
		 echo $currentdate =   date('M d, Y G:i:s',$currentdate);  
		 
    }elseif(!$complete->isEmpty()){
		$start_date= $complete[0]->start_date;
         $start_time= $complete[0]->start_time;
		  $last_date= $complete[0]->last_date;
         $last_time= $complete[0]->last_time;
          		 
		  $dstart_date_time = strtotime("$start_date $start_time");
		  $dlast_date_time = strtotime("$last_date $last_time");
          echo $maindstart_date_time =date('M d, Y G:i:s',$dstart_date_time);
		  echo "#";
		 if($last_date==NULL){
			 
			echo $maindlast_date_time=0;
			 			 
		 }else{
			   $dlast_date_time = strtotime("$last_date $last_time");
			   echo $maindlast_date_time =date('M d, Y G:i:s',$dlast_date_time);
		 }
		 
		  echo "#";
		 echo $currentdate =   date('M d, Y G:i:s',$currentdate); 
    }else{
        
		
}

	
     
	 
	 
     
     
 }
    


}