<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input as input;
use Illuminate\Support\Facades\DB;
use Auth;
use Session;
use Hash;
use App\Price;
use App\Refer;
use App\User;
use App\Icoagenda;
use App\Coinpayment;
use App\Lib\GoogleAuthenticator;


class AddCoinController extends Controller
{

  /**
     * Create a new controller instance.
     *
     * @return void
     */
   
    
    public function index(){
       
       
        $refer=  DB::table('refer')->select('users.id','users.username','users.email','refer.point','refer.user_id')->join('users','users.id','=','refer.user_id')->where('refer.point', '!=' , '')->orWhere('refer.point', '!=' , 0)->orderBy('id', 'desc')->paginate(10); 
        
       
        if($refer== null){
            
            return view('admin.addcoin.index');
            
        }else{
             
            
          return view('admin.addcoin.index', compact('refer'));  
        
        }
       
        
       }
    
    public function referal($id){
       $user_id = $id;
       $refer = Refer::where('user_id', $user_id)->get();  
       
       return view('admin.addcoin.detail', compact('refer'));  
    }
    
    public function referalupdate(Request $request,$id){
            $ntrc = str_random(16);
            $deposit['user_id'] = $id;
			$deposit['amount'] = $request->amount;
			$deposit['inusd'] = 0;
			$deposit['charge'] = "0";
			$deposit['gateway_id'] = 5;
			$deposit['trxid'] = $ntrc;
			$deposit['status'] = 1;
            $deposit['details'] = 0;
         if($request->amount <= 0)
        {
            return back()->with('alert','Amount Should be Positive Number');
            exit();
        }else{
        Coinpayment::create($deposit); 
        $update =  Refer::where('user_id', $id)->update(['point' => '']);
        
        return redirect('admin/addcoin')->withSuccess('Balance Added Successfuly');     
        }
    }
   
    
}