<?php

namespace App\Http\Controllers;

use App\Gsetting;
use Illuminate\Http\Request;

class TeracoinController extends Controller
{


    public function index()
    {
        $gsettings = Gsetting::find(1);
       
        return view('admin.dashboard.teracoin', compact('gsettings'));
    }

    
 public function update(Request $request, $id)
    {
        $settings = Gsetting::find($id);

        $this->validate($request,
               [
                'btcrate' => 'required',
                ]);

        $settings['btcrate'] = $request->btcrate;
       

        $settings->save();

        return back()->with('success', 'Updated Successfully!');
    }

   

    
}
