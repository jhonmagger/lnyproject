<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Icoagenda;
use Session;
use Hash;
use Auth;
use App\Coinpayment;
use App\User;
 
class IcoagendaController extends Controller
{
    public function index()
    {
     
     $icoagenda = Icoagenda::orderBy('id', 'desc')->paginate(10);    
        
        if($icoagenda == null)
        {
          return view('admin.icoagenda.index');
        
        }
     else{
    return view('admin.icoagenda.index', compact('icoagenda'));
   }      
    }
    
    public function store(Request $request){
        
        $this->validate($request,
            [
                'ico_name' => 'required',
                'start_dates' => 'required',
                'start_times'=>'required',
                'total_tetra'=>'required',
                'price'=>'required',
                'limit' => 'required',
                'status' => 'required',
                
            ]);
        $user_id = Auth::user()->id;
		
        $icoagenda['user_id'] = $user_id;
        $icoagenda['round_name'] = $request->ico_name;
        $icoagenda['start_date'] = $request->start_dates;
         $icoagenda['last_date'] = $request->last_dates;
        $icoagenda['total_exa'] = $request->total_tetra;
        $icoagenda['avl_total_exa']= $request->total_tetra;
        $icoagenda['price'] = $request->price;
        $icoagenda['limit']= $request->limit;
        $icoagenda['status'] = $request->status;
        $icoagenda['statuscomplted'] =0;
        $icoagenda['start_time']= $request->start_times;
         $icoagenda['last_time'] =$request->last_times;
        $icoagenda['start_timestamp'] = strtotime("$request->start_dates $request->start_times");
         
       if ($request->last_dates && $request->last_times){
           $icoagenda['last_timestamp'] =  strtotime("$request->last_dates $request->last_times");
           
       }else{
            $icoagenda['last_timestamp'] =0;
       }
             
        
        
        
        
        if($request->total_tetra>=$request->limit){
        $icoagendas = Icoagenda::orderBy('id', 'desc')->paginate(10);

		
        if($icoagendas->isEmpty()){
			
         Icoagenda::create($icoagenda);
         return back()->with('success', 'New Ico Agenda Created Successfully!');
        }else{
             $dstart_date_time =array();
             $dlast_date_time =array();
            foreach($icoagendas as $icoagendass){
                 $start_date = $icoagendass->start_date;
                 $start_time = $icoagendass->start_time;
                 $last_date = $icoagendass->last_date;
                 $last_time = $icoagendass->last_time;
                
                 $dstart_date_time[] = strtotime("$start_date $start_time");
                 $dlast_date_time[] = strtotime("$last_date $last_time");
                 
            }
            
                 $start_date_time = strtotime("$request->start_dates $request->start_times");
                 $last_date_time = strtotime("$request->last_dates $request->last_times");
            
                    
          if(min($dstart_date_time) > $start_date_time || min($dstart_date_time) > $last_date_time ) {
              
               Icoagenda::create($icoagenda);
                   return back()->with('success', 'New Ico Agenda Created Successfully!');
           }elseif(max($dlast_date_time)<$start_date_time || max($dlast_date_time)<$last_date_time ){
                    
                     Icoagenda::create($icoagenda);
                   return back()->with('success', 'New Ico Agenda Created Successfully!'); 
                      
                 }
                else{
                    
                    
                        return back()->with('alert', 'Ico Agenda of this Date  already Created'); 
                        }
                  
                 
                }
          
            
            
           
        }else{
         return back()->with('alert', 'Limit is exceed then total Tetra');  
        }
        
    }
   
   public function destroy(icoagenda $icoagenda){
       
       $ico_id = $icoagenda->id;
       
       $ico_agenda = DB::table('icoagenda')->where('id', $ico_id)->delete();
       $coin_delet = Coinpayment::where('details', $ico_id)->get();
       
       
       if (!$coin_delet->isEmpty()) { 
           
           foreach($coin_delet as $coin){
            $coinvaleu= $coin->id;  
            $coin_delet = Coinpayment::where('id', $coinvaleu)->delete();   
               
           }
       }else{
           
       }
     
       
        
        return back()->with('success', 'Ico Agenda Deleted Successfully!');
   }
    
    
    
    
    
    
    
    
    
    
    public function edit ($id){
        $icoagenda	=	Icoagenda::find($id);
        
        return view('admin.icoagenda.edit', compact('icoagenda'));
		
       
    }
    
    public function update(Request $request, $id){
        
      $this->validate($request,
            [
               'start_dates' => 'required',
                'start_times'=>'required',
                'last_dates'=>'required',
                'last_times'=>'required',
                'total_tetra'=>'required',
                'price'=>'required',
                'limit' => 'required',
                'status' => 'required',
                
            ]);
        $user_id = Auth::user()->id;
        $icoagendaupdate		=	Icoagenda::find($id);
        $icoagendaupdate['user_id'] = $user_id;
        $icoagendaupdate['start_date'] = $request->start_dates;
        $icoagendaupdate['last_date'] = $request->last_dates;
        $icoagendaupdate['total_exa'] = $request->total_tetra;
        $icoagendaupdate['price'] = $request->price;
        $icoagendaupdate['limit']= $request->limit;
        $icoagendaupdate['status'] = $request->status;
        $icoagendaupdate['statuscomplted'] =3;
        $icoagendaupdate['start_time']= $request->start_times;
        $icoagendaupdate['last_time'] =$request->last_times;
        
        
        
        if($request->total_tetra>=$request->limit){
        $icoagendas = Icoagenda::orderBy('id', 'desc')->paginate(10);
          if($icoagendas == null){
         $icoagendaupdate->save();
         return back()->with('success', 'Ico Agenda Update Successfully!');    
        }else{
           foreach($icoagendas as $icoagendass){
                $start_date = $icoagendass->start_date;
                 $start_time = $icoagendass->start_time;
                 $last_date = $icoagendass->last_date;
                 $last_time = $icoagendass->last_time;
                
                 $dstart_date_time = strtotime("$start_date $start_time");
                 $dlast_date_time = strtotime("$last_date $last_time");
                 $start_date_time = strtotime("$request->start_dates $request->start_times");
                 $last_date_time = strtotime("$request->last_dates $request->last_times");
               
               if($dstart_date_time>$start_date_time && $dstart_date_time>$last_date_time){
                    $icoagendaupdate->save();
                   return back()->with('success', 'Ico Agenda Update Successfully'); 
               }
               elseif($dlast_date_time<$start_date_time && $dlast_date_time<$last_date_time ){
                    $icoagendaupdate->save();
                   return back()->with('success', 'Ico Agenda Update Successfully'); 
               }
               
               else{
                   
                  return redirect('http://leigonphy.co/admin/icoagenda')->with('alert', 'Some thing is wrong'); ;
               }

           }
              
          }
            
          }else{
         return back()->with('alert', 'Limit is exceed then total Tetra');  
        }  
            
        
    }
    
    
}