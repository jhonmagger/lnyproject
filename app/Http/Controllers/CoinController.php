<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input as input;
use Illuminate\Support\Facades\DB;
use Auth;
use App\User;
use App\Uwdlog;
use App\Bitcoinaddress;
use App\Bitcoinhistory;
use App\Bitcoinsend;
use App\Coinpayment;
use App\Withdraw;

use App\Referralprice;
use App\Wdmethod;
use App\Gateway;
use App\Gsetting;
use App\Deposit;
use App\Charge;
use Carbon\Carbon;
use App\Reference;
use App\Icoagenda;
use App\Upgrade;
use App\Avatar;
use App\Docver;
use App\Price;
use App\Refer;
use Session;
use Hash;
use App\Lib\GoogleAuthenticator;
use Illuminate\Support\Facades\Redirect;



class CoinController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware(['auth', '2fa', 'ckstatus']);
    }

    public static function homeVariables(){
$all = file_get_contents("https://blockchain.info/ticker");
      $res = json_decode($all);
      $currentRate = $res->USD->last;
      $data = DB::table("bitcoinhistory")
      ->select(DB::raw("SUM(value) as count"))
      ->where('user_id', '=', Auth::user()->id)
      ->where('main', '=', '1')
      ->get();
      $datas = DB::table("coin_payment")
      ->select(DB::raw("SUM(inusd) as count"))
      ->where('user_id', '=', Auth::user()->id)
      ->where('status', '=', '1')
      ->get();

      $ico = DB::table("icoagenda")
      ->select(DB::raw("SUM(total_exa) as count"))
      ->where('status', '=', '1')
      ->get(); 
     $translog = Coinpayment::get();



      if(empty($data[0]->count)){
        $btcblance =0.00;
      }else{

        $btcblance =$data[0]->count;
      } 
      if(empty($datas[0]->count)){
        $coinblance =0.00;
      }else{

        $coinblance =$datas[0]->count;

      } 

      if(empty($ico[0]->count)){
        $icoblance =number_format("000000",2);

      }else{

        $icoblance =number_format($ico[0]->count,2);
      } 

      $datas = DB::table("icoagenda")
      ->select(DB::raw("SUM(total_exa) as count"))
      ->where('status', '=', '1')
      ->get();

      if(empty($datas[0]->count)){
       $coinblance =0.00;
     }else{
      $coinblance =$datas[0]->count;
      $icoblance =number_format($datas[0]->count,2);
    }

    $data = DB::table("coin_payment")
    ->select(DB::raw("SUM(amount) as count"))
    ->where('details','!=','0')
    ->where('status', '=', '1')
    ->get();

    if(empty($data[0]->count)){
      $soldtetra =0.000;
    }else{

     $soldtetra =$data[0]->count;


   } 


   $complete= Icoagenda::where('statuscomplted','=','3')->orderBy('id', 'asc')->limit(1)->get();
   $progress= Icoagenda::where('statuscomplted','=','1')->orderBy('id', 'asc')->limit(1)->get();

   if (!$progress->isEmpty()) {
    $ids = $progress[0]->id;      
  }elseif(!$complete->isEmpty()){
   $ids = $complete[0]->id;
 }else{

  $ids =0;
}

if(empty($ids)){

 $avilable = 0;

}else{
  $data = DB::table("icoagenda")
  ->select(DB::raw("SUM(avl_total_exa) as count"))
  ->where('id', '=', $ids)
  ->get();

  if(empty($data[0]->count)){
    $avilable= 0;
  }else{

    $avilable =$data[0]->count;

  } 
}  







$data = DB::table("bitcoinhistory")
->select(DB::raw("SUM(value) as count"))
->where('user_id', '=', Auth::user()->id)
->where('main', '=', '1')
->get();
$datas = DB::table("coin_payment")
->select(DB::raw("SUM(inusd) as count"))
->where('user_id', '=', Auth::user()->id)
->where('status', '=', '1')
->get();

if(empty($data[0]->count)){
 $btcblance =0.00;
}else{

  $btcblance =$data[0]->count;
} 
if(empty($datas[0]->count)){
  $coinblance =0.00;
}else{

  $coinblance =$datas[0]->count;
} 
$mainbal = $btcblance-$coinblance; 
$mainbtcbal  = sprintf('%f', (float)$mainbal); 
$all = file_get_contents("https://blockchain.info/ticker");
$res = json_decode($all);
$currentRate = $res->USD->last;

$complete= Icoagenda::where('statuscomplted','=','3')->orderBy('id', 'asc')->limit(1)->get();
$progress= Icoagenda::where('statuscomplted','=','1')->orderBy('id', 'asc')->limit(1)->get();

$prices =0;        
if (!$progress->isEmpty()) {
  $prices = $progress[0]->price;
  $ids= $progress[0]->id;    
}elseif(!$complete->isEmpty()){
  $prices = $complete[0]->price;
  $ids= $complete[0]->id;    
}else{
 $prices =0.00;
 $ids =0;    
}       
$data = DB::table("bitcoinhistory")
->select(DB::raw("SUM(value) as count"))
->where('user_id', '=', Auth::user()->id)
->where('main', '=', '1')
->get();
$datas = DB::table("coin_payment")
->select(DB::raw("SUM(inusd) as count"))
->where('user_id', '=', Auth::user()->id)
->where('status', '=', '1')
->get();

if(empty($data[0]->count)){
 $btcblance =0.00;
}else{

  $btcblance =$data[0]->count;
} 
if(empty($datas[0]->count)){
  $coinblance =0.00;
}else{

  $coinblance =$datas[0]->count;
} 
$mainbal = $btcblance-$coinblance; 
$mainbals  = sprintf('%f', (float)$mainbal);
$tetracoinprice =$prices;
$cal = $tetracoinprice/$currentRate;
if($cal!=0){
  $mincoin= ($mainbals)/$cal; 
  $totaltetracoins  = sprintf('%f', (float)$mincoin);
}else{
  $mincoin= 0; 
  $totaltetracoins  = sprintf('%f', (float)$mincoin);
}    
  $datasbal = DB::table("coin_payment")
        ->select(DB::raw("SUM(amount) as count"))
        ->where('user_id', '=', Auth::user()->id)
        ->where('status', '=', '1')
        ->get();
        $coinblances =$datasbal[0]->count;

$complete= Icoagenda::where('statuscomplted','=','3')->orderBy('id', 'asc')->limit(1)->get();
$progress= Icoagenda::where('statuscomplted','=','1')->orderBy('id', 'asc')->limit(1)->get();


if (!$progress->isEmpty()) {
  $limit = $progress[0]->limit;
  $totals= $progress[0]->total_exa;
  $ids= $progress[0]->id; 
  $ico_name = $progress[0]->round_name;    
}elseif(!$complete->isEmpty()){
  $limit = $complete[0]->limit;
  $totals= $complete[0]->total_exa;
  $ids= $complete[0]->id; 
  $ico_name = $complete[0]->round_name;    
}else{
  $limit = 0;
  $totals= 0;
  $ids= 0; 
  $ico_name = 0;  
}          
if(!empty($ids)){
  $data = DB::table("coin_payment")
  ->select(DB::raw("SUM(amount) as count"))
  ->where('details', '=', $ids)
  ->where('status', '=', '1')
  ->where('user_id', '=', Auth::user()->id)         
  ->get();
  if(empty($data[0]->count)){
    $hiddenbtcblance =0.000;
    $totals; 
    $ids;
    $ico_name;
    $limit; 
    $perday_user_bal = $limit - $hiddenbtcblance;    
  }else{
    $hiddenbtcblance =$data[0]->count;
    $totals; 
    $ids; 
    $ico_name;            
    $limit;
    $perday_user_bal = $limit - $hiddenbtcblance;

  } 
}else{
  $hiddenbtcblance =0;
  $totals=0; 
  $ids=0; 
  $ico_name=0;
  $limit=0;
  $perday_user_bal = $limit - $hiddenbtcblance;           
}






$datemain = date('m/d/Y');
$timemain = date("h:i:s");
$Date = date('m/d/Y H:i:s');
$currentdate =   strtotime("$Date"); 
$complete= Icoagenda::where('statuscomplted','=','3')->orderBy('id', 'asc')->limit(1)->get();
$progress= Icoagenda::where('statuscomplted','=','1')->orderBy('id', 'asc')->limit(1)->get();

if (!$progress->isEmpty()) {
 $start_date= $progress[0]->start_date;
 $start_time= $progress[0]->start_time;
 $last_date= $progress[0]->last_date;
 $last_time= $progress[0]->last_time;



 $dstart_date_time = strtotime("$start_date $start_time");


 $maindstart_date_time =date('M d, Y G:i:s',$dstart_date_time);

 if($last_date==NULL){

  $maindlast_date_time=0;

}else{
  $dlast_date_time = strtotime("$last_date $last_time");
  $maindlast_date_time =date('M d, Y G:i:s',$dlast_date_time);
}


$maincurrentdate =   date('M d, Y G:i:s',$currentdate);  

}elseif(!$complete->isEmpty()){
  $start_date= $complete[0]->start_date;
  $start_time= $complete[0]->start_time;
  $last_date= $complete[0]->last_date;
  $last_time= $complete[0]->last_time;

  $dstart_date_time = strtotime("$start_date $start_time");
  $dlast_date_time = strtotime("$last_date $last_time");
  $maindstart_date_time =date('M d, Y G:i:s',$dstart_date_time);

  if($last_date==NULL){

    $maindlast_date_time=0;

  }else{
    $dlast_date_time = strtotime("$last_date $last_time");
    $maindlast_date_time =date('M d, Y G:i:s',$dlast_date_time);
  }


  $maincurrentdate =   date('M d, Y G:i:s',$currentdate); 
}else{


}

$complete= Icoagenda::where('statuscomplted','=','3')->orderBy('id', 'asc')->limit(1)->get();
$progress= Icoagenda::where('statuscomplted','=','1')->orderBy('id', 'asc')->limit(2)->get();

if (!$progress->isEmpty()) {
 $statuscomplted = $progress[0]->statuscomplted;
}elseif(!$complete->isEmpty()){
  $statuscomplted = $complete[0]->statuscomplted;
}else{
 $statuscomplted =2;

}



$btc_address = Coinpayment::where('user_id', Auth::user()->id)->where('status','1')->get();
$icoagendasss = Icoagenda::where('status','=','1')->orderBy('start_date', 'asc')->get();
$icoagendassss = Icoagenda::where('status','=','1')->orderBy('start_date', 'asc')->limit(2)->get();

$auzArray = compact('currentRate','btcblance','coinblance','totaltetracoins','btc_address','totaltetracoin','icoagendassss'
  ,'icoagendasss','icoblance','icoblance','soldtetra','avilable','mainbtcbal','totaltetracoin','hiddenbtcblance','ids','totals','ico_name','limit','perday_user_bal',
  'maindstart_date_time','maindlast_date_time','maincurrentdate','statuscomplted','translog','coinblances');
return $auzArray;


    }

    public function coinbuy()
    {



      $all = file_get_contents("https://blockchain.info/ticker");
      $res = json_decode($all);
      $currentRate = $res->USD->last;
      $data = DB::table("bitcoinhistory")
      ->select(DB::raw("SUM(value) as count"))
      ->where('user_id', '=', Auth::user()->id)
      ->where('main', '=', '1')
      ->get();
      $datas = DB::table("coin_payment")
      ->select(DB::raw("SUM(inusd) as count"))
      ->where('user_id', '=', Auth::user()->id)
      ->where('status', '=', '1')
      ->get();

      $ico = DB::table("icoagenda")
      ->select(DB::raw("SUM(total_exa) as count"))
      ->where('status', '=', '1')
      ->get(); 
     $translog = Coinpayment::get();



      if(empty($data[0]->count)){
        $btcblance =0.00;
      }else{

        $btcblance =$data[0]->count;
      } 
      if(empty($datas[0]->count)){
        $coinblance =0.00;
      }else{

        $coinblance =$datas[0]->count;

      } 

      if(empty($ico[0]->count)){
        $icoblance =number_format("000000",2);

      }else{

        $icoblance =number_format($ico[0]->count,2);
      } 

      $datas = DB::table("icoagenda")
      ->select(DB::raw("SUM(total_exa) as count"))
      ->where('status', '=', '1')
      ->get();

      if(empty($datas[0]->count)){
       $coinblance =0.00;
     }else{
      $coinblance =$datas[0]->count;
      $icoblance =number_format($datas[0]->count,2);
    }

    $data = DB::table("coin_payment")
    ->select(DB::raw("SUM(amount) as count"))
    ->where('details','!=','0')
    ->where('status', '=', '1')
    ->get();

    if(empty($data[0]->count)){
      $soldtetra =0.000;
    }else{

     $soldtetra =$data[0]->count;


   } 


   $complete= Icoagenda::where('statuscomplted','=','3')->orderBy('id', 'asc')->limit(1)->get();
   $progress= Icoagenda::where('statuscomplted','=','1')->orderBy('id', 'asc')->limit(1)->get();

   if (!$progress->isEmpty()) {
    $ids = $progress[0]->id;      
  }elseif(!$complete->isEmpty()){
   $ids = $complete[0]->id;
 }else{

  $ids =0;
}

if(empty($ids)){

 $avilable = 0;

}else{
  $data = DB::table("icoagenda")
  ->select(DB::raw("SUM(avl_total_exa) as count"))
  ->where('id', '=', $ids)
  ->get();

  if(empty($data[0]->count)){
    $avilable= 0;
  }else{

    $avilable =$data[0]->count;

  } 
}  







$data = DB::table("bitcoinhistory")
->select(DB::raw("SUM(value) as count"))
->where('user_id', '=', Auth::user()->id)
->where('main', '=', '1')
->get();
$datas = DB::table("coin_payment")
->select(DB::raw("SUM(inusd) as count"))
->where('user_id', '=', Auth::user()->id)
->where('status', '=', '1')
->get();

if(empty($data[0]->count)){
 $btcblance =0.00;
}else{

  $btcblance =$data[0]->count;
} 
if(empty($datas[0]->count)){
  $coinblance =0.00;
}else{

  $coinblance =$datas[0]->count;
} 
$mainbal = $btcblance-$coinblance; 
$mainbtcbal  = sprintf('%f', (float)$mainbal); 
$all = file_get_contents("https://blockchain.info/ticker");
$res = json_decode($all);
$currentRate = $res->USD->last;

$complete= Icoagenda::where('statuscomplted','=','3')->orderBy('id', 'asc')->limit(1)->get();
$progress= Icoagenda::where('statuscomplted','=','1')->orderBy('id', 'asc')->limit(1)->get();

$prices =0;        
if (!$progress->isEmpty()) {
  $prices = $progress[0]->price;
  $ids= $progress[0]->id;    
}elseif(!$complete->isEmpty()){
  $prices = $complete[0]->price;
  $ids= $complete[0]->id;    
}else{
 $prices =0.00;
 $ids =0;    
}       
$data = DB::table("bitcoinhistory")
->select(DB::raw("SUM(value) as count"))
->where('user_id', '=', Auth::user()->id)
->where('main', '=', '1')
->get();
$datas = DB::table("coin_payment")
->select(DB::raw("SUM(inusd) as count"))
->where('user_id', '=', Auth::user()->id)
->where('status', '=', '1')
->get();

if(empty($data[0]->count)){
 $btcblance =0.00;
}else{

  $btcblance =$data[0]->count;
} 
if(empty($datas[0]->count)){
  $coinblance =0.00;
}else{

  $coinblance =$datas[0]->count;
} 
$mainbal = $btcblance-$coinblance; 
$mainbals  = sprintf('%f', (float)$mainbal);
$tetracoinprice =$prices;
$cal = $tetracoinprice/$currentRate;
if($cal!=0){
  $mincoin= ($mainbals)/$cal; 
  $totaltetracoins  = sprintf('%f', (float)$mincoin);
}else{
  $mincoin= 0; 
  $totaltetracoins  = sprintf('%f', (float)$mincoin);
}    
  $datasbal = DB::table("coin_payment")
        ->select(DB::raw("SUM(amount) as count"))
        ->where('user_id', '=', Auth::user()->id)
        ->where('status', '=', '1')
        ->get();
        $coinblances =$datasbal[0]->count;

$complete= Icoagenda::where('statuscomplted','=','3')->orderBy('id', 'asc')->limit(1)->get();
$progress= Icoagenda::where('statuscomplted','=','1')->orderBy('id', 'asc')->limit(1)->get();


if (!$progress->isEmpty()) {
  $limit = $progress[0]->limit;
  $totals= $progress[0]->total_exa;
  $ids= $progress[0]->id; 
  $ico_name = $progress[0]->round_name;    
}elseif(!$complete->isEmpty()){
  $limit = $complete[0]->limit;
  $totals= $complete[0]->total_exa;
  $ids= $complete[0]->id; 
  $ico_name = $complete[0]->round_name;    
}else{
  $limit = 0;
  $totals= 0;
  $ids= 0; 
  $ico_name = 0;  
}          
if(!empty($ids)){
  $data = DB::table("coin_payment")
  ->select(DB::raw("SUM(amount) as count"))
  ->where('details', '=', $ids)
  ->where('status', '=', '1')
  ->where('user_id', '=', Auth::user()->id)         
  ->get();
  if(empty($data[0]->count)){
    $hiddenbtcblance =0.000;
    $totals; 
    $ids;
    $ico_name;
    $limit; 
    $perday_user_bal = $limit - $hiddenbtcblance;    
  }else{
    $hiddenbtcblance =$data[0]->count;
    $totals; 
    $ids; 
    $ico_name;            
    $limit;
    $perday_user_bal = $limit - $hiddenbtcblance;

  } 
}else{
  $hiddenbtcblance =0;
  $totals=0; 
  $ids=0; 
  $ico_name=0;
  $limit=0;
  $perday_user_bal = $limit - $hiddenbtcblance;           
}






$datemain = date('m/d/Y');
$timemain = date("h:i:s");
$Date = date('m/d/Y H:i:s');
$currentdate =   strtotime("$Date"); 
$complete= Icoagenda::where('statuscomplted','=','3')->orderBy('id', 'asc')->limit(1)->get();
$progress= Icoagenda::where('statuscomplted','=','1')->orderBy('id', 'asc')->limit(1)->get();

if (!$progress->isEmpty()) {
 $start_date= $progress[0]->start_date;
 $start_time= $progress[0]->start_time;
 $last_date= $progress[0]->last_date;
 $last_time= $progress[0]->last_time;



 $dstart_date_time = strtotime("$start_date $start_time");


 $maindstart_date_time =date('M d, Y G:i:s',$dstart_date_time);

 if($last_date==NULL){

  $maindlast_date_time=0;

}else{
  $dlast_date_time = strtotime("$last_date $last_time");
  $maindlast_date_time =date('M d, Y G:i:s',$dlast_date_time);
}


$maincurrentdate =   date('M d, Y G:i:s',$currentdate);  

}elseif(!$complete->isEmpty()){
  $start_date= $complete[0]->start_date;
  $start_time= $complete[0]->start_time;
  $last_date= $complete[0]->last_date;
  $last_time= $complete[0]->last_time;

  $dstart_date_time = strtotime("$start_date $start_time");
  $dlast_date_time = strtotime("$last_date $last_time");
  $maindstart_date_time =date('M d, Y G:i:s',$dstart_date_time);

  if($last_date==NULL){

    $maindlast_date_time=0;

  }else{
    $dlast_date_time = strtotime("$last_date $last_time");
    $maindlast_date_time =date('M d, Y G:i:s',$dlast_date_time);
  }


  $maincurrentdate =   date('M d, Y G:i:s',$currentdate); 
}else{


}

$complete= Icoagenda::where('statuscomplted','=','3')->orderBy('id', 'asc')->limit(1)->get();
$progress= Icoagenda::where('statuscomplted','=','1')->orderBy('id', 'asc')->limit(2)->get();

if (!$progress->isEmpty()) {
 $statuscomplted = $progress[0]->statuscomplted;
}elseif(!$complete->isEmpty()){
  $statuscomplted = $complete[0]->statuscomplted;
}else{
 $statuscomplted =2;

}



$btc_address = Coinpayment::where('user_id', Auth::user()->id)->where('status','1')->get();
$icoagendasss = Icoagenda::where('status','=','1')->orderBy('start_date', 'asc')->get();
$icoagendassss = Icoagenda::where('status','=','1')->orderBy('start_date', 'asc')->limit(2)->get();




return view('front.user.coinbuy', compact('currentRate','btcblance','coinblance','totaltetracoins','btc_address','totaltetracoin','icoagendassss'
  ,'icoagendasss','icoblance','icoblance','soldtetra','avilable','mainbtcbal','totaltetracoin','hiddenbtcblance','ids','totals','ico_name','limit','perday_user_bal',
  'maindstart_date_time','maindlast_date_time','maincurrentdate','statuscomplted','translog','coinblances'));


}

public function coinconfirm(Request $request)
{

 $icoagendas = Icoagenda::where('id', $request->icoagenda)->get();
 $total_amount = $icoagendas[0]->avl_total_exa;
 $req_amount = $request->amount;
 $update_amnt=$total_amount - $req_amount;
 $this->validate($request,
   [
    'amount' => 'required',
  ]);

 if (($request->amount <= 0 && $request->balance <=0) || ($update_amnt < 0 ) )
 {
  return back()->with('alert', 'Invalid Amount Please try again');     

}else{
 icoagenda::where('id', $request->icoagenda)->update(['avl_total_exa' => $update_amnt]);


 $ntrc = str_random(16);

 $deposit['user_id'] = Auth::user()->id;
 $deposit['amount'] = $request->amount;
 $deposit['inusd'] = $request->balance;
 $deposit['charge'] = "0";
 $deposit['gateway_id'] = 5;
 $deposit['trxid'] = $ntrc;
 $deposit['status'] = 1;
 $deposit['details'] = $request->icoagenda;


 Coinpayment::create($deposit);

 $sql1 = Refer::where('user_id', '=', Auth::user()->id)->get();  

 $parent=$sql1[0]->parent;



 if ($parent == '') {

 }else{

   $sql_parent = Refer::where('r_link', '=', $parent)->get(); 
   $sql_parents = Refer::where('parent', '=', $parent)->get();
   $refer_percetage = Referralprice::get();      
   $count = count($sql_parents);

   if($count<20){

     $parent_id=$sql_parent[0]->user_id;
     $parent_point = $sql1[0]->point;
     $parent_points = $sql_parent[0]->point;
     $percent = $refer_percetage[0]->levelone;
     $buy_price = $request->amount;
     $percentage = ($percent / 100) * $buy_price;

     if ($parent_point == null || $parent_point=0) { 
       $updates =  Refer::where('user_id', $parent_id)->update(['point' => $percentage]);
     }else{
       $update_point= $parent_points + $percentage;
       $updates =  Refer::where('user_id', $parent_id)->update(['point' => $update_point]);
     }





   }else{

     $parent_id=$sql_parent[0]->user_id;
     $parent_point = $sql1[0]->point;
     $parent_points = $sql_parent[0]->point;
     $percent = $refer_percetage[0]->leveltwo;
     $buy_price = $request->amount;
     $percentage = ($percent / 100) * $buy_price;

     if ($parent_point == null || $parent_point=0){
       $updates =  Refer::where('user_id', $parent_id)->update(['point' => $percentage]); 


     }else{
      $update_point = $parent_points + $percentage;
      $updates =  Refer::where('user_id', $parent_id)->update(['point' => $update_point]);       
    } 
  }       







}     

          //







$Date = date('m/d/Y');
$time = date('H:i');
$mindate =   strtotime("$Date $time");  
$icoagenda = Icoagenda::where('id', $request->icoagenda)->get();
if(!$icoagenda->isEmpty()){
  $agendastartdate= $icoagenda[0]->start_date;
  $agendalastdate= $icoagenda[0]->last_date;  
  $agendastarttime = $icoagenda[0]->start_time;
  $agendalasttime = $icoagenda[0]->last_time; 
  $dstart_date_time = strtotime("$agendastartdate $agendastarttime");
  $dlast_date_time = strtotime("$agendalastdate $agendalasttime"); 

  $id= $icoagenda[0]->id;

  $coin_payment = DB::table("coin_payment")
  ->select(DB::raw("SUM(amount) as count"))
  ->where('details',$id)
  ->get();


  $diff = $icoagenda[0]->total_exa - $coin_payment[0]->count;
  
  if(($mindate > $dstart_date_time && $diff ==0) || ($mindate > $dstart_date_time && $mindate >= $dlast_date_time)) {

    $update =  icoagenda::where('id', $id)->update(['statuscomplted' => 2]);

  }elseif(($dstart_date_time > $mindate && $coin_payment->isEmpty()||($dstart_date_time > $mindate && $dlast_date_time > $mindate))){

   $update =  icoagenda::where('id', $id)->update(['statuscomplted' => 3]);
 }elseif(($mindate >= $dstart_date_time && $diff > 0) ||($mindate > $dstart_date_time && $mindate  <= $dlast_date_time) ){

  $update =  icoagenda::where('id', $id)->update(['statuscomplted' => 1]);
}

}




            //die();    




return back()->with('Success', 'Thank you for purcahse ico coin');

} 

}

public function coinupdate(Request $request)
{
 $tx_hash = $request->tx_hash; 
 if($tx_hash){
   $update =  Coinpayment::where('trxid', $tx_hash)->update(['status' => 1]);
   return view('front.user.thanks');

 }
 else{
   return view('front.user.wrong');
 }




}


}
