<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input as input;
use Illuminate\Support\Facades\DB;
use Auth;
use App\User;
use App\Uwdlog;
use App\Bitcoinaddress;
use App\Bitcoinhistory;
use App\Bitcoinsend;
use App\Coinpayment;
use App\Withdraw;

use App\Wdmethod;
use App\Gateway;
use App\Gsetting;
use App\Deposit;
use App\Charge;
use Carbon\Carbon;
use App\Reference;
use App\Upgrade;
use App\Avatar;
use App\Docver;
use App\Price;
use App\Refer;
use Session;
use App\Http\Controllers\CoinController;
use Hash;
use App\Icoagenda;
use App\Lib\GoogleAuthenticator;
use Google2FA;
use Notification;
use App\Notifications\UserSupportContact;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', '2fa', 'ckstatus']);
    }

    public function index()
    {
        //date_default_timezone_set('Asia/Kolkata');

        $Date      = date('m/d/Y');
        $time      = date('H:i');
        $mindate   = strtotime("$Date $time");
        $icoagenda = Icoagenda::get();
        $translog  = Coinpayment::get();

        if (!$icoagenda->isEmpty()) {
            $crates = 0;
            foreach ($icoagenda as $ico) {
                $agendastartdate = $ico['start_date'];
                $agendalastdate  = $ico['last_date'];
                $agendastarttime = $ico['start_time'];
                $agendalasttime  = $ico['last_time'];

                $dstart_date_time = strtotime("$agendastartdate $agendastarttime");
                $dlast_date_time  = strtotime("$agendalastdate $agendalasttime");

                $id = $ico['id'];

                $coin_payment = DB::table("coin_payment")
                    ->select(DB::raw("SUM(amount) as count"))
                    ->where('details', $id)
                    ->get();

                $diff = $ico['avl_total_exa'];

                if (($mindate > $dstart_date_time && $diff == 0) || ($mindate > $dstart_date_time && $mindate >= $dlast_date_time)) {
                    $update = icoagenda::where('id', $id)->update(['statuscomplted' => 2]);
                } elseif (($dstart_date_time > $mindate && $coin_payment->isEmpty() || ($dstart_date_time > $mindate && $dlast_date_time > $mindate))) {
                    $update = icoagenda::where('id', $id)->update(['statuscomplted' => 3]);
                } elseif (($mindate >= $dstart_date_time && $diff > 0) || ($mindate > $dstart_date_time && $mindate <= $dlast_date_time)) {
                    $update = icoagenda::where('id', $id)->update(['statuscomplted' => 1]);
                }
            }
        }

        //one tetra value
        $complete = Icoagenda::where('statuscomplted', '=', '3')->orderBy('id', 'asc')->limit(1)->get();
        $progress = Icoagenda::where('statuscomplted', '=', '1')->orderBy('id', 'asc')->limit(1)->get();

        if (!$progress->isEmpty()) {
            $price = $progress[0]->price;
        } elseif (!$complete->isEmpty()) {
            $price = $complete[0]->price;
        } else {
            $price = 0.00;
        }
        $btc_addresss = Bitcoinaddress::where('user_id', Auth::user()->id)->where('address_type', 'BTC')->get();

//btc address
        if (empty($btc_addresss[0])) {
            $btc_address = '';
        } else {
            $btc_address = $btc_addresss[0]->bitcoin_address;
        }
        //btc current rate

        $all         = file_get_contents("https://blockchain.info/ticker");
        $res         = json_decode($all);
        $currentRate = $res->USD->last;

//BTC balance

        $data  = DB::table("bitcoinhistory")
            ->select(DB::raw("SUM(value) as count"))
            ->where('user_id', '=', Auth::user()->id)
            ->where('main', '=', '1')
            ->get();
        $datas = DB::table("coin_payment")
            ->select(DB::raw("SUM(inusd) as count"))
            ->where('user_id', '=', Auth::user()->id)
            ->where('status', '=', '1')
            ->get();

        if (empty($data[0]->count)) {
            $btcblance = 0.00;
        } else {
            $btcblance = $data[0]->count;
        }
        if (empty($datas[0]->count)) {
            $coinblance = 0.00;
        } else {
            $coinblance = $datas[0]->count;
        }
        //for coin balance

        $datasbal = DB::table("coin_payment")
            ->select(DB::raw("SUM(amount) as count"))
            ->where('user_id', '=', Auth::user()->id)
            ->where('status', '=', '1')
            ->get();
        if (empty($datasbal[0]->count)) {
            $coinblances = 0.00;
        } else {
            $coinblancess = $datasbal[0]->count;
            $coinblances  = sprintf('%f', (float)$coinblancess);
        }
//my tetra wallet

        $mainbal  = $btcblance - $coinblance;
        $mainbals = sprintf('%f', (float)$mainbal);

        $tetracoinprice = $price;

        $cal = $tetracoinprice / $currentRate;
        if ($cal != 0) {
            $mincoin        = ($mainbals) / $cal;
            $totaltetracoin = sprintf('%f', (float)$mincoin);
        } else {
            $mincoin        = 0;
            $totaltetracoin = sprintf('%f', (float)$mincoin);
        }

        //agenda
        $icoagenda = Icoagenda::orderBy('start_date', 'ASC')->take(6)->get();
        $allprice  = Price::orderBy('id', 'ASC')->get();
        $refer     = Refer::where('user_id', '=', Auth::user()->id)->get();

        $nuevaVariable = CoinController::homeVariables();

//dd($nuevaVariable); exit;

        return view('home', compact('btc_address', 'currentRate', 'price', 'btcblance', 'coinblance', 'coinblances', 'totaltetracoin', 'icoagenda', 'allprice', 'refer', 'translog', 'nuevaVariable'));
    }

    public function convert()
    {
        $all         = file_get_contents("https://blockchain.info/ticker");
        $res         = json_decode($all);
        $currentRate = $res->USD->last;
        $price       = Price::latest()->first();

        $btusd  = Auth::user()->bitcoin * $currentRate;
        $nusd   = Auth::user()->balance * $price->price;
        $totusd = $btusd + $nusd;

        return view('front.user.convert', compact('currentRate', 'price', 'btusd', 'nusd', 'totusd'));
    }

    public function transactions()
    {
        $trans = Uwdlog::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->paginate(10);
        return view('front.user.trans', compact('trans'));
    }

    public function bittrans()
    {
        $all         = file_get_contents("https://blockchain.info/ticker");
        $res         = json_decode($all);
        $currentRate = $res->USD->last;
        $gateways    = Gateway::find(7);

        $merchant_address = $gateways->val1;
        $user_id          = Auth::user()->id;
        $user_email       = Auth::user()->email;
        $user_name        = Auth::user()->username;
        $Bitcoinaddress   = Bitcoinaddress::where('user_id', Auth::user()->id)->where('address_type', 'BTC')->first();
        if (empty($Bitcoinaddress)) {
            $result = file_get_contents(url('/') . "/php-client/sample/address-api/GenerateAddressBtcTest3.php");

            $btc_address   = urldecode($result);
            $a_res         = json_decode($btc_address, true);
            $private_key   = $a_res['private'];
            $address_btc   = $a_res['address'];
            $public_key    = $a_res['public'];
            $address_label = "btcaddress";
            $insert        = DB::table('bitcoinaddress')->insert(array(
                'user_id' => $user_id,
                'address_label' => $address_label,
                'bitcoin_address' => $address_btc,
                'private_key' => $private_key,
                'public' => $public_key,
                'address_type' => "BTC"
            ));
        } else {
            $bitcoin_address = $Bitcoinaddress['bitcoin_address'];
            $result          = file_get_contents(url('/') . "/php-client/sample/address-api/GetBalanceBtcTest.php?btc_address=" . $bitcoin_address);
            $obj             = json_decode($result);
            //echo "<pre>";
            //print_r($obj->unconfirmed_txrefs);
            if (!empty($obj->unconfirmed_txrefs)) {
                foreach ($obj->unconfirmed_txrefs as $btc) {
                    $tx_hash       = $btc->tx_hash;
                    $address       = $btc->address;
                    $value         = $btc->value / 100000000;
                    $confirmations = $btc->confirmations;
                    $balances      = sprintf('%f', (float)$value);
                    $sql           = Bitcoinhistory::where('tx_hash', $tx_hash)->get();
                    $sqls          = Bitcoinsend::where('hash_id', $tx_hash)->get();
                    $result        = $sql->merge($sqls);

                    if (count($result)) {
                        foreach ($result as $sqlh) {
                            if ($sqlh->tx_hash == $tx_hash || $sqlh->hash_id == $tx_hash) {
                            } else {
                                $insert = DB::table('bitcoinhistory')->insert(array(
                                    'user_id' => $user_id,
                                    'address' => $address,
                                    'tx_hash' => $tx_hash,
                                    'value' => $balances,
                                    'main' => $confirmations

                                ));
                            }
                        }
                    } else {
                        $insert = DB::table('bitcoinhistory')->insert(array(
                            'user_id' => $user_id,
                            'address' => $address,
                            'tx_hash' => $tx_hash,
                            'value' => $balances,
                            'main' => $confirmations

                        ));
                    }
                }
            } else {
                $confirmsbtc = array();
                $get_tx      = array();
                if (!empty($obj->txrefs)) {
                    foreach ($obj->txrefs as $confirmbtc) {
                        $confirmsbtc[] = $confirmbtc->tx_hash;
                    }
                    $get_sql = Bitcoinhistory::where('user_id', Auth::user()->id)->get();
                    if (count($get_sql)) {
                        foreach ($get_sql as $get_sqls) {
                            $get_tx[] = $get_sqls->tx_hash;
                            if (in_array($get_sqls->tx_hash, $confirmsbtc)) {
                                $update          = bitcoinhistory::where('user_id', Auth::user()->id)->update(['main' => 1]);
                                $bitcoin_address = $Bitcoinaddress['bitcoin_address'];
                                $private_key     = $Bitcoinaddress['private_key'];
                                $results         = file_get_contents(url('/') . "/php-client/sample/address-api/Getmainbalancetest.php?btc_address=" . $bitcoin_address);
                                $r               = $results;
                                $fess            = 3000;
                                $mainbalance     = $r - $fess;
                                if ($mainbalance >= 3500) {
                                    $serialized_data = serialize(array($bitcoin_address, $mainbalance, $private_key, $merchant_address));
                                    $results         = file_get_contents(url('/') . "/php-client/sample/transaction-api/transaction.php?fromAddress=" . $serialized_data);
                                    $strings         = preg_replace('/\s+/', '', $results);
                                    if (!empty($results)) {
                                        $insert = DB::table('bitcoinsend')->insert(array(
                                            'user_id' => $user_id,
                                            'hash_id' => $strings,
                                            'balance' => $mainbalance,
                                            'confirm' => 1

                                        ));
                                    } else {
                                    }
                                }
                            }
                        }
                    }
                }

                $result = array_diff($confirmsbtc, $get_tx);
                if (!empty($result)) {
                    foreach ($result as $tag) {
                        $result          = file_get_contents("https://api.blockcypher.com/v1/btc/main/txs/" . $tag);
                        $objcon          = json_decode($result);
                        $bitcoin_address = $Bitcoinaddress['bitcoin_address'];
                        $tx_hash         = $objcon->hash;
                        $value           = $objcon->outputs[0]->value / 100000000;
                        $confirmations   = 1;
                        $balances        = sprintf('%f', (float)$value);
                        $sql             = Bitcoinhistory::where('tx_hash', $tx_hash)->get();
                        $sqls            = Bitcoinsend::where('hash_id', $tx_hash)->get();
                        $result          = $sql->merge($sqls);
                        if (count($result)) {
                            foreach ($result as $sqlh) {
                                if ($sqlh->tx_hash == $tx_hash || $sqlh->hash_id == $tx_hash) {
                                } else {
                                    $insert = DB::table('bitcoinhistory')->insert(array(
                                        'user_id' => $user_id,
                                        'address' => $bitcoin_address,
                                        'tx_hash' => $tx_hash,
                                        'value' => $balances,
                                        'main' => 1

                                    ));
                                }
                            }
                        } else {
                        }
                    }
                }
            }
        }

        $data  = DB::table("bitcoinhistory")
            ->select(DB::raw("SUM(value) as count"))
            ->where('user_id', '=', Auth::user()->id)
            ->where('main', '=', '1')
            ->get();
        $datas = DB::table("coin_payment")
            ->select(DB::raw("SUM(inusd) as count"))
            ->where('user_id', '=', Auth::user()->id)
            ->where('status', '=', '1')
            ->get();

        if (empty($data[0]->count)) {
            $btcblance = 0.00;
        } else {
            $btcblance = $data[0]->count;
        }
        if (empty($datas[0]->count)) {
            $coinblance = 0.00;
        } else {
            $coinblance = $datas[0]->count;
        }
        $sql         = Bitcoinhistory::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get();
        $btc_address = Bitcoinaddress::where('user_id', Auth::user()->id)->where('address_type', 'BTC')->get();

//die();
        return view('front.user.bitlog', compact('data', 'sql', 'btc_address', 'coinblance', 'btcblance', 'currentRate'));
    }

    public function cointrans()

    {
        $all         = file_get_contents("https://blockchain.info/ticker");
        $res         = json_decode($all);
        $currentRate = $res->USD->last;

        $price = Price::latest()->first();

        $data  = DB::table("bitcoinhistory")
            ->select(DB::raw("SUM(value) as count"))
            ->where('user_id', '=', Auth::user()->id)
            ->where('main', '=', '1')
            ->get();
        $datas = DB::table("coin_payment")
            ->select(DB::raw("SUM(inusd) as count"))
            ->where('user_id', '=', Auth::user()->id)
            ->where('status', '=', '1')
            ->get();

        $datasbal = DB::table("coin_payment")
            ->select(DB::raw("SUM(amount) as count"))
            ->where('user_id', '=', Auth::user()->id)
            ->where('status', '=', '1')
            ->get();
        if (empty($data[0]->count)) {
            $btcblance = 0.00;
        } else {
            $btcblance = $data[0]->count;
        }
        if (empty($datas[0]->count)) {
            $coinblance = 0.00;
        } else {
            $coinblance = $datas[0]->count;
        }
        if (empty($datasbal[0]->count)) {
            $coinblances = 0.00;
        } else {
            $coinblances = $datasbal[0]->count;
        }

        $btc_address = Coinpayment::where('user_id', Auth::user()->id)->where('status', '1')->get();

        $tetracoin      = Price::find(4);
        $tetracoinprice = $tetracoin->price;

        return view('front.user.coinlog', compact('currentRate', 'btcblance', 'coinblance', 'btc_address', 'tetracoinprice', 'coinblances'));
    }

    public function userprofile()
    {
        $user   = User::find(Auth::id());
        $avatar = Avatar::where('user_id', $user['id'])->pluck('photo')->first();
        return view('front.user.profile', compact('user', 'avatar'));
    }

    public function cngavatar(Request $request)
    {
        $avatar = Avatar::where('user_id', Auth::id())->first();

        if ($avatar == null) {
            $this->validate($request, [
                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8000'
            ]);

            if ($request->hasFile('photo')) {
                $newava['photo'] = Auth::id() . '.png';
                $request->photo->move('assets/images/avatar', $newava['photo']);
            }
            $newava['user_id'] = Auth::id();

            Avatar::create($newava);
            return back()->withSuccess('Your Photo Updated Successfuly!');
        } else {
            $this->validate($request, [
                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);

            if ($request->hasFile('photo')) {
                $avatar['photo'] = Auth::id() . '.png';
                $request->photo->move('assets/images/avatar', $avatar['photo']);
            }

            $avatar->save();

            return back()->withSuccess('Your Photo Updated Successfuly');
        }
    }

    public function userupdate(Request $request)
    {
        $user = User::find(Auth::id());

        $this->validate($request,
            [
                'firstname' => 'required|string|max:255',
                'lastname' => 'required|string|max:255',
                'address' => 'required|string|max:255',
                'city' => 'required|string|max:255',
                'postcode' => 'required|string|max:255',
                'country' => 'required|string|max:255',
                'mobile' => 'required|string|max:255',

            ]);

        $user['firstname'] = $request->firstname;
        $user['lastname']  = $request->lastname;
        $user['address']   = $request->address;
        $user['city']      = $request->city;
        $user['postcode']  = $request->postcode;
        $user['country']   = $request->country;
        $user['mobile']    = $request->mobile;

        $user->save();

        //$msg =  'User Information Updated';
        //send_email($user->email, $user->username, 'Info Updated', $msg);
        //$sms =  'User Information Updated';
        //send_sms($user->mobile, $sms);

        return back()->withSuccess('Profile Information Updated Successfuly');
    }

    //Documnet Verify
    public function document()
    {
        return view('front.user.document');
    }

    public function doc_verify(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:8000',
            ]);

        $docm['user_id'] = Auth::id();
        $docm['name']    = $request->name;
        $docm['details'] = $request->details;
        if ($request->hasFile('photo')) {
            $docm['photo'] = uniqid() . '.' . $request->photo->getClientOriginalExtension();
            $request->photo->move('assets/images/document', $docm['photo']);
        }

        Docver::create($docm);

        return back()->withSuccess('Verification Request Sent Successfuly!');
    }

    //Change password
    public function changepass()
    {
        $user   = User::find(Auth::id());
        $avatar = Avatar::where('user_id', $user['id'])->pluck('photo')->first();
        return view('auth.chpass', compact('user', 'avatar'));
    }

    public function chnpass(Request $request)
    {
        $user = User::find(Auth::user()->id);

        if (Hash::check(Input::get('passwordold'), $user['password']) && Input::get('password') == Input::get('password_confirmation')) {
            $user->password = bcrypt(Input::get('password'));
            $user->save();

            return back()->withSuccess('Password  Changed successfully');
        } else {
            return redirect()->back()->with('message', 'Password  not Changed!');
        }
    }

    public function deposit()
    {
        $all         = file_get_contents("https://blockchain.info/ticker");
        $res         = json_decode($all);
        $currentRate = $res->USD->last;
        $price       = Price::latest()->first();

        $btusd  = Auth::user()->bitcoin * $currentRate;
        $nusd   = Auth::user()->balance * $price->price;
        $totusd = $btusd + $nusd;

        return view('front.user.deposit', compact('btusd', 'nusd', 'totusd'));
    }

    public function refered()
    {
        $user   = User::find(Auth::User()->id);
        $refers = User::where('refid', $user['id'])->orderBy('id', 'desc')->get();
        $today  = Reference::where('refer', $user['username'])->whereDate('created_at', Carbon::today()->toDateString())->get();
        return view('front.user.refered', compact('refers', 'today'));
    }

    public function google2fa()
    {
        $user     = Auth::user();
        $secret   = Google2FA::generateSecretKey();
        $prevcode = $user->secretcode;

        Google2FA::setAllowInsecureCallToGoogleApis(true);
        $prevqr = Google2FA::getQRCodeGoogleUrl('Leigonphy', $user->email, $prevcode);

        return view('front.user.goauth.create', compact('secret', 'prevcode', 'prevqr'));
    }

    public function create2fa(Request $request)
    {
        $this->validate($request, ['key' => 'required']);

        $secret = $request->input('key');

        $user = Auth::user();
        $user->update(['secretcode' => $secret, 'gtfa' => 0]);

        Google2FA::setAllowInsecureCallToGoogleApis(true);
        $qrCodeUrl = Google2FA::getQRCodeGoogleUrl('Leigonphy', $user->email, $secret);

        //$msg =  'Google Two Factor Authentication Enabled Successfully';
        //send_email($user->email, $user->username, 'Google 2FA', $msg);
        //$sms =  'Google Two Factor Authentication Enabled Successfully';
        //send_sms($user->mobile, $sms);

        return view('front.user.goauth.enablecode', compact('secret', 'qrCodeUrl'));
    }

    public function enablecode(Request $request)
    {
        $this->validate($request, ['key' => 'required', 'code' => 'required']);

        $user = Auth::user();
        if (Google2FA::verifyKey($user->secretcode, $request->input('code'))) {
            $user->update(['gtfa' => 1]);
            return redirect('home/g2fa')->with('success', 'Two Factor Authentication Enabled Successfully');
        }

        return redirect()->back()->with('error', 'Code is incorrect');
    }

    public function disable2fa()
    {
        $user = Auth::user();
        $user->update(['secretcode' => '0', 'gtfa' => 0]);

        return redirect()->back()->with('success', 'Two Factor Authentication Disabled Successfully');
    }

    public function support()
    {
        return view('front.user.support');
    }

    public function postSupport(Request $request)
    {
        $this->validate($request, ['message' => 'required|min:3']);

        Notification::route('mail', 'support@leigonphy.co')->notify(new UserSupportContact(auth()->user(), $request->input('message')));

        return back()->with('success', 'Your message has been successfully sended');
    }

}
