<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Sandbox;
class SandboxController extends Controller
{
  public function index(){
  
     $sandboxs = Sandbox::find(1);
        if($sandboxs == null)
        {
            $default = [
                'webTitle' => 'THESOFTKING',
                'subtitle' => 'Subtitle',
                'startdate' => '12/10/2017',
                'colorCode' => '009933',
                'curCode' => 'BDT',
                'curSymbol' => 'TK',
                'decimalPoint' => '2',
                'registration' => '1',
                'emailVerify' => '0',
                'smsVerify' => '1',
                'emailNotify' => '0',
                'smsNotify' => '1',
            ];
            Sandbox::create($default);
        }
        return view('admin.dashboard.sandbox', compact('sandboxs'));

  }

    public function update(Request $request, $id)
    {
      $sandbox = Sandbox::find($id);
        $this->validate($request,
               [
                'status' => 'required',
                ]);
        $sandbox['sandbox'] = $request->status;
        
        $sandbox->save();

        return back()->with('success', 'Sandbox Settings Updated Successfully!');    
        
    }
    
    
    
    
    

}
