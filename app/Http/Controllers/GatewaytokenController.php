<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gatewaytoken;

class GatewaytokenController extends Controller
{
    public function show()
    {
    	$gateways = Gatewaytoken::all();
        
        if($gateways == null)
        {
          return view('admin.deposit.gatewaytoken');
        
        }
     else{
    return view('admin.deposit.gatewaytoken', compact('gateways'));
   }     
}

     public function store(Request $request)
    {
        $this->validate($request, [
            'gateimg' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name' => 'required',
            'status' => 'nullable'
        ]);

        if($request->hasFile('gateimg'))
        {             
            $gateway['gateimg'] = uniqid().'.'.$request->gateimg->getClientOriginalExtension();
            $request->gateimg->move('assets/images/gateway',$gateway['gateimg']);
        }
        
        $gateway['name'] = $request->name;
       
        $gateway['val2'] = $request->val2;
        
        $gateway['status'] = $request->status;

        Gatewaytoken::create($gateway);

        return back()->with('success','New Btc token Added successfully.');
    }

    public function update(Request $request, $id)
    {
    	$gateway = Gatewaytoken::findorFail($id);

        $this->validate($request, [
            'gateimg' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name' => 'required',
            'val2' => 'nullable',
            'status' => 'nullable'
        ]);

        if($request->hasFile('gateimg'))
        {
            $path = 'assets/images/gateway/'.$gateway->gateimg;

                if(file_exists($path))
                {
                    unlink($path);
                }
                
            $gateway['gateimg'] = uniqid().'.'.$request->gateimg->getClientOriginalExtension();
            $request->gateimg->move('assets/images/gateway',$gateway['gateimg']);
        }

        $gateway['name'] = $request->name;
       
        $gateway['val2'] = $request->val2;
        
        $gateway['status'] = $request->status;

        $gateway->save();

        return back()->with('success','BTC TokenInformation Updated successfully.');
    }
}
