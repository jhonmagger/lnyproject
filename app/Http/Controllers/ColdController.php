<?php

namespace App\Http\Controllers;

use App\Gsetting;
use Illuminate\Http\Request;

class ColdController extends Controller
{


    public function index()
    {
        $gsettings = Gsetting::find(1);
       
        return view('admin.dashboard.wallet', compact('gsettings'));
    }

    

    public function update(Request $request, $id)
    {
        $settings = Gsetting::find($id);

        $this->validate($request,
               [
                'walletaddress' => 'required',
                ]);

        $settings['walletaddress'] = $request->walletaddress;
       

        $settings->save();

        return back()->with('success', 'walletaddress Updated Successfully!');
    }

    
}
