<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Gateway;
use App\Price;
use App\Deposit;
use App\Uwdlog;
use App\Gsetting;
use Session;
use Stripe\Stripe;
use Stripe\Token;
use Stripe\Charge;
use App\Lib\coinPayments;


class PaymentController extends Controller
{
	public function __construct()
	{
		$this->middleware(['auth', '2fa']);
	}

	public function depositnow()
	{
		$gset = Gsetting::first();
		$track = Session::get('Track');
		$data = Deposit::where('trxid',$track)->orderBy('id', 'DESC')->first();

		if($data->status!=0)
		{
			// Error Message // Redirect
			return redirect()->route('home')->with('alert', 'An Error Occurd!');;
			exit();
		}
		$gatewayData = Gateway::where('id', $data->gateway_id)->first();

		////////PAYPAL
		if ($data->gateway_id==1) 
		{
			$paypal['amount'] = $data->inusd;
			$paypal['sendto'] = $gatewayData->val1;
			$paypal['track'] = $track;

			return view('front.user.payment.paypal', compact('paypal'));

		}

		////////PM
		elseif ($data->gateway_id==2) 
		{

			$perfect['amount'] = $data->inusd;
			$perfect['value1'] = $gatewayData->val1;
			$perfect['value2'] = $gatewayData->val2;
			$perfect['track'] = $track;

			return view('front.user.payment.perfect', compact('perfect'));
		}

		////////Bitcoin
		elseif ($data->gateway_id==3) 
		{

			if($data->bcam==0)
			{

				$blockchain_root = "https://blockchain.info/";
				$blockchain_receive_root = "https://api.blockchain.info/";
				$mysite_root = url('/');
				$secret = "ABIR";
				$my_xpub = $gatewayData->val2;
				$my_api_key = $gatewayData->val1;

				$invoice_id = $track;
				$callback_url = $mysite_root . "/home/ipnbtc?invoice_id=" . $invoice_id . "&secret=" . $secret;

				$resp = @file_get_contents($blockchain_receive_root . "v2/receive?key=" . $my_api_key . "&callback=" . urlencode($callback_url) . "&xpub=" . $my_xpub);

				if (!$resp) {

					//BITCOIN API HAVING ISSUE. PLEASE TRY LATER
					return redirect()->route('home');

					exit;
				}

				$response = json_decode($resp);
				$sendto = $response->address;

					// $sendto = "1HoPiJqnHoqwM8NthJu86hhADR5oWN8qG7";

				$bcamo = file_get_contents("https://blockchain.info/tobtc?currency=USD&value=$data->inusd");
				$data['bcid'] = $sendto;
				$data['bcam'] = $bcamo;
				$data->save();
			}
			/////UPDATE THE SEND TO ID

			$bitcoin['amount'] = $data->bcam;
			$bitcoin['sendto'] = $data->bcid;

			$var = "bitcoin:$data->bcid?amount=$data->bcam";
			$bitcoin['code'] =  "<img src=\"https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=$var&choe=UTF-8\" title='' style='width:300px;' />";

			return view('front.user.payment.bitcoin', compact('bitcoin'));
		}

		//Stripe
		elseif($data->gateway_id == 4)
		{
			return view('front.user.payment.stripe');
		}
		//Manual Payments
		else{

			return redirect()->route('home')->with('alert', 'Sorry!');
		}
	}

// 	public function ipnpaypal()
// 	{

// 		$raw_post_data = file_get_contents('php://input');
// 		$raw_post_array = explode('&', $raw_post_data);
// 		$myPost = array();
// 		foreach ($raw_post_array as $keyval) 
// 		{
// 			$keyval = explode ('=', $keyval);
// 			if (count($keyval) == 2)
// 				$myPost[$keyval[0]] = urldecode($keyval[1]);
// 		}


// 		$req = 'cmd=_notify-validate';
// 		if(function_exists('get_magic_quotes_gpc')) 
// 		{
// 			$get_magic_quotes_exists = true;
// 		}
// 		foreach ($myPost as $key => $value) 
// 		{
// 			if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
// 				$value = urlencode(stripslashes($value));
// 			} else {
// 				$value = urlencode($value);
// 			}
// 			$req .= "&$key=$value";
// 		}


// 	    // $paypalURL = "https://www.sandbox.paypal.com/cgi-bin/webscr";
// 		$paypalURL = "https://secure.paypal.com/cgi-bin/webscr";
// 		$ch = curl_init($paypalURL);
// 		if ($ch == FALSE) 
// 		{
// 			return FALSE;
// 		}
// 		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
// 		curl_setopt($ch, CURLOPT_POST, 1);
// 		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
// 		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
// 		curl_setopt($ch, CURLOPT_SSLVERSION, 6);
// 		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
// 		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
// 		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);

// // Set TCP timeout to 30 seconds
// 		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
// 		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close', 'User-Agent: company-name'));
// 		$res = curl_exec($ch);
// 		$tokens = explode("\r\n\r\n", trim($res));
// 		$res = trim(end($tokens));

// 		if (strcmp($res, "VERIFIED") == 0 || strcasecmp($res, "VERIFIED") == 0) 
// 		{


// 			$receiver_email  = $_POST['receiver_email'];
// 			$mc_currency  = $_POST['mc_currency'];
// 			$mc_gross  = $_POST['mc_gross'];
// 			$track = $_POST['custom'];

// 			$DepositData = Deposit::where('trxid',$track)->orderBy('id', 'DESC')->first();
// 			$gatewayData = Gateway::where('id', 1)->first();


// 			if($receiver_email==$gatewayData->val1 && $mc_currency=="USD" && $mc_gross ==$DepositData->inusd)
// 			{

// 				$DepositData['extrx'] = $_POST['ipn_track_id'];

// 				$DepositData['status'] = 1;

// 				$user = User::find($DepositData['user_id']);
// 				$user['balance'] =  $user['balance'] + $DepositData['amount'];
// 				$user->save(); 

// 				$ulog['user_id'] = $DepositData['user_id'];
// 				$ulog['trxid'] = $DepositData['trxid'];
// 				$ulog['amount'] = $DepositData['amount'];
// 				$ulog['flag'] = 1;
// 				$ulog['balance'] = $user['balance'];
// 				$ulog['desc'] = "Deposite by".$gatewayData->name;

// 				Uwdlog::create($ulog);

// 				$DepositData->save();

// 				return redirect()->route('home')->with('success', 'Deposit Successfull!');

// 			}
// 		}

// 	}

// 	public function ipnperfect()
// 	{

// 		$gatewayData = Gateway::where('id', 2)->first();

// 		$passphrase=strtoupper(md5($gatewayData->val2));


// 		define('ALTERNATE_PHRASE_HASH',  $passphrase);
// 		define('PATH_TO_LOG',  '/somewhere/out/of/document_root/');
// 		$string=
// 		$_POST['PAYMENT_ID'].':'.$_POST['PAYEE_ACCOUNT'].':'.
// 		$_POST['PAYMENT_AMOUNT'].':'.$_POST['PAYMENT_UNITS'].':'.
// 		$_POST['PAYMENT_BATCH_NUM'].':'.
// 		$_POST['PAYER_ACCOUNT'].':'.ALTERNATE_PHRASE_HASH.':'.
// 		$_POST['TIMESTAMPGMT'];

// 		$hash=strtoupper(md5($string));
// 		$hash2 = $_POST['V2_HASH'];

// 		if($hash==$hash2){

// 			$amo = $_POST['PAYMENT_AMOUNT'];
// 			$unit = $_POST['PAYMENT_UNITS'];
// 			$track = $_POST['PAYMENT_ID'];

// 			$DepositData = Deposit::where('trxid',$track)->orderBy('id', 'DESC')->first();

// 			if($_POST['PAYEE_ACCOUNT']==$gatewayData->val1 && $unit=="USD" && $amo ==$DepositData->inusd){

// 				$DepositData['extrx'] = $_POST['PAYMENT_BATCH_NUM'];

// 				$DepositData['status'] = 1;

// 				$user = User::find($DepositData['user_id']);
// 				$user['balance'] =  $user['balance'] + $DepositData['amount'];
// 				$user->save(); 

// 				$ulog['user_id'] = $DepositData['user_id'];
// 				$ulog['trxid'] = $DepositData['trxid'];
// 				$ulog['amount'] = $DepositData['amount'];
// 				$ulog['flag'] = 1;
// 				$ulog['balance'] = $user['balance'];
// 				$ulog['desc'] = "Deposite by".$gatewayData->name;

// 				Uwdlog::create($ulog);

// 				$DepositData->save();

// 				return redirect()->route('home')->with('success', 'Deposit Successfull!');

// 			}
// 		}

// 	}

	public function ipnbtc(){

		$gatewayData = Gateway::where('id', 3)->first();


		$track = $_GET['invoice_id'];
		$secret = $_GET['secret'];
		$address = $_GET['address'];
		$value = $_GET['value'];
		$confirmations = $_GET['confirmations'];
		$value_in_btc = $_GET['value'] / 100000000;

		$trx_hash = $_GET['transaction_hash'];

		$DepositData = Deposit::where('trxid',$track)->orderBy('id', 'DESC')->first();


		if ($DepositData->status==0) {

			if ($DepositData->bcam==$value_in_btc && $DepositData->bcid==$address && $secret=="ABIR" && $confirmations>2){

				$DepositData['extrx'] = $trx_hash;

				$DepositData['status'] = 1;

				$user = User::find($DepositData['user_id']);
				$user['balance'] =  $user['balance'] + $DepositData['amount'];
				$user->save(); 

				$ulog['user_id'] = $DepositData['user_id'];
				$ulog['trxid'] = $DepositData['trxid'];
				$ulog['amount'] = $DepositData['amount'];
				$ulog['flag'] = 1;
				$ulog['balance'] = $user['balance'];
				$ulog['desc'] = "Deposite by".$gatewayData->name;

				Uwdlog::create($ulog);

				$DepositData->save();

				return redirect()->route('home')->with('success', 'Deposit Successfull!');

			}

		}

	}


// 	public function ipnstripe(Request $request)
// 	{
// 		$track =   Session::get('Track');
// 		$data = Deposit::where('trxid',$track)->orderBy('id', 'DESC')->first();

// 		$this->validate($request,
// 			[
// 				'cardNumber' => 'required',
// 				'cardExpiry' => 'required',
// 				'cardCVC' => 'required',
// 			]);

// 		$cc = $request->cardNumber;
// 		$exp = $request->cardExpiry;
// 		$cvc = $request->cardCVC;

// 		$exp = $pieces = explode("/", $_POST['cardExpiry']);
// 		$emo = trim($exp[0]);
// 		$eyr = trim($exp[1]);
// 		$cnts = $data->inusd*100;

// 		$gatewayData = Gateway::where('id', 4)->first();

// 		Stripe::setApiKey($gatewayData->val1);

// 		try{
// 			$token = Token::create(array(
// 				"card" => array(
// 					"number" => "$cc",
// 					"exp_month" => $emo,
// 					"exp_year" => $eyr,
// 					"cvc" => "$cvc"
// 				)
// 			));

// 			try{
// 				$charge = Charge::create(array(
// 					'card' => $token['id'],
// 					'currency' => 'USD',
// 					'amount' => $cnts,
// 					'description' => 'item',
// 				));


// 				if ($charge['status'] == 'succeeded') {


// 					$data['status'] = 1;

// 					$user = User::find($data['user_id']);
// 					$user['balance'] =  $user['balance'] + $data['amount'];
// 					$user->save(); 

// 					$ulog['user_id'] = $data['user_id'];
// 					$ulog['trxid'] = $data['trxid'];
// 					$ulog['amount'] = $data['amount'];
// 					$ulog['flag'] = 1;
// 					$ulog['balance'] = $user['balance'];
// 					$ulog['desc'] = "Deposite by".$gatewayData->name;

// 					Uwdlog::create($ulog);

// 					$data->save();

// 					return redirect()->route('home')->with('success', 'Deposit Successfull!');

// 				}


// 			}
// 			catch (Exception $e){
// 				return redirect()->route('home')->with('alert', $e->getMessage());
// 			}

// 		}catch (Exception $e){
// 			return redirect()->route('home')->with('alert', $e->getMessage());
// 		}

// 	}

	public function depconfirm(Request $request)
	{
		$this->validate($request,
			[
				'amount' => 'required',
			]);

		if ($request->amount <= 0) 
		{
			return redirect()->route('deposit')->with('alert', 'Invalid Amount');
		}
		else
		{
			$all = file_get_contents("https://blockchain.info/ticker");
			$res = json_decode($all);
			$btcRate = $res->USD->last;
			$cprice = Price::latest()->first();

			$method = Gateway::find(5);

			$amon = $request->amount;
                        print_r($amon);
                        
                        
                         
			$bcoin = ($amon*0.01)/$btcRate;
                        

                        die();
// You need to set a callback URL if you want the IPN to work
			$callbackUrl = route('ipn.coinPay');

// Create an instance of the class
			$CP = new coinPayments();

// Set the merchant ID and secret key (can be found in account settings on CoinPayments.net)
			$CP->setMerchantId($method->val1);
			$CP->setSecretKey($method->val2);

// Create a payment button with item name, currency, cost, custom variable, and the callback URL

			$ntrc = str_random(16);

			$deposit['user_id'] = Auth::user()->id;
			$deposit['amount'] = $amon;
			$deposit['inusd'] = $bcoin;
			$deposit['charge'] = "0";
			$deposit['gateway_id'] = 5;
			$deposit['trxid'] = $ntrc;
			$deposit['status'] = 0;
			Deposit::create($deposit);


			$form = $CP->createPayment('Purchase nCoin', 'btc',  $bcoin, $ntrc, $callbackUrl);


			return view('front.user.depoconf', compact('bcoin','amon','form'));
		} 

	}

	public function ipncoin(Request $request)
	{
		$track = $request->custom;
		$status = $request->status;
		$amount1 = floatval($request->amount1);
		$currency1 = $request->currency1;

		$DepositData = Deposit::where('trxid', $track)->first();


		if ($currency1 == "btc" && $amount1 >= $DepositData->inusd && $DepositData->status == '0') {

			if ($status>=100 || $status==2) {

				$DepositData['status'] = 1;

				$user = User::find($DepositData['user_id']);
				$user['balance'] =  $user['balance'] + $DepositData['amount'];
				$user->save(); 

				$ucode = Uaccount::where('user_id',$user->id)->first();

		        if ($ucode == null)
		        {
		            $uac['user_id'] = Auth::id();
		            $uac['accnum'] = str_random(32);
		            $new_account = Uaccount::create($uac);
		           $ucode = Uaccount::where('user_id',$user->id)->first();
		        }

				$ulog['user_id'] = $DepositData['user_id'];
				$ulog['trxid'] = $DepositData['trxid'];
				$ulog['amount'] = $DepositData['amount'];
				$ulog['charge'] = '0';
				$ulog['toacc'] = $ucode->accnum;
				$ulog['flag'] = 1;
				$ulog['status'] = 1;
				$ulog['balance'] = $user['balance'];
				$ulog['desc'] = "Purchased by BitCoin";

				Uwdlog::create($ulog);

				$DepositData->save();

			}

		}

	}

}	
