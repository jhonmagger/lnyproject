<?php

namespace App\Http\Middleware;

use Closure;
use PragmaRX\Google2FALaravel\Support\Authenticator;

class Google2FA
{
    public function handle($request, Closure $next)
    {
        $twofactor = auth()->check() ? (bool) auth()->user()->gtfa : false;
        $authenticator = app(Authenticator::class)->boot($request);
        if (!$twofactor || $authenticator->isAuthenticated()) {
            return $next($request);
        }
        return $authenticator->makeRequestOneTimePasswordResponse();
    }
}