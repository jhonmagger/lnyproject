@extends('admin.layouts.master')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-red-sunglo">
					<i class="icon-settings font-red-sunglo"></i>
					<span class="caption-subject bold uppercase">Sandbox Settings</span>
				</div>
			</div>
			<div class="portlet-body form">
				<form role="form" method="POST" action="{{url('admin/sandbox')}}/{{$sandboxs->id}}">
					{{ csrf_field() }}
					{{method_field('put')}}
					
					
					<hr/>
					<div class="row">
						<hr/>
						

						<div class="col-md-4">
							
							<input type="radio"  name="status" value="0" id="inlineRadio1" {{ $sandboxs->sandbox== "0" ? 'checked' : '' }}>
                            <label for="inlineRadio1"> Sandbox </label>
						</div>
						<div class="col-md-4">
							
							
<input type="radio" name="status" value="1" id="inlineRadio2" {{ $sandboxs->sandbox== "1" ? 'checked' : '' }}>
                             <label for="inlineRadio2"> Main </label>
						</div>
					</div>


					
					<div class="row">
						<hr/>
						<div class="col-md-4 col-md-offset-4">
							<button class="btn blue btn-block btn-lg">Update</button>
						</div>
					</div>
			</form>
		</div>
	</div>
</div>
</div>
@endsection
