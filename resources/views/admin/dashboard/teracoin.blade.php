@extends('admin.layouts.master')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-red-sunglo">
					<i class="icon-settings font-red-sunglo"></i>
					<span class="caption-subject bold uppercase">Exchange Rate of Coin </span>
				</div>
			</div>
			<div class="portlet-body form">
				<form role="form" method="POST" action="{{url('admin/tetraprice')}}/{{$gsettings->id}}">
					{{ csrf_field() }}
					{{method_field('put')}}
					<div class="row">
						<div class="col-md-12">
							<h4>One token rate in btc</h4>
							<input type="text" class="form-control input-lg" value="{{$gsettings->btcrate}}" name="btcrate" placeholder="BTC">
                                                   
						</div>
						
					
					
<div class="row">
</div>

					<div class="row">
						<hr/>
						<div class="col-md-4 col-md-offset-4">
							<button class="btn blue btn-block btn-lg">Update</button>
						</div>
					</div>
			</form>
		</div>
	</div>
</div>
</div>
@endsection
