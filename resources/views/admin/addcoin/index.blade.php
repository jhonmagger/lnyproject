@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Referral List</span>
                </div>

            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover order-column">
                <thead>
                    <tr>
                        <th>
                            S.No 
                        </th>
                        <th>
                            Username
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Point
                        </th>
                        <th>
                        Add Coin
                        </th>
                  	 </tr>
                </thead>
                <tbody>
                    <?php $i=1;?>
		 	@foreach($refer as $refers)
                     <tr>
                     	<td>
                        <?php echo $i;?>	
                        </td>
                        <td>
                            {{$refers->username}}    
                        </td>
                         <td>
                            {{$refers->email}}    
                        </td>
                         <td>
                            {{$refers->point}}       
                        </td> 
                        
                       
                           <td>
                               <a href="{{route('addcoin.referal', $refers->user_id)}}" class="btn btn-outline btn-circle btn-sm green">
                             <i class="fa fa-eye"></i> Add Coin </a>
                               
                               </td>
                         
                           
                     </tr>
                     <?php $i++;?>
 			@endforeach
                   
 			<tbody>
           </table>
           <?php echo $refer->render(); ?>
        </div>
			
			</div><!-- row -->
			</div>
		</div>
	</div>		
</div>
@endsection