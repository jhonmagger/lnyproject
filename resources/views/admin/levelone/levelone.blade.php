@extends('admin.layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-list font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Referral Level 1 Price </span>
                    </div>
                  
                </div>
 <span class="caption-subject font-green uppercase"></span>
                <div class="portlet-body">

                    <div class="table-scrollable">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                
                                <th>S.No </th>
                                <th> Price</th>
                                <th> Action </th>
                            </tr>
                            </thead>
                            <tbody>
                           
                            @foreach($levelone as $levelones)
                            <tr>
                               
                                <td> {{$levelones->id}} </td>
                                <td> {{$levelones->levelone}} </td>
                                <td>
                                    
<a href="" class="btn btn-outline btn-circle btn-sm purple" data-toggle="modal" data-target="#Modal{{$levelones->id}}">
<i class="fa fa-edit"></i> Edit </a>
                                        
                                    </a>
                                </td>
                            </tr>




 <div id="Modal{{$levelones->id}}" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Edit Price  Information</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form" method="POST" action="{{url('admin/levelone')}}/{{$levelones->id}}" enctype="multipart/form-data">
                                     {{ csrf_field() }}
                                     {{method_field('put')}}
                                         <div class="form-group">
                                            <label for="price">Levelone Price</label>
                                            <div class="input-group">
                                                <input type="number" class="form-control" id="levelone" name="levelone" value="{{$levelones->levelone}}" >
                                                <span class="input-group-addon">%</span>
                                            </div>
                                            
                                        </div>
                                        <div class="form-group">
                                        <button type="submit" class="btn btn-lg btn-success btn-block" >Update</button>
                                    </div>
                                </form>                                   
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>










                
                            @endforeach
                            </tbody>
                        </table>
                         
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
               
@endsection