@extends('admin.layouts.master')

@section('content')
    
    <div class="col-md-4 col-md-offset-4">
        
      <div class="update" style="font-size: 25px;
    text-decoration: underline;"> Update ICO Agenda </div>
<form role="form" method="POST" action="{{ route('icoagenda.update', $icoagenda->id) }}" enctype="multipart/form-data">
    
{{ csrf_field() }}
                                         <div class="form-group">
                                        <label for="price">Start_Date</label>
                                          
                                             <input type="hidden" class="form-control  date-picker" readonly name="user_id" value="<?php echo $icoagenda->user_id;?>">
                                             <div class="input-group">
                                                 
                                                 
                                                 
                                  <input type="text" class="form-control  date-picker" readonly name="start_dates" value="<?php echo $icoagenda->start_date;?>">               
                                                 
                            	 
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                 </div>
                                              </div>
    
                                      <div class="form-group">
                                            <label for="price">Start_Time</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="time" name="start_times" value="<?php echo $icoagenda->start_time;?>" >
                                                <span class="input-group-addon">Time</span>
                                            </div>
                                            
                                        </div>
    
    
    <div class="form-group">
                                        <label for="price">Last_Date</label>
                                             
                                             <div class="input-group">
                            	  <input type="text" class="form-control  date-picker" readonly name="last_dates" value="<?php echo $icoagenda->last_date;?>">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                 </div>
                                              </div>
                                    
                                    <div class="form-group">
                                            <label for="price">Last_Time</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="time" name="last_times" value="<?php echo $icoagenda->last_time;?>" >
                                                <span class="input-group-addon">Time</span>
                                            </div>
                                            
                                        </div>
    
    
                                    
                                    
                                    <div class="form-group">
                                            <label for="price">Total Tetra</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="total_tetra" name="total_tetra" value="<?php echo $icoagenda->total_exa;?>" >
                                                <span class="input-group-addon">TETRA</span>
                                            </div>
                                            
                                        </div>
                                    
                                    <div class="form-group">
                                            <label for="price">Price(USD)</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="price" name="price" value="<?php echo $icoagenda->price;?>">
                                                <span class="input-group-addon">USD</span>
                                            </div>
                                            
                                        </div>
                                    
                                    <div class="form-group">
                                            <label for="price">Limit</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="limit" name="limit" value="<?php echo $icoagenda->limit;?>" readonly >
                                                <span class="input-group-addon">TETRA/OCC</span>
                                            </div>
                                            
                                        </div>
                                    
                            
                                    
                                    
                                    
                                    <div class="form-group">
					<label for="status">Status</label>
					<select class="form-control" name="status">
                        <?php if($icoagenda->status==1){ ?> <option value="1" selected>Active</option>  <?php }else{?>
    <option value="0" selected>Inactive</option>
<?php } ?>
						
						
					</select>

				</div>
                                    
                                    
                                    
                                    
                                    
                                    
                                        <div class="form-group">
                                        <button type="submit" class="btn btn-lg btn-success btn-block" >Update</button>
                                    </div>
                                </form>  
        </div>
                            
@endsection