<!DOCTYPE html>
<html lang="zxx" class="js">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117102859-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117102859-1');
</script>

    <meta charset="utf-8">
    <meta name="author" content="Leigonphy INC">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Exchange, Trading, Lending and Payment Gateway, everything you need about cryptocurrency you will find it here.">
    <meta name="Description" content="Exchange, Trading, Lending and Payment Gateway, everything you need about cryptocurrency you will find it here.">
    <meta name="keywords" content="ico, cryptocurrency, exchange, lending, leigonphy, lending, trading"/>
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="{{ asset('assets/web/images/favicon.png')}}">
    <!-- Site Title  -->
    <title>Leigonphy - Complete Exchange and Trading Platform</title>
    <!-- Vendor Bundle CSS -->
    <link rel="stylesheet" href="{{ asset('assets/web/assets/css/vendor.bundle.css')}}">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{ asset('assets/web/assets/css/style.css?ver=1.1')}}">
    <link rel="stylesheet" href="{{ asset('assets/web/assets/css/theme-java.css?ver=1.1')}}">
    <link rel="stylesheet" href="{{ asset('assets/web/assets/css/leygonphy-fix.css?ver=1.1')}}">
</head>

<body class="theme-dark io-azure" data-spy="scroll" data-target="#mainnav" data-offset="80">

    <!-- Header -->
    <header class="site-header is-sticky">

        <!-- Place Particle Js -->
        <div id="particles-js" class="particles-container particles-js">
        </div>

        <!-- Navbar -->
        <div class="navbar navbar-expand-lg is-transparent" id="mainnav">
            <nav class="container">

                <a class="navbar-brand" href="./">
                    <img class="logo logo-light" alt="logo" src="{{ asset('assets/web/images/logo-white.png')}}">
                </a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggle">
                    <span class="navbar-toggler-icon">
                        <span class="ti ti-align-justify"></span>
                    </span>
                </button>

                <div class="collapse navbar-collapse justify-content-end" id="navbarToggle">
                    <ul class="navbar-nav">
                      <?php if (url()->current()=="https://leigonphy.co/login" || url()->current()=="https://leigonphy.co/register") { ?>
                        <li class="nav-item"><a class="nav-link menu-link" href="https://leigonphy.co#intro">About<span class="sr-only">(current)</span></a></li>
                        <li class="nav-item"><a class="nav-link menu-link" href="https://leigonphy.co#tokenSale">ICO</a></li>
                        <li class="nav-item"><a class="nav-link menu-link" href="https://leigonphy.co#roadmap">Roadmap</a></li>
                        <li class="nav-item"><a class="nav-link menu-link" href="https://leigonphy.co#exchange">Exchange</a></li>
                        <li class="nav-item"><a class="nav-link menu-link" href="https://leigonphy.co#apps">Apps</a></li>
                        <li class="nav-item"><a class="nav-link menu-link" target="_blank"  href="{{ asset('assets/web/documents/Leigonphy_Whitepaper.pdf')}}">Whitepaper</a></li>
                        <li class="nav-item"><a class="nav-link menu-link" href="https://leigonphy.co#faq">Faq</a></li>
                        <li class="nav-item"><a class="nav-link menu-link" href="https://leigonphy.co#contact">Contact</a></li>
                        <<?php }else{ ?>
                          <li class="nav-item"><a class="nav-link menu-link" href="#intro">About<span class="sr-only">(current)</span></a></li>
                          <li class="nav-item"><a class="nav-link menu-link" href="#tokenSale">ICO</a></li>
                          <li class="nav-item"><a class="nav-link menu-link" href="#roadmap">Roadmap</a></li>
                          <li class="nav-item"><a class="nav-link menu-link" href="#exchange">Exchange</a></li>
                          <li class="nav-item"><a class="nav-link menu-link" href="#apps">Apps</a></li>
                          <li class="nav-item"><a class="nav-link menu-link" target="_blank"  href="{{ asset('assets/web/documents/Leigonphy_Whitepaper.pdf')}}">Whitepaper</a></li>
                          <li class="nav-item"><a class="nav-link menu-link" href="#faq">Faq</a></li>
                          <li class="nav-item"><a class="nav-link menu-link" href="#contact">Contact</a></li>
                        <<?php } ?>
                    </ul>
                    <ul class="navbar-nav navbar-btns">
                        <li class="nav-item"><a class="nav-link btn btn-sm btn-outline menu-link" href="https://leigonphy.co/register">Sign up</a></li>
                        <li class="nav-item"><a class="nav-link btn btn-sm btn-outline menu-link" href="https://leigonphy.co/login">Log in</a></li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- End Navbar -->





@include('front.layouts.message')
@yield('content')
    <!-- Start Section -->
    <div class="section footer-scetion footer-particle section-pad-sm section-bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xl-4 res-l-bttm">
                    <a class="footer-brand" href="./">
                        <img class="logo logo-light" alt="logo" src="{{ asset('assets/web/images/logo-white.png')}}">
                    </a>
                    <ul class="social">
                        <li><a href="https://www.facebook.com/Leigonphy"><em class="fa fa-facebook"></em></a></li>
                        <li><a href="https://twitter.com/leigonphy"><em class="fa fa-twitter"></em></a></li>
                        <li><a href="https://www.instagram.com/leigonphy.official/"><em class="fa fa-instagram"></em></a></li>
                        <li><a href="https://www.youtube.com/channel/UCeDpMZgeSnwNeAW89Hv9PIg"><em class="fa fa-youtube-play"></em></a></li>
                        <li><a href="https://t.me/LeigonphyLNY"><em class="fa fa-telegram"></em></a></li>

                    </ul>
                </div><!-- .col  -->
                <div class="col-sm-6 col-xl-4 res-l-bttm">
                </div><!-- .col  -->
                <div class="col-xl-4">
                    <ul class="link-widget fix-right">
                        <li><a href="#intro" class="menu-link">What is LNY</a></li>
                        <li><a href="{{ asset('assets/web/documents/Leigonphy_Whitepaper.pdf')}}" target="_blank" class="menu-link">Whitepaper</a></li>
                        <li><a href="#tokenSale" class="menu-link">ICO</a></li>
                        <li><a href="#why-lny" class="menu-link">Why Us</a></li>
                        <li><a href="#exchange" class="menu-link">Exchange</a></li>
                        <li><a href="#contact" class="menu-link">Contact</a></li>
                        <li><a href="#roadmap" class="menu-link">Roadmap</a></li>
                        <li><a href="#faq" class="menu-link">FAQ</a></li>
                    </ul>
                </div><!-- .col  -->
            </div><!-- .row  -->
            <div class="gaps size-2x"></div>
            <div class="row">
                <div class="col-md-7">
                    <span class="copyright-text">
                        Copyright &copy; 2018, Leigonphy LNY INC.
                    </span>
                </div><!-- .col  -->
                <div class="col-md-5 text-right mobile-left">
                    <ul class="footer-links">
                        <!-- <li><a href="#">Privacy Policy</a></li> -->
                        <li><a href="{{ asset('assets/web/documents/Terms_and_Conditions.pdf')}}">Terms &amp; Conditions</a></li>
                    </ul>
                </div><!-- .col  -->
            </div><!-- .row  -->
        </div><!-- .container  -->
    </div>
    <!-- End Section -->

    <!-- Preloader !remove please if you do not want -->
    <div id="preloader">

        <div id="loader">
            <img src="{{ asset('assets/web/assets/images/loader.svg')}}" alt="loader">
        </div>
        <div class="loader-section loader-top"></div>
        <div class="loader-section loader-bottom"></div>
    </div>
    <!-- Preloader End -->

    <!-- JavaScript (include all script here) -->
    <script src="{{ asset('assets/web/assets/js/jquery.bundle.js')}}"></script>
    <script src="{{ asset('assets/web/assets/js/script.js')}}"></script>

</body>
</html>
