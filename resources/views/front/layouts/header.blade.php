        <div id="header" class="header navbar navbar-default navbar-fixed-top">
            <!-- begin container-fluid -->
            <div class="container-fluid">
                <!-- begin mobile sidebar expand / collapse button -->
                <div class="navbar-header">
                    <a href="{{url('admin/home')}}" class="navbar-brand">
                        <img class="img-responsive" src="{{ asset('assets/web/images/logo-white.png') }}" style="max-width: 80%; ">
                    </a>
                    <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <ul class="nav navbar-nav navbar-right">

                    <li>
                        <span class="btn btn-primary btn-md">1 BTC = $ <span id="btcpriceajax">{{rtrim(number_format(floatval($btcrate) , $gset->decimalPoint, '.', ''),'.0')}}</span></span>
                    </li>
                    <li>
                        <span class="btn btn-primary btn-md" style="margin-left:5px; margin-right: 20px;">1 {{$gset->curCode}} = $ <span id="icopriceajax"> <?php echo $crate;?></span></span>
                    </li>
                </ul>


            </div>
            <!-- end container-fluid -->
        </div>
        <!-- end #header -->


<script>
$(document).ready(function(){
setInterval(function(){
    $.ajax({
      url: "https://leigonphy.co/home/btcrate",
      success: function(res) {

       $("#btcpriceajax").html(res);
       $("#btcpriceajaxs").html(res);
      }
    });
},450000);
});
</script>
<script>
$(document).ready(function(){
setInterval(function(){
    $.ajax({
      url: "https://leigonphy.co/home/iconrate",
      success: function(res) {

      $("#icopriceajax").html(res);
      $("#icopriceajaxs").html(res);


      }
    });
},4500000);
});

</script>

<script>
$(document).ready(function(){
setInterval(function(){
    $.ajax({
      url: "https://leigonphy.co/home/totalrate",
      success: function(res) {

          $("#totaltetra").html(res);

         }
    });
},450000);
});
</script>
<script>
$(document).ready(function(){
setInterval(function(){
    $.ajax({
      url: "https://leigonphy.co/home/btcpricedeposit",
      success: function(res) {
      $("#btcpricedeposit").html(res);


         }
    });
},450000);
});
</script>
<script>
$(document).ready(function(){
setInterval(function(){
    $.ajax({
      url: "https://leigonphy.co/home/icopricedeposit",
      success: function(res) {
          $("#coinvalue").html(res);



         }
    });
},450000);
});
</script>


<script>
$(document).ready(function(){
setInterval(function(){
    $.ajax({
      url: "https://leigonphy.co/home/avalibale",
      success: function(res) {
           $("#avltetra").html(res);

         }
    });
},450000);
});
</script>
<script>
$(document).ready(function(){
setInterval(function(){
    $.ajax({
      url: "https://leigonphy.co/home/soldtetra",
      success: function(res) {
           $("#soldtetra").html(res);



         }
    });
},450000);
});
</script>
<script>
$(document).ready(function(){
setInterval(function(){
    $.ajax({
      url: "https://leigonphy.co/home/limtcoin",
      success: function(res) {
    var datone =res.split(',');

    var purchase_ico =parseInt(datone[0]);

    var total_limit_agenda = parseInt(datone[1]);

    var agendaid= parseInt(datone[2]) ;

    var remaining_ico_agenda = total_limit_agenda - purchase_ico;
    var perday_limit =datone[3];
    var ico_name = datone[4] ;
    var perday_user_bal = perday_limit - purchase_ico;
     var statuscomplted = datone[5] ;

     $("#purchase_ico").html(purchase_ico);
     $("#total_limit_agenda").html(total_limit_agenda);
     $("#agendaid").html(agendaid);
     $("#remaining_ico_agenda").html(remaining_ico_agenda);
     $("#perday_limit").html(perday_limit);
     $("#perday_limits").html(perday_limit);
     $("#roundname").html(ico_name);
     $("#perday_user_bal").html(perday_user_bal);
     $("#perday_user_bals").html(perday_user_bal);
    $("#statuscomplted").html(statuscomplted);

         }
    });
},4500000);
});
</script>
<script>
$(document).ready(function(){
setInterval(function(){
    $.ajax({
      url: "https://leigonphy.co/home/timercontrol",
      success: function(res) {
       var datones =res.split('#');

        var start_date_time= datones[0];
         var last_date_time = datones[1];
         var current_date_time = datones[2];



     $("#start_date_time").html(start_date_time);
     $("#last_date_time").html(last_date_time);
     $("#current_date_time").html(current_date_time);






           }
    });
    },4500000);
});
</script>
