@extends('front.layouts.admaster')
@section('content')

<div class="panel-lny-desk w50">

    <div class="row">
        <div class="col-md-12">
            <div style="text-align: center">
                <img src="https://leigonphy.co/assets/web/assets/images/newavatar.png" style="width: 180px;">
          	</div>
						<div class="block-lny" >
					 		 <div class="block-title-lny"><h4>Two Factor Authenticator</h4></div>
					 		 <div class="block-info-lny text-center" style="padding: 5px 30%;">

				<form role="form" method="POST" action="{{route('go2fa.enablecode')}}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group">
						<label class="secondpara" style="color:black;font-size:16px;">Use google authenticator to scan the QR code below or use the below code</label><br/>
						<br><a class="btn btn-primary btn-lg btn-block" href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en" target="_blank">DOWNLOAD APP</a>
						<hr/>
						<div class="input-group">
							<input type="text" name="key" value="{{$secret}}" class="form-control input-lg" id="code" readonly>
							<span class="input-group-addon btn btn-success" id="copybtn">Copy</span>
						</div>
						<div class="form-group">
				             <img src="{{$qrCodeUrl}}">
				        </div>
				        <br>
				        <p class="secondpara" style="color:black;font-size:16px;">Please, enter the code fromn your authenticator</p>
				        <div class="col-sm-12"> <input type="text" class="form-control auth-code" placeholder="Code" name="code" required style="margin: 11px auto;"></div>
						<button type="submit" class="btn btn-block btn-lg btn-primary" style="display:block !important">Enable</button>
					</div>
				</form>
			</div>
		 </div>


	</div>
</div>
</div>


<!--Copy Data -->
<script type="text/javascript">
  document.getElementById("copybtn").onclick = function()
  {
    document.getElementById('code').select();
    document.execCommand('copy');
  }
</script>




@endsection
