@extends('front.layouts.admaster')
@section('content')
<div class="panel-lny-desk w50">

    <div class="row">
        <div class="col-md-12">
            <div style="text-align: center">
                <img src="https://leigonphy.co/assets/web/assets/images/newavatar.png" style="width: 180px;">
          	</div>
						<div class="block-lny" >
					 		 <div class="block-title-lny"><h4>Two Factor Authenticator</h4></div>
					 		 <div class="block-info-lny text-center" style="padding: 5px 30%;">

					 @if(Auth::user()->gtfa == '1')
					 <form role="form" method="POST" action="{{route('disable.2fa')}}" enctype="multipart/form-data">
					 {{ csrf_field() }}
					 <!-- <div class="form-group">
					  <label style="text-transform: capitalize;">use google authenticator to scan the QR code below or use the below code</label>
					 <a class="btn btn-primary btn-lg btn-block" href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en" target="_blank">DOWNLOAD APP</a>


					  <div class="input-group">
					  <input type="text" value="{{$prevcode}}" class="form-control input-lg" id="code" readonly>
					 	 <span class="input-group-addon btn btn-success" id="copybtn">Copy</span>
					  </div>
					 </div>
					 <div class="form-group">
					 				<img src="{{$prevqr}}">
					 	 </div> -->
					 <button type="submit" class="btn btn-block btn-lg btn-danger">Disable Two Factor Authenticator</button>
					 </form>
					 @else
					 <form role="form" method="POST" action="{{route('go2fa.create')}}" enctype="multipart/form-data">
					 {{ csrf_field() }}
					 <div class="form-group">
					  <label style="text-transform: capitalize;">use google authenticator to scan the QR code below or use the below code</label><br/>
					 <a class="btn btn-primary btn-lg btn-block" href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en" target="_blank">DOWNLOAD APP</a>
					 <hr/>
					  <div class="input-group">
					  <input type="hidden" name="key" value="{{$secret}}" class="form-control input-lg" id="code" readonly>

					  </div>
					 </div>

					 <button type="submit" class="btn btn-block btn-lg btn-primary" style="display:block !important">Enable Two Factor Authenticator</button>
					 </form>
					 @endif
					  </div>
					 </div>


        </div>
    </div>
</div>


<!--Copy Data -->
<script type="text/javascript">
  document.getElementById("copybtn").onclick = function()
  {
    document.getElementById('code').select();
    document.execCommand('copy');
  }
</script>

@endsection
