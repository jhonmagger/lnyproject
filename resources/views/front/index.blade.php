@extends('front.layouts.master')
@section('content')
 
        <!-- Banner/Slider -->
        <div id="header" class="banner banner-full d-flex align-items-center">

            <div class="container">
                <div class="banner-content">

                    <div class="row align-items-center mobile-center">
                        <div class="col-lg-6 col-md-12 order-lg-first countdown-box-fix">
                            <div class="header-txt">
                                <div class="countdown-box text-center">
                                    <h2>REGISTRATION IS OPEN</h2>
                                </div>
                                      <h1><strong>Exchange Trading Lending and Payment Gateway</strong></h1>
                              <p class="lead">All in one Platform!<br>
                                Everything you need about cryptocurrency you will find it here! Join us!</p>
                              <ul class="btns">
                                    <li><a href="/register" class="btn">Sign up to Join</a></li>
                                    <li class="fright"><a href="#tokenSale" class="btn btn-alt">ICO CROWDSALE</a></li>
                                </ul>
                            </div>
                        </div><!-- .col  -->
                        <div class="col-lg-6 col-md-12 order-first res-m-bttm-lg">
                            <div class="header-image-alt">
                                <img src="{{ asset('assets/web/images/header-image-blue.png')}}" alt="header">
                            </div>
                        </div><!-- .col  -->
                    </div><!-- .row  -->
                </div><!-- .banner-content  -->
            </div><!-- .container  -->
        </div>
        <!-- End Banner/Slider -->
        </div><!-- .container  -->
        </div><!-- .header-partners  -->
        <a href="#intro" class="scroll-down menu-link">SCROLL DOWN</a>
    </header>
    <!-- End Header -->

    <!-- Start Section -->
    <div class="section section-pad section-bg section-pro nopb into-section" id="intro">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">

                    
                    <video width="600" height="400" controls poster="{{ asset('assets/web/images/videothumb.png')}}">
  <source src="{{ asset('assets/web/video/v3.mp4')}}" type="video/mp4">
  Your browser does not support HTML5 video.
</video>
                                    </div><!-- .col  -->
                <div class="col-md-6 order-md-first order-last">
                    <div class="text-block">
                        <h6 class="heading-xs">What is Leigonphy</h6>
                        <h2>We’ve built a platform <br> to make everything happen just in one place.</h2>
                        <p class="lead">Leigonphy Platform is designed to make sure trading and gaining can be done with such ease at a place. Using the Leigonphy Platform allows our users opportunity to play with money while honing their trading skills and earning using this crypto trading and exchange platform. </p>
                        <p>Leigonphy coin is a digital coin or currency of decentralized nature that makes use of the blockchain technology for its transactions. This platform was created with the aim of creating an easy to use cryptocurrency exchange that is secure to use by everyone. </p>
                        <p></p>
                    </div>
                </div><!-- .col  -->
            </div><!-- .row  -->
        </div><!-- .conatiner  -->
    </div>
    <!-- Start Section -->


    <!-- Start Section -->
    <div class="section section-pad section-bg section-pro" id="why-lny">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-8 offset-lg-2 col-md-8 offset-md-2">
                    <div class="section-head-s2">
                        <h6 class="heading-xs">Why Leigonphy</h6>
                        <h2 class="section-title">Competitive Advantage</h2>
                        <p>We are proud to introduce LEIGONPHY, a unique platform where you will have everything just in one place. Know our advantage.</p>
                    </div>
                </div><!-- .col  -->
            </div><!-- .row  -->
            <div class="row text-center">
                <div class="col-md-3">
                    <div class="features-box">
                        <img src="{{ asset('assets/web/images/features-icon-light-a.png')}}" alt="features">
                        <h5>Exchange & Trading Platform</h5>
                        <p>We provide the feature of Exchange & Trading to increase the value of the LNY. Capable of trade with multiple cryptocurrency from all other platforms, all in one place, the Leigonphy Exchange & Trading Platform will provide multiple currency coins in the market. New coins will be acepted too. LNY takes 0 fees for use the platform, no trading fee.</p>
                    </div>
                </div><!-- .col  -->
                <div class="col-md-3">
                    <div class="features-box">
                        <img src="{{ asset('assets/web/images/features-icon-light-c.png')}}" alt="features">
                        <h5>LeigonPay Gateway</h5>
                        <p>We will be offering a Debit Card very soon. This debit card can be used from all major ATM networks worldwide at any time. The service will be available after the launch of the Exchange Platform and LeigonPay Gateway.</p>
                    </div>
                </div><!-- .col  -->
                <div class="col-md-3">
                    <div class="features-box">
                        <img src="{{ asset('assets/web/images/features-icon-light-b.png')}}" alt="features">
                        <h5>Leigonphy Lending Feature</h5>
                        <p> We are not a lending platform, but we offer the option for those interested. This is not a pyramidal or referral program. Each user will be its own investor. We Guarantee 0.5% for any lending period and monthly 35% max back. We will be paying back in bitcoins.</p>
                    </div>
                </div><!-- .col  -->
                <div class="col-md-3">
                    <div class="features-box">
                        <img src="{{ asset('assets/web/images/features-icon-light-d.png')}}" alt="features">
                        <h5>Stake</h5>
                        <p>Don't just leave your coins idle, stake and earn more. The staking process will begin automatically except the wallet is locked. LNY makes sure its users make profits through different ways, and staking is one of those ways. Our staking program will be activated after the launching of our internal exchange.
</p>
                    </div>
                </div><!-- .col  -->
            </div><!-- .row  -->
            <div class="gaps size-2x d-none d-md-block"></div>
            <div class="row text-center justify-content-center">
                <div class="col-md-12">
                    <ul class="btns">
                        <li><a href="{{ asset('assets/web/documents/Leigonphy_Whitepaper.pdf')}}" target="_blank"  class="btn">Download Whitepaper</a></li>
                    </ul>
                    <div class="gaps size-1x d-none d-md-block"></div>
                    <a href="https://t.me/LeigonphyLNY" class="btn btn-simple"> <em class="fa fa-paper-plane"></em> Join us on Telegram</a>
                </div><!-- .col  -->
            </div><!-- .row  -->
        </div><!-- .conatiner  -->
        <div class="mask-ov-right mask-ov-s2"></div><!-- .mask overlay -->
    </div>
    <!-- Start Section -->


    <!-- Start Section -->
    <div class="section section-pad section-bg token-section section-gradiant" id="tokenSale">
        <div class="container">
                <div class="row text-center">
                    <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                        <div class="section-head-s2">
                            <h6 class="heading-xs">Tokens</h6>
                            <h2 class="section-title">ICO Crowdsale</h2>
                            <p>Leigonphy token will be released on the basis of Bitcoin platform. </p>
                        </div>
                    </div><!-- .col  -->
                </div><!-- .row  -->
                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <div class="row event-info">
                            <div class="col-sm-6">
                                <div class="event-single-info">
                                    <h6>Start</h6>
                                    <p>July 20, 2018 (04:00PM GMT+1)</p>
                                </div>
                            </div><!-- .col  -->
                            <div class="col-sm-6">
                                <div class="event-single-info">
                                    <h6>Number of tokens for sale</h6>
                                    <p>6.000.000 LNY </p>
                                </div>
                            </div><!-- .col  -->
                            <div class="col-sm-6">
                                <div class="event-single-info">
                                    <h6>End</h6>
                                    <p>August 4, 2018</p>
                                </div>
                            </div><!-- .col  -->
                            <div class="col-sm-6">
                                <div class="event-single-info">
                                    <h6>Tokens exchange rate</h6>
                                    <p>1 LNY = $0.35 - $0.60 - $0.70 - $0.80 - $0.90 - $1.00  </p>
                                </div>
                            </div><!-- .col  -->
                            <div class="col-sm-6">
                                <div class="event-single-info">
                                    <h6>Acceptable currencies</h6>
                                    <p>BTC</p>
                                </div>
                            </div><!-- .col  -->
                            <div class="col-sm-6">
                                <div class="event-single-info">
                                    <h6>Minimal transaction amount</h6>
                                    <p>BTC = $10</p>
                                </div>
                            </div><!-- .col  -->
                        </div><!-- .row  -->
                    </div><!-- .col  -->
                    <div class="col-lg-5">
                        <div class="countdown-box text-center">
                            <h6>ICO will start in</h6>
                            <div class="token-countdown d-flex align-content-stretch" data-date="2018/07/20 15:00:00"></div>
                            <a href="https://leigonphy.co/login" class="btn btn-alt btn-sm">Join &amp; BUY TOKEN NOW</a>
                        </div>
                    </div><!-- .col  -->
                </div><!-- .row  -->

                <div class="gaps size-3x"></div><div class="gaps size-3x d-none d-md-block"></div><!-- .gaps  -->

                <div class="row text-center">
                    <div class="col-md-6">
                        <div class="single-chart light res-m-btm">

                            <div>
                            </div>
                        </div>
                    </div><!-- .col  -->

                    <div class="col-md-6">
                        <div class="single-chart light">
                            <div> </div>
                        </div>
                    </div><!-- .col  -->
                </div>

        </div><!-- .container  -->
        <div class="roadmap_row text-center">
                <img src="{{ asset('assets/web/images/chart-blue-b.png')}}" width="1550" height="450" alt="roadmap"/><!-- .row  -->
        </div>
    </div>
    <!-- Start Section -->


    <!-- Start Section -->
    <div class="section section-pad section-bg-alt section-pro-alt roadmap-section" id="roadmap">
        <div class="container fullw">
            <div class="row text-center">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                    <div class="section-head-s2">
                        <h6 class="heading-xs">Roadmap</h6>
                        <h2 class="section-title">The Timeline</h2>
                        <p></p>
                    </div>
                </div><!-- .col  -->
            </div><!-- .row  -->
                <img src="{{ asset('assets/web/images/roadmap-v2.png')}}" alt="roadmap">
        </div><!-- .container  -->
    </div>
    <!-- Start Section -->

<!-- Start Section -->
    <div class="section section-pad section-bg team-section section-gradiant section-fix bg-fixed " id="exchange">
        <div class="bg-exchange">
            <div class="container ">
                <div class="row text-center">
                    <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                        <div class="section-head-s2">
                            <h6 class="heading-xs">Exchange & Trading</h6>
                            <h2 class="section-title">Exchange - Trading </h2>
                            <p>We will be lauching a prototype this year with well-established coins like Bitcoin, Ethereum, Litecoin, Bitcoin Cash, Dash and more coins including LNY getting easily traded.</p>
                        </div>
                    </div><!-- .col  -->
                </div><!-- .row  -->
                <div class="row"> <!-- FILA  -->
                    <div class="col-md-6 "><!-- COLUMNA  -->
                        <p>LNY platform will have a full trading system, where we accept popular coins as well as some new coins that have showed such acceptance in the world of cryptocurrencies. <br><br>LNY, with its innovative team made up of professionals, is there to create and advance the digital Cryptocurrency market. It brings the future to the present by creating a trading and exchange platform that allows the benefits of decentralization to reach our users, and spice up transactions with the anonymity undertone. </p>
                    </div>
                    <div class="col-md-6"><!-- COLUMNA  -->
                    </div>
                </div>  
            </div>
        </div>
    </div>
    <!-- Start Section -->
    <div class="section section-pad  payment-section section-pro section-fix " id="payment">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                    <div class="section-head-s2">
                        <h6 class="heading-xs">LeigonPay Gateway</h6>
                        <h2 class="section-title">Payment Gateway Worldwide</h2>
                        <p>We will be offering a Debit Card very soon. This card can be use in any ATM compatible with Visa and Mastercard at any time. The service will be available after the launch of the Exchange Platform.</p>
                    </div>
                </div><!-- .col  -->
            </div><!-- .row  -->
            <div class="row "> <!-- FILA  -->
                    <div class="col-md-6 "><!-- COLUMNA  -->
                        <p>Get the benefits of our Debit
                        Card,
                        Getting your LeigonPay Debit Card:
                        it can be purchased directly from your personal account, anyone can get it by using LNY funds (equivalent of only $10). First we have to check identity for security purpose.
                        Once received, you can use as much as $5,000 daily and withdraw up to $2,500 from all major ATM networks worldwide. By using LeigonPay card you will get 0.2% of the purchase value in LNY back. Coming Soon!
                        </p>
                    </div>
                <div class="col-md-6 text-right"><!-- COLUMNA  -->
                        
                        <img src="{{ asset('assets/web/images/card_2-01-min.png')}}" alt="credit-card">
                  </div>
            </div>
        </div>
    </div>
        <!-- Start Section -->


<!-- Start Section -->
    <div class="section section-pad section-bg team-section section-gradiant section-fix bg-fixed" id="lending">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                    <div class="section-head-s2">
                        <h6 class="heading-xs">Lending Program</h6>
                        <h2 class="section-title">Lending Plans</h2>
                        <p>This is not a pyramidal or referral program system. Each user will be its own investor. Our lending program is 100% based on bitcoin. We will be paying daily interest with bitcoin. Leigonphy already has an active Bitcoin mining farm which is part of the general sustenance of the platform and will also serve to protect our users in the lending program.</p>
                    </div>
                </div><!-- .col  -->
            </div><!-- .row  -->
            <div class="row"> <!-- FILA  -->
                <div class="col-md-2"></div>
                <div class="col-md-8 lending_table"><!-- COLUMNA  -->
 <table class="fullwidth">
                            <tbody>

                            <tr>
                            <td><strong>LEVEL</strong></td>
                            <td><strong>AMOUNT INVESTED</strong></td>
                            <td><strong>DAILY INTEREST</strong></td>
                            <td class="text-center"><strong>CAPITAL BACK</strong></td>
                            </tr>
                            <tr>
                                <td>Level 1</td>
                                <td>0.01 BTC - 0.1 BTC</td>
                                <td>Min 0.5% to 1.15 Max
                                    <br>

                                    daily interest
                                </td>
                            <td class="text-center">After 175 Days</td>
                            </tr>
                            <tr>
                            <td>Level 2</td>
                            <td>0.11 BTC - 0.5 BTC</td>
                            <td>Min 0.5% to 1.15 Max
                                <br>
                                +0,10 daily interest
                            </td>
                            <td class="text-center">After 140 Days</td>
                            </tr>
                            <tr>
                            <td>Level 3</td>
                            <td>0.51 BTC - 1 BTC</td>
                            <td>Min 0.5% to 1.15 Max
                                <br>
                                +0,20% daily interest
                            </td>
                            <td class="text-center">After 110 Days</td>
                            </tr>
                            <tr>
                            <td>Level 4</td>
                            <td>1 BTC or more</td>
                            <td>Min 0.5% to 1.15 Max
                                <br>
                                +0,30% daily interest
                                <br>
                            </td>
                            <td class="text-center">After 80 Days</td>
                            </tr>


                            </tbody>

                        </table>
                </div>
                <div class="col-md-2"></div>
            </div>  
        </div>
    </div>

    <!-- Start Section -->
    <div class="section section-pad section-bg-alt section-pro nopb" id="apps">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                    <div class="section-head-s2">
                        <h6 class="heading-xs">Ico Apps</h6>
                        <h2 class="section-title">ICO Mobile App</h2>
                        <p>Even if you are just starting out in the world of cryptocurrency, having access to your investments from the convenience of your phone can make it much easier to integrate crypto with your everyday life.</p>
                    </div>
                </div><!-- .col  -->
            </div><!-- .row  -->
            
            <div class="row align-items-center">
                <div class="col-md-6 res-m-btm">
                    <div class="text-block">
                        <p>We offer our own Wallet/App Where you can use all our features easy and safe.</p>
                        <ul>
                            <li>Multi-currency Wallet</li>
                            <li>Exchanging Crypto-assets and currencies</li>
                            <li>Trading System Integration</li>
                            <li>Payment Gateway Functionality</li>
                            <li>Staking System Functionality</li>
                        </ul>
                        <ul class="btns">
                            <li><a href="#" class="btn btn-sm">COOMING SOON</a></li><!-- 
                            <li>
                                <a href="#"><em class="fa fa-apple"></em></a>
                                <a href="#"><em class="fa fa-android"></em></a>
                                <a href="#"><em class="fa fa-windows"></em></a>
                            </li> -->
                        </ul>
                    </div>
                </div><!-- .col  -->
                <div class="col-md-6 mt-auto ">
                    <div class="">
                        <img src="{{ asset('assets/web/images/mobile-app.png')}}" alt="graph">
                    </div>
                </div><!-- .col  -->
            </div><!-- .row  -->

        </div><!-- .container  -->
    </div>
    <!-- Start Section --> 
    

        <!-- Start Section -->
    <div class="section section-pad faq-section section-gradiant section-bg" id="faq">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                    <div class="section-head-s2">
                        <h6 class="heading-xs">Faqs</h6>
                        <h2 class="section-title">Frequently Asked Questions</h2>
                    </div>
                </div><!-- .col  -->
            </div><!-- .row  -->
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-custom tab-custom-s2">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs text-center">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tab-1">GENERAL</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab-4">EXCHANGE & TRADING</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab-2">LENDING</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab-3">STAKE</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="tab-1">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="single-faq">
                                            <h5>What is ICO?</h5>
                                            <p>ICO stands for Initial Coin Offering. It is a way for the program to raise initial funds in order to complete their platform. Once you have purchased coins you will then be able to trade these coins on the exchanges that list them. Hopefully for a profit. Alternatively you can invest the coins into the lending programs.</p>
                                        </div>
                                    </div><!-- .col  -->
                                    <div class="col-md-6">
                                        <div class="single-faq">
                                            <h5>What cryptocurrencies can I use to purchase?</h5>
                                            <p>Leigonphy only accepts bitcoins in ICO phase. When the platform is in operation you can use another variety of currency.</p>
                                        </div>
                                    </div><!-- .col  -->
                                    <div class="col-md-6">
                                        <div class="single-faq">
                                            <h5>How can I participate in the ICO Token sale?</h5>
                                            <p>- Create an Account and Login to Leigonphy Platform.</p>
                                            <p>- Go to Bitcoin Wallet dashboard and deposit some bitcoins.</p>
                                            <p>- When first stage open, buy some LNY coins and be part of the platform and a happy holder of LNY.</p>   
                                        </div>
                                    </div><!-- .col  -->
                                    <div class="col-md-6">
                                        <div class="single-faq">
                                            <h5>How do I benefit from the ICO Token?</h5>
                                            <p>One of the advantages of investing in ICOs is the chance of investing in a new or upcoming technology. Every single ICO is established to revolutionize an industry in one way or the other. A careful analysis of ICOs could help investors get on board to the right startup.</p> 

                                            <p>ICOs follow the limited supply-demand principle, allowing their cryptocoins to gain value in the future. Its initial investors could leverage the economic prominence of the principle, increasing their chances of profiting exponentially.</p>
                                        </div>
                                    </div><!-- .col  -->
                                </div><!-- .row  -->
                            </div><!-- End tab-pane -->
                            <div class="tab-pane fade" id="tab-2">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="single-faq">
                                            <h5>What is a Lending Program?</h5>
                                            <p>A Lending Program allows you to lend your bitcoin in return for interest. Generally daily. The programs invest your bitcoin for you and pay you a portion of the profits. Generally around 1.5% a day.</p>
                                        </div>
                                    </div><!-- .col  -->
                                    <div class="col-md-6">
                                        <div class="single-faq">
                                            <h5>Which program should I choose?</h5>
                                            <p>We aim to provide as much information as we can in order to help you choose. Read up on the programs and decide which program you feel comfortable investing in.</p>
                                        </div>
                                    </div><!-- .col  -->
                                    <div class="col-md-6">
                                        <div class="single-faq">
                                            <h5>Are Lending Programs Safe?</h5>
                                            <p>All of the programs should be considered risky and you should do as much research on the programs prior to investing. You should also not invest anything more than you can afford to lose. Leigonphy has its own mining farm to make sure the stability of the platform.</p>
                                        </div>
                                    </div><!-- .col  -->
                                    <div class="col-md-6">
                                        <div class="single-faq">
                                            <h5>How do I lend bitcoin? & How Often is Interest Paid?</h5>
                                            <p>Once you have registered to the program you generally need to purchase the coin associated with the program. Investor choose the level 1 program for example: investor deposits BTC into our system, investor needs to exchange the BTC for our coins and put that into lending program. 

                                            Interest is paid daily and starts 24 hours after your first deposit.</p>
                                        </div>
                                    </div><!-- .col  -->
                                </div><!-- .row  -->
                            </div><!-- End tab-pane -->
                            <div class="tab-pane fade" id="tab-3">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="single-faq">
                                            <h5> What is Proof of Stake (PoS)?</h5>
                                            <p>The Proof of Stake (POS) involves the distribution process of rewards of block to aid the maintaining of the network's security via the proof of holding. For staking the LNY coin, the user is expected to earn a bonus payment of LNY coin which depends on the number of coins staked, and how long.
. </p>
                                        </div>
                                    </div><!-- .col  -->
                                    <div class="col-md-6">
                                        <div class="single-faq">
                                            <h5>How Can My Leigonphy Wallet Be Back-upped?</h5>
                                            <p>For your wallet to be back-upped, make a spare copy of the wallet.bat file. After doing that, the file then contains the user's encrypted password and private key. It is necessary that another copy should be kept separate from the one on your device in the case the system fails.</p>
                                        </div>
                                    </div><!-- .col  -->
                                    <div class="col-md-6">
                                        <div class="single-faq">
                                            <h5>What Wallet Can Be Used To Hold Leigonphy Coins?</h5>
                                            <p>The LNY website has a variety of wallets, ranging from the Windows through the Mac OS X to the Linux Wallets on its website.</p>
                                        </div>
                                    </div><!-- .col  -->
                                    <div class="col-md-6">
                                        <div class="single-faq">
                                            <h5>Does My Desktop Wallet Have To Be Connected Every time?</h5>
                                            <p>PoS is different from PoW, that means the staking process works differently from mining. Here, your wallet is not required to be left open or connected every second during your aging or staking period.</p>
                                        </div>
                                    </div><!-- .col  -->
                                </div><!-- .row  -->
                            </div><!-- End tab-pane -->
                            <div class="tab-pane fade" id="tab-4">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="single-faq">
                                            <h5>What are network fees?</h5>
                                            <p>Each time any amount of cryptocurrency is sent or exchanged, the currency goes through a blockchain. This process requires a small fee called a ‘network fee’. The network fee is a commission that a blockchain takes from the amount sent in each currency. It is critical to make sure that the amount you want to exchange on Leigonphy is enough to cover the exchange fees for both the process of sending your original currency and receiving the new currency you want to buy. If the sent amount is too low to cover the fees of the network, the transaction will not go through and your funds will be lost.</p>
                                        </div>
                                    </div><!-- .col  -->
                                    <div class="col-md-6">
                                        <div class="single-faq">
                                            <h5>What coins can you exchange? </h5>
                                            <p>Leigonphy lets you exchange one kind of cryptocurrency for another. We will support the following currencies: BTC - ETH - LTC - DASH - BCH and LNY We plan to support additional currencies, please contact us if you have any in mind, <a href="mailto:info@leigonphy.co">info@leigonphy.co</a>.</p>
                                        </div>
                                    </div><!-- .col  -->
                                    <div class="col-md-6">
                                        <div class="single-faq">
                                            <h5>What happens if my transaction doesn’t go through?</h5>
                                            <p>If your transaction fails, contact us at <a href="mailto:info@leigonphy.co">info@leigonphy.co</a> or fill out our request form. When you reach out, let us know your transaction ID (TXID). Don't forget to include your “refund address”. In case the transaction fails for any reason, we can return the funds to you using this address.</p>
                                        </div>
                                    </div><!-- .col  -->
                                    <div class="col-md-6">
                                        <div class="single-faq">
                                            <h5>Are you using a third party exchange or API in the backend?</h5>
                                            <p>No. The entire system is built in-house. Exchange is entirely done through public blockchains. We have reserves for each coin we support so some coins might be dynamically turned on/off based on reserve level. Eventualy we will be support additional currencies.</p>
                                        </div>
                                    </div><!-- .col  -->
                                </div><!-- .row  -->
                            </div><!-- End tab-pane -->
                        </div><!-- End tab-content -->
                    </div><!-- End tab-custom -->
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- End container -->
        <div class="mask-ov-left mask-ov-s5"></div><!-- .mask overlay -->
    </div>
    <!-- End Section -->


    <!-- Start Section -->
    <div class="section section-pad section-bg  section-pro contact-section" id="contact">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-6 offset-lg-3">
                    <div class="section-head-s2">
                        <h6 class="heading-xs">Contact</h6>
                        <h2 class="section-title">Get In Touch</h2>

                    <p>Any question? Reach out to us and we’ll get back to you shortly.</p>
                    </div>
                </div><!-- .col  -->
            </div><!-- .row  -->
            <div class="row">
                <div class="col-lg-4">
                    <ul class="contact-info-alt">
                        <li><a href="mailto:info@leigonphy.co"><em class="fa fa-envelope"></em><span>info@leigonphy.co</span></a></li>
                        <li><a href="https://t.me/LeigonphyLNY"><em class="fa fa-paper-plane"></em><span>Join us on Telegram</span></a></li>
                    </ul>
                </div><!-- .col  -->
                <div class="col-lg-4 text-center">
                    <ul class="contact-info-alt">
                            <li><a href="mailto:support@leigonphy.co"><em class="fa fa-envelope"></em><span>support@leigonphy.co</span></a></li>
                    </ul>
                </div><!-- .col  -->
                <div class="col-lg-4 text-center">
                    <ul class="contact-info-alt">
                            <li><a href="mailto:contact@leigonphy.co"><em class="fa fa-envelope"></em><span>contact@leigonphy.co</span></a></li>
                    </ul>
                </div><!-- .col  -->
            </div><!-- .row  -->
        </div><!-- .container  -->
        <div class="mask-ov-right mask-ov-s6"></div><!-- .mask overlay -->
    </div>
    <!-- End Section -->
    


 
@endsection
