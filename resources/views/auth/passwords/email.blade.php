@extends('front.layouts.master')

@section('content')
        <div id="header" class="banner banner-full d-flex align-items-center">

            <div class="container">
                <div class="banner-content no-padding">

                    <div class="row align-items-center mobile-center">
                        <div class="col-lg-12 col-md-12 \">
                            <div class="header-txt">
                                <div class="countdown-box text-center">
                  <h2 class="section-title">Password Recovery</h2>
                </div>
                  <section  class="circle-section section-padding section-background">
                    <div class="container">
                      <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6 register-lny">
                          <div class="login-admin login-admin1">
                            <div class="login-header text-center">
                              ENTER YOUR EMAIL TO RESET PASSWORD
                            </div>
                            <div class="login-form text-center input-fullwidth">
                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                          <div class="row">
                            <div class="col-sm-12 form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                              <span class="input-group-addon"><i class="fa fa-at fa" aria-hidden="true"></i></span>
                                <input id="email" type="email" placeholder="Enter Your Email" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                          </div>
                      <div class="row">
                            <div class="col-sm-12 form-group">
                             <input value="Send Password Reset Link" type="submit">
                        </div>
                        <?php if (isset($_GET["res"]) && $_GET["res"] == "1") {?>
                        <div class="text-center" style="width: 100%; color: #9CD735"><strong>The email has been sent</strong></div>
                        <?php } ?>
                      </div>
                    </form>                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>


      </div>
    </div><!-- .col  -->
  </div><!-- .row  -->
</div><!-- .banner-content  -->
</div><!-- .container  -->
</div>
<!-- End Banner/Slider -->
</div><!-- .container  -->
</div><!-- .header-partners  -->
</header>

@endsection
