@extends('front.layouts.master')
@section('content')

        <div id="header" class="banner banner-full d-flex align-items-center">

            <div class="container">
                <div class="banner-content no-padding">

                    <div class="row align-items-center mobile-center">
                        <div class="col-lg-12 col-md-12">
                            <div class="header-txt">
                                <div class="countdown-box text-center">
                  <h2 class="section-title">Log in</h2>
                </div>
                  <section  class="circle-section section-padding section-background">
                    <div class="container">
                      <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6 register-lny">
                          <div class="login-admin login-admin1">
                            <div class="login-header text-center">
                              
                            </div>
                            <div class="login-form text-center input-fullwidth">
                              <form method="POST" action="{{ route('postLogin') }}">
                                {{ csrf_field() }}

                                @if ($errors->has('username'))
                                <span class="help-block">
                                  <strong>{{ $errors->first('username') }}</strong>
                                </span>
                                @endif
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                                        <input name="username" placeholder="Username" type="text" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <span class="input-group-addon"><i class="fa fa-key fa" aria-hidden="true"></i></span>
                                        <input name="password" placeholder="Password" type="password">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <input value="Login" type="submit">
                                    </div>
                                </div>
                      <div class="row">
                         <div class="col-md-3"></div>
                        <div class="col-md-6 form-group text-center font-white">
                          <a href="{{ route('password.request') }}">Forgot Password</a> 
                        </div>
                      </div>

                              </form>
                              <!-- <div class="text-center" style="text-transform: uppercase;">
                                <br><br>
                                <a href="{{ route('password.request') }}">Forgot Password</a> | <a href="{{ route('register') }}">Register</a> 
                                <br><br>
                              </div>-->

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
</div>
                        </div><!-- .col  -->
                    </div><!-- .row  -->
                </div><!-- .banner-content  -->
            </div><!-- .container  -->
        </div>
        <!-- End Banner/Slider -->
        </div><!-- .container  -->
        </div><!-- .header-partners  -->
    </header>






@endsection    
  