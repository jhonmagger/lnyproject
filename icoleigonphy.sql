-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

USE `icoleigonphy`;

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `abouts`;
CREATE TABLE `abouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `heading` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `video` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `abouts` (`id`, `heading`, `details`, `video`, `created_at`, `updated_at`) VALUES
(1,	'About',	'<span style=\"color: rgb(84, 84, 84); font-family: arial, sans-serif; font-size: small;\">Exchange, Trading, Lending and Payment Gateway, everything you need about cryptocurrency you will find it here.</span>',	NULL,	'2017-11-09 04:45:07',	'2018-04-07 17:03:44');

DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`),
  UNIQUE KEY `admins_username_unique` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `admins` (`id`, `name`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1,	'MR. Admin',	'rajman.test12@gmail.com',	'admin',	'$2y$10$s3adlwcTUZMvYEhfN1liQu78t.qDxpr7BQhexEOZz/LvdfvUB7T1W',	'VreEF3zszEZGDt2yBIaZytLGkC7MuFU9YLC6GCEhh5czBgXwh6og8g5Yb44Z',	NULL,	'2018-04-19 01:27:32');

DROP TABLE IF EXISTS `admin_password_resets`;
CREATE TABLE `admin_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `admin_password_resets_email_index` (`email`),
  KEY `admin_password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `avatars`;
CREATE TABLE `avatars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `avatars` (`id`, `user_id`, `photo`, `created_at`, `updated_at`) VALUES
(1,	1,	'1.png',	'2017-11-12 04:10:46',	'2017-11-12 04:10:46'),
(2,	3,	'3.png',	'2017-11-29 06:24:10',	'2017-11-29 06:24:10'),
(3,	25,	'25.png',	'2018-03-17 22:18:34',	'2018-03-17 22:18:34'),
(4,	42,	'42.png',	'2018-03-31 05:18:48',	'2018-03-31 05:18:48'),
(5,	52,	'52.png',	'2018-04-01 05:48:41',	'2018-04-01 05:48:41'),
(6,	67,	'67.png',	'2018-04-17 00:23:20',	'2018-04-17 00:23:20');

DROP TABLE IF EXISTS `bitcoinaddress`;
CREATE TABLE `bitcoinaddress` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `address_label` varchar(500) CHARACTER SET latin1 NOT NULL,
  `bitcoin_address` varchar(500) CHARACTER SET latin1 NOT NULL,
  `private_key` varchar(500) CHARACTER SET latin1 NOT NULL,
  `public` varchar(500) CHARACTER SET latin1 NOT NULL,
  `address_type` varchar(500) CHARACTER SET latin1 NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `bitcoinaddress` (`id`, `user_id`, `address_label`, `bitcoin_address`, `private_key`, `public`, `address_type`, `created_at`, `updated_at`) VALUES
(44,	24,	'btcaddress',	'1Kkat1U5G6P5WpCfza6ytqpBdFojWdq6kL',	'a68ff6339fd5bbbe4349d75b6ac42cde4e018780bced1c1d2dc139e00071ac31',	'030346c60c9b764b8e960151ff90ecd8fc9936baaec7bdcd3d76943cb3302ac99a',	'BTC',	NULL,	NULL),
(45,	25,	'btcaddress',	'1ELAmpbZixaJ6u3HfpY7YZSvGLmBqhCZEt',	'9036be5f1b35f7d61cbcef8eba6bc3d55771fed3c8b55cf888c92ba82b76d2f1',	'021046acd712b600199bb9cd5a30453f2fbcc61f1ed16606b29fe1075ac2c230eb',	'BTC',	NULL,	NULL),
(46,	30,	'btcaddress',	'141ZEgnks5qxwdNX5Vn2CNZV5Gt58E1EGY',	'32b875c77328f460a5dcbd3071a968cb5aa0724fdd68bb6488c1b09ec42c5822',	'02735a2a4f9a99dbfad80acd40532e422a8609abe132005fde2a94fd8c7f6a7e82',	'BTC',	NULL,	NULL),
(47,	32,	'btcaddress',	'162wghLydr3pobSRq8hnQY1kp1966PdnHX',	'95d70ae76cbdb29c35a9a210ecd46677da3bebd3f13876c14c9f021eb384499c',	'03a541f713b06d1e03dbee26f6e1601900e255bdb01bc6c0206ef09018cad269bf',	'BTC',	NULL,	NULL),
(48,	36,	'btcaddress',	'1NWibbMyZaxxjpFSBeLM32tr2jbkcfyyid',	'341b67d96ce184b6be2dfc702d033926e2121f8046ec9b25eefc2be19eec6dac',	'025187eb680f1b80fe2b97e3ba61f6d33f5d60cea9299eacbe9a6dca6c5f7bd4ab',	'BTC',	NULL,	NULL),
(49,	37,	'btcaddress',	'16Hqd1Gk1zru3676W49wZaBRGEbhefSpao',	'e514bbba99f86e033699cc1b4a4830211a2bc39c7a42578b6b66176ad3b137d5',	'03f9ff6664a6c7a910d990fa9cc00eba2114bcf96a272f76a05e9c7a6b691b9667',	'BTC',	NULL,	NULL),
(50,	38,	'btcaddress',	'14FH3SH8bx1nkWBE9agyAXaxGBrV2UtH7i',	'71d832dc49e0a52fad21291c9decabd6255940b03ac625db85a3b6a91a497694',	'03385271905257b62ed32bb402725819f489dfc29f5b4a6e80b0317948d904a946',	'BTC',	NULL,	NULL),
(51,	26,	'btcaddress',	'17xWAY8PCBHrFhfXvN8F1EEQ4E8KS51YtW',	'9e8af3c541a59972321ac4c3aea04c6d4c058eba3b657e26ab43527c1fe2b2da',	'03a8d47e1a6793b9f7c00135a8c5c8e7272e3fb1cd646b9a12eafbc895faa31a32',	'BTC',	NULL,	NULL),
(52,	39,	'btcaddress',	'14f7YdCCAj7Equ7U22uFSZ2KwznfLeFoMQ',	'efb7659fbb9f3f55baf39b0652797322261710a8f7f3a2fc9a01f67bfa3c3f26',	'035ff9a9f00a15b72182ce0b1a4995cdd8c940117defc0c07707aae19fb417fc4d',	'BTC',	NULL,	NULL),
(53,	46,	'btcaddress',	'1Lfd9YH2EBjrJGcuLdgGn2SLbKTZqsGfAs',	'3ba8410eda836c98853fa892f9aaf426828098e4af316822282a413cc88e79a9',	'02b3c30017aaa5c3b8eb6cedca58f390ab5537b8f477052ea106a093e37c91dd08',	'BTC',	NULL,	NULL),
(54,	42,	'btcaddress',	'19nE9RmdXoju7K9KKxWmxNQPTN8ECvaMX3',	'2f72f77821288cfacd0dc67c1f309d55d47e20c852c42e71ed128147afafcec0',	'0351130a2709e19ceb00ae99d5b5ce3c1b7a41253c4ffe585914e03b4a400c5aee',	'BTC',	NULL,	NULL),
(55,	51,	'btcaddress',	'1HjARRc3Mcsz6yEN2HRekv7NQFRTz1uViR',	'6629848915f2ebc0e45c9045b39704462da537dc49506fdba8e11e2b26c68fd9',	'037e21618d5f37758f079418ddb4a7ad70dc7f5ec9571fdd7b493b0ae15be97c71',	'BTC',	NULL,	NULL),
(56,	47,	'btcaddress',	'1Pht25GHSAKEdGa81h1N7eDUudpW4qzTx4',	'68e6dd1570bd994cb43a864a347e31d15f05db421d944e797a740c7ec32d21d4',	'032ebdf79ac3dddd107fe888d169f956d22309b3858348045e886f549479f017eb',	'BTC',	NULL,	NULL),
(57,	52,	'btcaddress',	'1JJ3UJJCZ1pY3RXtT4kaT4wAmdfin8AkPQ',	'af15edaf5d4cd92a980dfacebd773a50f3f2859be33d48d0af7a778dfb2ddb41',	'0272ff70f582867700230468d7a9c870b2c620ca37e6fbf74a6d4435cf3547ed1a',	'BTC',	NULL,	NULL),
(58,	53,	'btcaddress',	'1ZgzCEJN6ws3VKvex8XJw98v2zR5hZ8Uu',	'f027a5ffaef84e9fa660764b6e52a729239138d458380e4123c02e8d6c23584c',	'0240aedc26fdcd8a6e3cbb94760c48835851be324ea72a5be2d047447598b4d69c',	'BTC',	NULL,	NULL),
(59,	54,	'btcaddress',	'1MEKGr5Aas3q9B6Npd8LjGBkGjKLdwX4Tp',	'ed3403da5ba0ae33896abc430233b3f2fa9eada750593f9b62f02e5a2b7fa3d7',	'03708768506563a796f0fa154b0e16a4ca27b2696000d74ca448e3c7939262fac1',	'BTC',	NULL,	NULL),
(60,	59,	'btcaddress',	'19RvwFeqGEaegJByGV9MymEgSyAjfnUt43',	'5d99757adbffb3b2d75c7b447c3acb5f78641c4f9bb53cc6c1a7f407746123f3',	'0250d4d1f0be62b9e3de909c007c0fac645a983572619d5045a7578b213a08251d',	'BTC',	NULL,	NULL),
(61,	67,	'btcaddress',	'1HYfBk9226CfcDhXEzfdu9UqV2GXEBW388',	'fd14278a40922ed9645a6d180ca8b6a82f7cc92192b6ede8af60c7c338871896',	'02ca98bdf0dbf20760fb9959c2b85014bf4d03abfa693486fa9892ae1d90e5ef6e',	'BTC',	NULL,	NULL),
(62,	69,	'btcaddress',	'14jp1yHs233Tye4CJGFC7JWoyLyBDgifNe',	'd0b3b7aa74ef2f8e561b6bb4cb7b0723af28a389692c3381bb6372ad790d6e5c',	'037c4cba4941b1e4f23fe122e80e6bbae6b8f528bc4b398f12c5b2fe7c5c522fc4',	'BTC',	NULL,	NULL),
(63,	74,	'btcaddress',	'18eYEZGFFboEWqFk7Rz8tA6npEemkGV7xp',	'ae00eee8095f402b0b38decaa27d8aefaf0d73ee7c19eef677e13c4eae55f4af',	'03b82520dd5a1ee2046dbb22a95901d3b01fdb7f26c37085edb3968d3c49a7607a',	'BTC',	NULL,	NULL),
(64,	76,	'btcaddress',	'1E8JK3pe5rp2Hia53RfeMfTpnng64i6Rq2',	'e7e3e4c34aef62b0587e5fba435a0bd3fa047a3e4985f02dcd51f5f4d0788e4a',	'021c48e10a7f127c78577b49e62cc518ccbc01b67185bacce4092d46867321ff4f',	'BTC',	NULL,	NULL),
(65,	75,	'btcaddress',	'1HLga2B94zjBePRfvhtBbYfeoQTZUt7BFu',	'eb9980e3f84b98eba8fccb8a99dc6ed10c8e4d16d2633518d649c2a59dd64734',	'0281196dfd8d1b24d45216fe63a0a13080e478e9f651ff4ebb682e4bdbf3d5c4c8',	'BTC',	NULL,	NULL),
(66,	68,	'btcaddress',	'1KosZz83PMaUm8GmB5u2kmSxBNjFogW8Jx',	'20d752b92036b7eb5d182a24873e9bb316c7328152c658ff4008cf3216a68deb',	'032b4114ea7127c53591db583adae25b579183f2583c3d9767bfaa11f3e9a5009a',	'BTC',	NULL,	NULL),
(67,	70,	'btcaddress',	'123jiDXSAdUsdhgC7EnLVXwTaXzfbJdwK3',	'c01af4a08acaf70928d3985ffbf4000ab9d5c68cc26968876715b940ec161c86',	'03a0984daa14c2bb9b0b8bab89e6ac8d648a42b5c00e586931bc67481443795289',	'BTC',	NULL,	NULL),
(68,	71,	'btcaddress',	'1HLcVfE5wxFBi3WPYN1mEXcrtLq41ekmPi',	'940b8b16b98f0bdf6e52284139e266814af0426dbdc8b8df889244eb90ba86ce',	'0213da7d6e51410e1987727b784eb6a2610edc56666552138e09af7c570e1bfbb8',	'BTC',	NULL,	NULL),
(69,	78,	'btcaddress',	'1MqFVfQ3qon3uyoGUdvQur5NSQUmRbwQWg',	'3130f6ccf817e13c359d07b5c30cca148c8be5bc355b3028c31ce6293e6dcea8',	'03257f1889db4689e4203f2287c5410163ca4496aef36384f12c93bb6d8a93660c',	'BTC',	NULL,	NULL),
(70,	83,	'btcaddress',	'16iNULoFWmy4hHVCfc91hKpVKJL1UVXRtj',	'73e0c4807633930b4cc67bc9e1dfedc4e5b0f5082ce57441007c4612b82b7ce5',	'02b77376a4da981be1d89283dca01af20b9001979c11c891427b5e50507d04f9a3',	'BTC',	NULL,	NULL),
(71,	86,	'btcaddress',	'17qTUgcTeEbNxmtTvKLR3pBfRrXkYz2Q4S',	'70eca0ec43716a2510da7ceaacbc8140e47c0e17a8f4de1a1942ca2d581c952c',	'03d74fd3aaf04ebce20712038e4e1baa4b91589bb89c80a9152a392074c8cc2ecb',	'BTC',	NULL,	NULL);

DROP TABLE IF EXISTS `bitcoinhistory`;
CREATE TABLE `bitcoinhistory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `address` varchar(500) CHARACTER SET latin1 NOT NULL,
  `tx_hash` varchar(500) CHARACTER SET latin1 NOT NULL,
  `value` varchar(500) CHARACTER SET latin1 NOT NULL,
  `main` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `bitcoinhistory` (`id`, `user_id`, `address`, `tx_hash`, `value`, `main`, `time`, `created_at`, `updated_at`) VALUES
(15,	75,	'1ELAmpbZixaJ6u3HfpY7YZSvGLmBqhCZEt',	'6d28fa9b985e7cb7b526ce8b66a4ea66d759b501e57afc0fbe0fed1a440aa7bc',	'5',	1,	'2018-03-10 14:55:28',	NULL,	'2018-03-27 17:45:56'),
(16,	66,	'162wghLydr3pobSRq8hnQY1kp1966PdnHX',	'44f8c0e59f6c1e3a934d589f6ca09579fb9c8b4188b6e819f0f2ffd1064364bc',	'5.000112',	1,	'2018-03-10 20:53:03',	NULL,	'2018-03-27 13:08:19'),
(17,	65,	'14FH3SH8bx1nkWBE9agyAXaxGBrV2UtH7i',	'0c8ce21c56ada8fe7ea4c95bc90acabedaf4085b20ea6ee226bff333acce603b',	'5.000547',	1,	'2018-03-13 18:34:16',	NULL,	'2018-03-28 09:23:52'),
(18,	53,	'14FH3SH8bx1nkWBE9agyAXaxGBrV2UtH7i',	'50b5694c50e362c3ed68f5b770dfb2ab32df9bed2a2146ec5d39ee18e0583c65',	'5.000218',	1,	'2018-03-13 18:58:19',	NULL,	'2018-03-28 09:23:52'),
(19,	67,	'14FH3SH8bx1nkWBE9agyAXaxGBrV2UtH7i',	'8ccead55a4a56e81e5bc2e194df4dca54d6c4a3081ea7dc0c9429750e8a87650',	'5.500250',	1,	'2018-03-17 22:42:25',	NULL,	'2018-03-28 09:23:52'),
(20,	68,	'14FH3SH8bx1nkWBE9agyAXaxGBrV2UtH7i',	'50b5694c50e362c3ed68f5b770dfb2ab32df9bed2a2146ec5d39ee18e0583c65',	'5',	1,	'2018-04-02 13:34:45',	NULL,	NULL),
(21,	69,	'14FH3SH8bx1nkWBE9agyAXaxGBrV2UtH7i',	'8ccead55a4a56e81e5bc2e194df4dca54d6c4a3081ea7dc0c9429750e8a87650',	'5',	1,	'2018-04-02 13:35:19',	NULL,	NULL),
(22,	70,	'14FH3SH8bx1nkWBE9agyAXaxGBrV2UtH7i',	'8ccead55a4a56e81e5bc2e194df4dca54d6c4a3081ea7dc0c9429750e8a87650',	'5',	1,	'2018-04-02 13:35:53',	NULL,	NULL),
(23,	71,	'14FH3SH8bx1nkWBE9agyAXaxGBrV2UtH7i',	'8ccead55a4a56e81e5bc2e194df4dca54d6c4a3081ea7dc0c9429750e8a87650',	'5',	1,	'2018-04-02 13:36:52',	NULL,	NULL),
(24,	78,	'1MqFVfQ3qon3uyoGUdvQur5NSQUmRbwQWg',	'8ccead55a4a56e81e5bc2e194df4dca54d6c4a3081ea7dc0c9429750e8a87650',	'7',	1,	'2018-04-18 00:14:50',	'2018-04-18 00:14:50',	'2018-04-18 00:14:50');

DROP TABLE IF EXISTS `bitcoinsend`;
CREATE TABLE `bitcoinsend` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `balance` varchar(500) CHARACTER SET latin1 NOT NULL,
  `hash_id` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `confirm` varchar(500) CHARACTER SET latin1 NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `bitcoinsend` (`id`, `user_id`, `balance`, `hash_id`, `confirm`, `time`, `created_at`, `updated_at`) VALUES
(8,	25,	'18173',	'ef416c01f429c9d876cb73b8596df56064b438ffb572c791a40b57d4b591c299',	'1',	'2018-03-10 15:14:48',	NULL,	NULL),
(9,	32,	'8200',	'83a725649ce14c914acb95d4a7b55db88ffe1323a96f276478d73975a6e68a8f',	'1',	'2018-03-10 22:04:10',	NULL,	NULL),
(10,	38,	'51747',	'd446ae86a4467d89d4da0fcc830bdf9ba7713bf46d1052b780b6d0e35a1736ef',	'1',	'2018-03-13 18:42:05',	NULL,	NULL),
(11,	38,	'18786',	'52cf1a5e4007a7577e9718a345bb3049926d5cf67f7532a27927a229d15f5b67',	'1',	'2018-03-13 19:08:04',	NULL,	NULL),
(13,	38,	'22000',	'ce7f7409b0a54d0cebccce76c0fa94bba56b6ec40c619e1d83e74babf45dfd94',	'1',	'2018-03-17 22:53:03',	NULL,	NULL);

DROP TABLE IF EXISTS `charges`;
CREATE TABLE `charges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trancharge` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '00',
  `trncrgp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '00',
  `basep` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '00',
  `varp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '00',
  `convcrg` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `charges` (`id`, `trancharge`, `trncrgp`, `basep`, `varp`, `convcrg`, `created_at`, `updated_at`) VALUES
(1,	'0',	'0',	'0',	'0',	'0',	'2017-11-09 05:23:17',	'2018-04-18 00:22:35');

DROP TABLE IF EXISTS `coin_payment`;
CREATE TABLE `coin_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gateway_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `trxid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `charge` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inusd` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `coin_payment` (`id`, `gateway_id`, `user_id`, `trxid`, `amount`, `charge`, `inusd`, `details`, `status`, `created_at`, `updated_at`) VALUES
(755,	5,	51,	'ao8niTjQoQ5FlDF6',	10000000,	'0',	'0',	'0',	1,	'2018-04-01 05:22:49',	'2018-04-01 05:22:49'),
(765,	5,	52,	'enCm7sWLwXhOmJKy',	75,	'0',	'0',	'0',	1,	'2018-04-02 11:43:53',	'2018-04-02 11:43:53'),
(766,	5,	55,	'MH35YDQvsjVR7Fl4',	10,	'0',	'0',	'0',	1,	'2018-04-02 13:18:09',	'2018-04-02 13:18:09'),
(780,	5,	57,	'NxwLqfx1glziLvxr',	500,	'0',	'0',	'0',	1,	'2018-04-02 15:51:09',	'2018-04-02 15:51:09'),
(834,	5,	65,	'rnI3P7CjS91Ogjob',	15,	'0',	'0',	'0',	1,	'2018-04-03 14:03:22',	'2018-04-03 14:03:22'),
(835,	5,	54,	'EdzpQVKKtZF6veIY',	1705.7100000000003,	'0',	'0',	'0',	1,	'2018-04-03 15:12:27',	'2018-04-03 15:12:27'),
(836,	5,	51,	'KsmD1l8KTRupV3NL',	0.02,	'0',	'0',	'0',	1,	'2018-04-03 15:12:35',	'2018-04-03 15:12:35'),
(837,	5,	57,	'Dzgq6mMaBsbQ8Fi7',	0.02,	'0',	'0',	'0',	1,	'2018-04-03 15:12:41',	'2018-04-03 15:12:41'),
(867,	5,	68,	'7rHSa57jlTNCiUir',	135,	'0',	'0',	'0',	1,	'2018-04-03 23:11:49',	'2018-04-03 23:11:49'),
(882,	5,	67,	'A9Fo0G3SssGKYG6P',	18.46,	'0',	'0',	'0',	1,	'2018-04-03 23:33:58',	'2018-04-03 23:33:58'),
(962,	5,	67,	'pNicgo0n7QQiC9wW',	5,	'0',	'0',	'0',	1,	'2018-04-04 09:16:48',	'2018-04-04 09:16:48'),
(974,	5,	67,	'AJUh4cusAB2viQW5',	212.5,	'0',	'0',	'0',	1,	'2018-04-15 21:16:55',	'2018-04-15 21:16:55'),
(975,	5,	65,	'TzGUHuL9RPuGxa4z',	0.25,	'0',	'0',	'0',	1,	'2018-04-15 21:17:00',	'2018-04-15 21:17:00'),
(976,	5,	68,	'gv3daO1Lx4bfOMhQ',	0.55,	'0',	'0',	'0',	1,	'2018-04-15 21:17:04',	'2018-04-15 21:17:04'),
(987,	5,	67,	'RydIKIAxMRDKT8b8',	245,	'0',	'0',	'0',	1,	'2018-04-18 00:22:59',	'2018-04-18 00:22:59');

DROP TABLE IF EXISTS `contacs`;
CREATE TABLE `contacs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'email@example.com',
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0138283',
  `location` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `contacs` (`id`, `email`, `mobile`, `location`, `created_at`, `updated_at`) VALUES
(1,	'example@email.com',	'0183839098',	'Uttara, Dhaka, Bangladesh',	'2017-11-09 04:56:45',	'2017-11-09 04:57:02');

DROP TABLE IF EXISTS `deposits`;
CREATE TABLE `deposits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gateway_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `trxid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `charge` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inusd` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `docvers`;
CREATE TABLE `docvers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `docvers` (`id`, `user_id`, `name`, `photo`, `details`, `created_at`, `updated_at`) VALUES
(1,	1,	'Passport',	'5a0ff6ed52127.jpg',	'aASDKLJ klJASDJKA AFJKDFKJ',	'2017-11-18 03:01:33',	'2017-11-18 03:01:33'),
(2,	3,	'NID',	'5a1012eff0ff1.jpg',	'Nid safj Sadh sadfjfs',	'2017-11-18 05:01:04',	'2017-11-18 05:01:04');

DROP TABLE IF EXISTS `footers`;
CREATE TABLE `footers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `heading` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Footer Heading',
  `text` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `footers` (`id`, `heading`, `text`, `created_at`, `updated_at`) VALUES
(1,	'LEIGONPHY',	'<font color=\"#ff9900\">                                                LEIGONPHY INC</font>',	'2017-11-09 05:18:45',	'2018-04-07 17:05:04');

DROP TABLE IF EXISTS `gateways`;
CREATE TABLE `gateways` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gateimg` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minamo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `maxamo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `charged` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chargep` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `val1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `val2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `gateways` (`id`, `name`, `gateimg`, `minamo`, `maxamo`, `charged`, `chargep`, `rate`, `val1`, `val2`, `currency`, `status`, `created_at`, `updated_at`) VALUES
(7,	'BitCoin',	'5a151c9285893.png',	'0',	'0',	'0',	'0',	'0',	'3PURrSJ7cPLBN72uuMWG6Qw4WgfPCLpdQJ',	NULL,	'0',	1,	NULL,	'2018-03-17 22:39:08');

DROP TABLE IF EXISTS `gatewaystoken`;
CREATE TABLE `gatewaystoken` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gateimg` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `val2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `gatewaystoken` (`id`, `name`, `gateimg`, `val2`, `status`, `created_at`, `updated_at`) VALUES
(1,	'BitCoins',	'5a151c9285893.png',	'1283f2c7a5ce4f78b4922bbae1ce51dc',	1,	NULL,	'2018-03-01 00:16:51');

DROP TABLE IF EXISTS `gsettings`;
CREATE TABLE `gsettings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `webTitle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'THESOFTKING',
  `subtitle` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `colorCode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '336699',
  `curCode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Dollor',
  `curSymbol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '$',
  `registration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `emailVerify` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `smsVerify` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `decimalPoint` int(11) NOT NULL DEFAULT '2',
  `emailNotify` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `smsNotify` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `emailMessage` text COLLATE utf8mb4_unicode_ci,
  `emailSender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'email@example.com',
  `smsApi` text COLLATE utf8mb4_unicode_ci,
  `walletaddress` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `btcrate` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `startdate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `gsettings` (`id`, `webTitle`, `subtitle`, `colorCode`, `curCode`, `curSymbol`, `registration`, `emailVerify`, `smsVerify`, `decimalPoint`, `emailNotify`, `smsNotify`, `emailMessage`, `emailSender`, `smsApi`, `walletaddress`, `btcrate`, `startdate`, `created_at`, `updated_at`) VALUES
(1,	'Leigonphy',	'All in one place',	'336699',	'LNY',	'LNY',	'1',	'0',	'1',	5,	'1',	'0',	'<span style=\"color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span><span style=\"color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span><span style=\"color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span>',	'info@leigonphy.co',	'<span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span>',	'moAdwdttFYtcFC56u6gtUA5Nx7SrM6DZxM',	'0.034375',	'10/13/2017',	'2017-11-09 04:33:35',	'2018-04-21 22:56:58');

DROP TABLE IF EXISTS `icoagenda`;
CREATE TABLE `icoagenda` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `round_name` varchar(500) CHARACTER SET latin1 NOT NULL,
  `start_date` varchar(500) CHARACTER SET latin1 NOT NULL,
  `last_date` varchar(500) CHARACTER SET latin1 DEFAULT NULL,
  `total_exa` varchar(500) CHARACTER SET latin1 NOT NULL,
  `avl_total_exa` varchar(500) CHARACTER SET latin1 NOT NULL,
  `price` double NOT NULL,
  `limit` varchar(500) CHARACTER SET latin1 NOT NULL,
  `status` varchar(500) CHARACTER SET latin1 NOT NULL,
  `statuscomplted` int(11) NOT NULL,
  `start_time` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_time` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_timestamp` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_timestamp` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `icoagenda` (`id`, `user_id`, `round_name`, `start_date`, `last_date`, `total_exa`, `avl_total_exa`, `price`, `limit`, `status`, `statuscomplted`, `start_time`, `last_time`, `start_timestamp`, `last_timestamp`, `created_at`, `updated_at`) VALUES
(370,	1,	'Pre-sale',	'07/20/2018',	'07/23/2018',	'600000',	'600000',	0.35,	'1500',	'1',	3,	'04:00',	NULL,	'1532059200',	'0',	'2018-04-29 01:17:27',	'2018-05-02 00:52:53'),
(374,	1,	'Stage 1',	'07/24/2018',	'07/27/2018',	'700000',	'700000',	0.6,	'1500',	'1',	3,	'04:00',	NULL,	'1532404800',	'0',	'2018-04-29 03:48:49',	'2018-05-02 00:52:53'),
(375,	1,	'Stage 2',	'07/28/2018',	'07/31/2018',	'1000000',	'1000000',	0.7,	'1500',	'1',	3,	'04:00',	NULL,	'1532750400',	'0',	'2018-04-29 03:50:28',	'2018-05-02 00:52:53'),
(376,	1,	'Stage 3',	'08/01/2018',	'08/04/2018',	'1200000',	'1200000',	0.8,	'1500',	'1',	3,	'04:00',	NULL,	'1533096000',	'0',	'2018-04-29 03:51:36',	'2018-05-02 00:52:53'),
(377,	1,	'Stage 4',	'08/05/2018',	'08/08/2018',	'1200000',	'1200000',	0.9,	'1500',	'1',	3,	'04:00',	NULL,	'1533441600',	'0',	'2018-04-29 03:53:20',	'2018-05-02 00:52:53'),
(380,	1,	'Stage 5',	'08/09/2018',	'08/13/2018',	'1300000',	'1300000',	1,	'1500',	'1',	3,	'04:00',	NULL,	'1533787200',	'0',	'2018-04-29 03:59:13',	'2018-05-02 00:52:53');

DROP TABLE IF EXISTS `logos`;
CREATE TABLE `logos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'logo.png',
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'icon.png',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `logos` (`id`, `logo`, `icon`, `created_at`, `updated_at`) VALUES
(1,	'logo.png',	'icon.png',	'2017-11-09 04:38:26',	'2017-11-09 04:38:26');

DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Menu',
  `content` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_order_unique` (`order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `menus` (`id`, `name`, `content`, `order`, `created_at`, `updated_at`) VALUES
(1,	'About us',	'<span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span><div><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</span><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><br></span></div><div><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><br></span></div><div><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur</span><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><br></span></div>',	1,	'2017-11-09 04:37:23',	'2017-11-09 04:37:23'),
(2,	'Vision',	'<span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur</span>\r\n                            <div><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><br></span></div><div><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</span><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><br></span></div>',	2,	'2017-11-09 04:38:17',	'2017-11-09 04:38:17');

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1,	'2014_10_12_100000_create_password_resets_table',	1),
(2,	'2017_10_03_041238_create_gsettings_table',	1),
(3,	'2017_10_03_094945_create_charges_table',	1),
(4,	'2017_10_03_103634_create_gateways_table',	1),
(5,	'2017_10_05_064050_create_policies_table',	1),
(6,	'2017_10_05_100721_create_menus_table',	1),
(7,	'2017_10_07_092822_create_logos_table',	1),
(8,	'2017_10_07_102327_create_sliders_table',	1),
(9,	'2017_10_08_044553_create_footers_table',	1),
(10,	'2017_10_08_052556_create_socials_table',	1),
(11,	'2017_10_08_064217_create_contacs_table',	1),
(12,	'2017_10_08_072143_create_statistics_table',	1),
(13,	'2017_10_10_052921_create_abouts_table',	1),
(14,	'2017_10_10_060529_create_services_table',	1),
(15,	'2017_10_10_064511_create_sericons_table',	1),
(16,	'2017_10_10_084215_create_paymethods_table',	1),
(17,	'2017_10_10_094357_create_payintros_table',	1),
(18,	'2017_10_10_100857_create_testimonials_table',	1),
(19,	'2017_10_11_110236_create_users_table',	1),
(20,	'2017_10_14_114326_create_wdmethods_table',	1),
(21,	'2017_10_14_121813_create_withdraws_table',	1),
(22,	'2017_10_16_060513_create_uwdlogs_table',	1),
(23,	'2017_10_18_084156_create_deposits_table',	1),
(24,	'2017_10_23_112715_create_packages_table',	1),
(25,	'2017_10_24_100915_create_references_table',	1),
(26,	'2017_10_31_051256_create_admins_table',	1),
(27,	'2017_10_31_051257_create_admin_password_resets_table',	1),
(28,	'2017_11_08_061551_create_upgrades_table',	1),
(29,	'2017_11_08_073303_create_avatars_table',	1),
(30,	'2017_11_11_044005_create_bonds_table',	2),
(31,	'2017_11_11_054957_create_prizes_table',	3),
(32,	'2017_11_11_091306_create_soldbs_table',	4),
(33,	'2017_11_11_135944_create_winners_table',	5),
(34,	'2017_11_13_090045_create_games_table',	6),
(35,	'2017_11_13_121323_create_ginvests_table',	7),
(36,	'2017_11_18_063251_create_docvers_table',	8),
(37,	'2017_11_18_111501_create_uaccounts_table',	9),
(38,	'2017_11_19_125342_add_google2fa_column_to_users',	10),
(39,	'2017_11_20_143601_create_prices_table',	11),
(40,	'2017_11_21_064508_create_tranlimits_table',	12),
(41,	'2017_11_29_154623_create_subscribes_table',	13),
(42,	'2017_11_30_124359_create_timelines_table',	14);

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('pial@sk.co',	'$2y$10$TM0ak8nV7556SWXv3RhxhO4GJzOmiXtMm5Znzdtyd8cApZCaFQRj.',	'2017-11-29 08:46:23'),
('laneaw134@gmail.com',	'$2y$10$3.oKVpMfGB0uzow7Ygvr1uOtF/vUP0dMNSYS9dT9uSUKX9.E5cdG6',	'2018-03-11 00:20:11'),
('ravirathour2010@gmail.com',	'$2y$10$wUlvoLLxJ5xiFgz4fV/c/e3Vn8T/ubjqQqYbSOaS2NNvBp4BJz/6e',	'2018-03-31 05:20:15'),
('wknexux@hotmail.com',	'$2y$10$XBXwFq3nnaK3YMCCmwn7h.fH41bb/ehMXj.O1XMC4tE6XuPizDXj6',	'2018-04-02 15:16:35'),
('ihanimal@gmail.com',	'$2y$10$Mh6/ydg3aELslRtmDVKCKO5Q9vwBMKkC0AvA8NaoaP8TQPaEfD.mG',	'2018-05-01 18:44:38'),
('wkguille@gmail.com',	'$2y$10$qpViMPzVLG1ai4T7HWhtYOoU5753nKbqn65rwDc/MxnlKRuIxlw4a',	'2018-05-01 18:46:01');

DROP TABLE IF EXISTS `payintros`;
CREATE TABLE `payintros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `heading` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Payment Heading Text',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `payintros` (`id`, `heading`, `details`, `created_at`, `updated_at`) VALUES
(1,	'Payment Method',	'<span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium&nbsp;</span><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium&nbsp;</span><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium&nbsp;</span><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium&nbsp;</span>',	'2017-11-09 04:47:16',	'2017-11-09 04:47:30');

DROP TABLE IF EXISTS `paymethods`;
CREATE TABLE `paymethods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'icon.png',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `paymethods` (`id`, `payment`, `created_at`, `updated_at`) VALUES
(1,	'5a04330ed8fd8.png',	'2017-11-09 04:50:54',	'2017-11-09 04:50:54'),
(2,	'5a043315dfc92.png',	'2017-11-09 04:51:01',	'2017-11-09 04:51:01'),
(3,	'5a04332dd3fbb.png',	'2017-11-09 04:51:25',	'2017-11-09 04:51:25'),
(4,	'5a04333a5af92.jpg',	'2017-11-09 04:51:38',	'2017-11-09 04:51:38');

DROP TABLE IF EXISTS `policies`;
CREATE TABLE `policies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `privacy` text COLLATE utf8mb4_unicode_ci,
  `terms` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `policies` (`id`, `privacy`, `terms`, `created_at`, `updated_at`) VALUES
(1,	'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit&nbsp;<span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span><div><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><br></span></div>',	'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit&nbsp;<span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span>',	'2017-11-09 05:23:36',	'2017-11-09 05:23:58');

DROP TABLE IF EXISTS `prices`;
CREATE TABLE `prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `prices` (`id`, `price`, `created_at`, `updated_at`) VALUES
(4,	'1',	'2018-02-27 18:04:28',	'2018-03-25 22:46:44'),
(5,	'0.18',	'2018-04-19 01:00:35',	'2018-04-26 01:00:35');

DROP TABLE IF EXISTS `refer`;
CREATE TABLE `refer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r_link` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `point` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level1` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level2` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `refer` (`id`, `user_id`, `r_link`, `parent`, `point`, `level1`, `level2`, `status`, `created_at`, `updated_at`) VALUES
(11,	'42',	'FHAQGF',	NULL,	'',	NULL,	NULL,	NULL,	'2018-03-29 07:30:14',	'2018-03-30 09:26:37'),
(12,	'43',	'JGARHG',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-03-29 10:10:33',	'2018-03-30 09:19:50'),
(13,	'44',	'EFASEI',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-03-29 10:12:21',	'2018-03-29 10:12:21'),
(14,	'45',	'FDATMG',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-03-29 12:38:24',	'2018-03-29 12:38:24'),
(15,	'46',	'IJAUGB',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-03-29 13:52:23',	'2018-03-29 13:52:23'),
(16,	'47',	'EKAVIE',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-03-29 19:46:28',	'2018-03-29 19:46:28'),
(17,	'48',	'GFAWHG',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-03-29 21:13:45',	'2018-03-29 21:13:45'),
(18,	'49',	'KCAXDH',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-03-29 21:16:20',	'2018-03-29 21:16:20'),
(19,	'50',	'FHAYEF',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-03-29 21:18:43',	'2018-03-29 21:18:43'),
(20,	'51',	'EJAZIE',	NULL,	'',	NULL,	NULL,	NULL,	'2018-03-29 23:26:42',	'2018-04-03 15:12:35'),
(21,	'52',	'FDBAGE',	NULL,	'',	NULL,	NULL,	NULL,	'2018-03-30 04:10:48',	'2018-04-02 11:43:53'),
(22,	'53',	'EHBBJD',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-03-30 04:13:05',	'2018-03-30 04:13:05'),
(23,	'54',	'JBBCJH',	NULL,	'',	NULL,	NULL,	NULL,	'2018-03-30 04:15:49',	'2018-04-03 15:12:27'),
(24,	'55',	'LHBDOD',	'FDBAGE',	NULL,	NULL,	NULL,	NULL,	'2018-04-02 11:39:20',	'2018-04-02 11:39:20'),
(25,	'56',	'HCBEFI',	'JBBCJH',	NULL,	NULL,	NULL,	NULL,	'2018-04-02 13:17:24',	'2018-04-02 13:17:24'),
(26,	'57',	'FIBFFM',	'EJAZIE',	'',	NULL,	NULL,	NULL,	'2018-04-02 13:24:29',	'2018-04-03 15:12:41'),
(27,	'58',	'DHBGMG',	'FIBFFM',	NULL,	NULL,	NULL,	NULL,	'2018-04-02 13:29:53',	'2018-04-02 13:29:53'),
(28,	'59',	'EEBHFB',	'EJAZIE',	NULL,	NULL,	NULL,	NULL,	'2018-04-02 13:32:19',	'2018-04-02 13:32:19'),
(29,	'60',	'LCBIIC',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-04-03 05:29:05',	'2018-04-03 05:29:05'),
(30,	'61',	'FKBJFG',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-04-03 11:00:20',	'2018-04-03 11:00:20'),
(31,	'62',	'IGBKCG',	'JBBCJH',	NULL,	NULL,	NULL,	NULL,	'2018-04-03 12:14:18',	'2018-04-03 12:14:18'),
(32,	'63',	'EEBLGC',	'HCBEFI',	NULL,	NULL,	NULL,	NULL,	'2018-04-03 12:18:38',	'2018-04-03 12:18:38'),
(33,	'64',	'CFBMBD',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-04-03 12:43:53',	'2018-04-03 12:43:53'),
(34,	'65',	'KJBNHG',	NULL,	'',	NULL,	NULL,	NULL,	'2018-04-03 13:38:45',	'2018-04-15 21:17:00'),
(35,	'66',	'HCBOGH',	'KJBNHG',	NULL,	NULL,	NULL,	NULL,	'2018-04-03 13:41:45',	'2018-04-03 13:41:45'),
(36,	'67',	'HFBPBC',	NULL,	'',	NULL,	NULL,	NULL,	'2018-04-03 14:50:21',	'2018-04-18 00:22:59'),
(37,	'68',	'KEBQEJ',	NULL,	'1.5',	NULL,	NULL,	NULL,	'2018-04-03 14:51:58',	'2018-04-22 03:06:48'),
(38,	'69',	'DCBRHG',	'KEBQEJ',	NULL,	NULL,	NULL,	NULL,	'2018-04-03 14:54:16',	'2018-04-03 14:54:16'),
(39,	'70',	'GCBSHG',	'KEBQEJ',	NULL,	NULL,	NULL,	NULL,	'2018-04-03 15:03:41',	'2018-04-03 15:03:41'),
(40,	'71',	'DFBTEI',	'HFBPBC',	NULL,	NULL,	NULL,	NULL,	'2018-04-03 15:05:50',	'2018-04-03 15:05:50'),
(41,	'72',	'FGBUEI',	'GCBSHG',	NULL,	NULL,	NULL,	NULL,	'2018-04-04 16:29:55',	'2018-04-04 16:29:55'),
(42,	'73',	'LCBVFD',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-04-08 13:53:57',	'2018-04-08 13:53:57'),
(43,	'74',	'EMBWEE',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-04-15 20:27:24',	'2018-04-15 20:27:24'),
(44,	'75',	'EDBXCK',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-04-15 22:10:46',	'2018-04-15 22:10:46'),
(45,	'76',	'JHBYDH',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-04-16 12:01:34',	'2018-04-16 12:01:34'),
(46,	'77',	'CGBZGE',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-04-18 00:07:56',	'2018-04-18 00:07:56'),
(47,	'78',	'MECAEF',	'HFBPBC',	NULL,	NULL,	NULL,	NULL,	'2018-04-18 00:12:00',	'2018-04-18 00:12:00'),
(48,	'79',	'EICBOE',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-04-18 21:10:08',	'2018-04-18 21:10:08'),
(49,	'80',	'EECCHF',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-04-18 21:32:08',	'2018-04-18 21:32:08'),
(50,	'81',	'IBCDMK',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-04-20 15:10:38',	'2018-04-20 15:10:38'),
(51,	'82',	'HKCECI',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-04-21 23:14:36',	'2018-04-21 23:14:36'),
(52,	'83',	'HECFLF',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-04-21 23:16:31',	'2018-04-21 23:16:31'),
(53,	'84',	'IHCGDC',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-05-01 03:22:06',	'2018-05-01 03:22:06'),
(54,	'85',	'JFCHGG',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-05-01 03:27:55',	'2018-05-01 03:27:55'),
(55,	'86',	'ICCILH',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-05-01 04:15:20',	'2018-05-01 04:15:20'),
(56,	'87',	'LDCJED',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-05-01 18:41:25',	'2018-05-01 18:41:25');

DROP TABLE IF EXISTS `references`;
CREATE TABLE `references` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `refer` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `references` (`id`, `user`, `refer`, `commis`, `flag`, `created_at`, `updated_at`) VALUES
(1,	'salman',	'1',	'00',	0,	'2017-11-11 04:46:17',	'2017-11-11 04:46:17');

DROP TABLE IF EXISTS `referral_price`;
CREATE TABLE `referral_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `levelone` int(11) NOT NULL,
  `leveltwo` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `referral_price` (`id`, `levelone`, `leveltwo`, `created_at`, `updated_at`) VALUES
(1,	5,	10,	'2018-04-03 22:22:29',	'2018-04-03 22:22:29');

DROP TABLE IF EXISTS `sandbox`;
CREATE TABLE `sandbox` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sandbox` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `sandbox` (`id`, `sandbox`, `created_at`, `updated_at`) VALUES
(1,	0,	NULL,	'2018-03-07 18:31:10');

DROP TABLE IF EXISTS `sericons`;
CREATE TABLE `sericons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'cogs',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Service Name',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `sericons` (`id`, `icon`, `name`, `created_at`, `updated_at`) VALUES
(1,	'cogs',	'Engineering',	'2017-11-09 04:45:51',	'2017-11-09 04:45:51'),
(2,	'television',	'Design',	'2017-11-09 04:46:06',	'2017-11-09 04:46:06'),
(3,	'users',	'HR Management',	'2017-11-09 04:46:26',	'2017-11-09 04:46:26'),
(4,	'archive',	'Archive',	'2017-11-09 04:47:11',	'2017-11-09 04:47:11');

DROP TABLE IF EXISTS `services`;
CREATE TABLE `services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `heading` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `services` (`id`, `heading`, `created_at`, `updated_at`) VALUES
(1,	'What is',	'2017-11-09 04:45:24',	'2017-11-30 06:38:33');

DROP TABLE IF EXISTS `sliders`;
CREATE TABLE `sliders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bold` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `small` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `sliders` (`id`, `image`, `bold`, `small`, `created_at`, `updated_at`) VALUES
(1,	'5a1fa65a08185.jpg',	'A Revolutionary Digital Money System<div><span style=\"color: rgb(52, 152, 219); font-family: Montserrat, sans-serif; font-size: 40px; font-weight: 800; text-align: center;\">TOTOCOIN&nbsp;</span><span style=\"font-family: Montserrat, sans-serif; font-size: 40px; font-weight: 800; text-align: center; color: rgb(255, 255, 255);\">is Digital Cash</span></div>',	'The Cryptocurrency For Payments',	'2017-11-09 04:39:43',	'2017-11-30 00:36:54');

DROP TABLE IF EXISTS `socials`;
CREATE TABLE `socials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `facode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'facebook',
  `faurl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'https://www.facebook.com',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `socials` (`id`, `facode`, `faurl`, `created_at`, `updated_at`) VALUES
(1,	'facebook',	'https://www.facebook.com/',	'2017-11-09 04:33:35',	'2017-11-09 04:33:35'),
(2,	'twitter',	'Twitter',	'2017-11-09 04:56:11',	'2017-11-09 04:56:11'),
(3,	'linkedin',	'Linkedin',	'2017-11-09 04:56:26',	'2017-11-09 04:56:26'),
(4,	'google',	'Google',	'2017-11-09 04:56:41',	'2017-11-09 04:56:41');

DROP TABLE IF EXISTS `statistics`;
CREATE TABLE `statistics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'users',
  `bold` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Bold Text',
  `small` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Small Text',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `statistics` (`id`, `icon`, `bold`, `small`, `created_at`, `updated_at`) VALUES
(1,	'user',	'Bold Text',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',	'2017-10-08 12:45:39',	'2017-11-09 05:18:20'),
(2,	'globe',	'Bold',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',	NULL,	'2017-11-09 05:18:27'),
(3,	'thumbs-up',	'Bold',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',	NULL,	'2017-11-09 05:18:34'),
(4,	'comment',	'Bold',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',	NULL,	'2017-11-09 05:18:40');

DROP TABLE IF EXISTS `subscribes`;
CREATE TABLE `subscribes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `subscribes` (`id`, `email`, `created_at`, `updated_at`) VALUES
(4,	'ggaha@gg.cd',	'2017-11-29 10:06:23',	'2017-11-29 10:06:23'),
(5,	'pialneel@gmail.com',	'2017-11-29 10:08:00',	'2017-11-29 10:08:00'),
(6,	'fsdfsdf@eafg.ds',	'2017-11-29 10:10:47',	'2017-11-29 10:10:47'),
(7,	'hfghfg@hsdf.dsf',	'2017-11-29 10:12:51',	'2017-11-29 10:12:51');

DROP TABLE IF EXISTS `testimonials`;
CREATE TABLE `testimonials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'photo.png',
  `company` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Client Company',
  `comment` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `testimonials` (`id`, `photo`, `company`, `comment`, `created_at`, `updated_at`) VALUES
(4,	'5a1fce935ded1.png',	'Crypto Currency',	'TOTOCOIN is a Crypto Currency. But Sometimes it\'s More than that as Transactions are Much Easier.',	'2017-11-30 03:25:39',	'2017-11-30 03:27:32'),
(5,	'5a1fd4b60fa59.png',	'Decentralized',	'TOTOCOIN is Completely Decentralized and Price Depends on Demand and Supply.',	'2017-11-30 03:51:50',	'2017-11-30 03:51:50');

DROP TABLE IF EXISTS `timelines`;
CREATE TABLE `timelines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `timelines` (`id`, `title`, `desc`, `date`, `created_at`, `updated_at`) VALUES
(5,	'Testing',	'<strong style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem Ipsum</strong><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span>',	'02/05/2018',	'2018-02-23 20:47:02',	'2018-02-23 20:48:19'),
(6,	'testing2',	'<strong style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem Ipsum</strong><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span>',	'02/21/2018',	'2018-02-23 20:48:48',	'2018-02-23 20:48:48');

DROP TABLE IF EXISTS `tranlimits`;
CREATE TABLE `tranlimits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `tranlimits` (`id`, `coin`, `created_at`, `updated_at`) VALUES
(1,	'8',	NULL,	'2017-11-21 01:05:40');

DROP TABLE IF EXISTS `uaccounts`;
CREATE TABLE `uaccounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `accnum` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `accnum` (`accnum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `uaccounts` (`id`, `accnum`, `user_id`, `created_at`, `updated_at`) VALUES
(5,	'cV4nLTzvUY7rZyQrgpg09rsNTvX2cPou',	3,	'2017-11-28 04:04:33',	'2017-11-28 04:04:33'),
(6,	'TqE2a5xqt4qRTWo7jnIxqjWcgY3xt1Fi',	1,	'2017-11-28 08:13:37',	'2017-11-28 08:13:37'),
(7,	'fDVNkSei3D1L6k8vtyDWnDfBrUBNaHzY',	1,	'2017-11-29 00:42:33',	'2017-11-29 00:42:33'),
(8,	'DJQXiGn4W1XqGBtYOhJYxaIqVJebNohX',	1,	'2017-11-29 04:40:47',	'2017-11-29 04:40:47'),
(9,	'hI25Nqu5eEgpEj5TFdwm6pemwEBKJaur',	1,	'2017-11-29 05:08:33',	'2017-11-29 05:08:33'),
(10,	'7QooLAD4fQkyxfTcwWzS8Ql1G0VadcVQ',	14,	'2018-02-22 19:08:51',	'2018-02-22 19:08:51'),
(11,	'zhjb6JhYpaKKJ2KUDZP6oDT05UFjLNwd',	14,	'2018-02-24 14:40:37',	'2018-02-24 14:40:37'),
(12,	'ny1VvLMrlxdKt77DkgCscxmPcIparbCV',	14,	'2018-02-24 14:47:44',	'2018-02-24 14:47:44'),
(13,	'hCEoR1Z5odv3M6pROKphHqjLePpNt1gM',	14,	'2018-02-24 14:47:52',	'2018-02-24 14:47:52'),
(14,	'WMWSd2pTAKdWuiXhwdp135cpaXrpmuW7',	14,	'2018-02-24 14:52:49',	'2018-02-24 14:52:49'),
(15,	'FaKN51wnJ0WANkDCZhHw5lhoqq1X1Lml',	14,	'2018-02-24 14:53:10',	'2018-02-24 14:53:10'),
(16,	'r6dvCIYKkJTFPlrg6ufsnYJ0aHqNN074',	14,	'2018-02-24 15:15:54',	'2018-02-24 15:15:54'),
(17,	'pQDL5LAIFDz9oIzkEnGnG0ZW4eRQKeHB',	14,	'2018-02-24 15:27:29',	'2018-02-24 15:27:29'),
(18,	'N1MOzZ1c3irhWBS5vZCZxARUfPDKgFmm',	14,	'2018-02-24 15:28:31',	'2018-02-24 15:28:31'),
(19,	'0yNxwb3o051JaaqulWTGzqmXExKVOrTR',	14,	'2018-02-24 15:57:38',	'2018-02-24 15:57:38'),
(20,	'I71NOnTKD5JQDawfk8eSmQWV973wysq1',	14,	'2018-02-24 15:57:42',	'2018-02-24 15:57:42'),
(21,	'uWbFBdCjeZMd5SYR42Pj1hItQJBpsRus',	14,	'2018-02-24 15:57:43',	'2018-02-24 15:57:43'),
(22,	'UuvrDVcTZwgOSGiOkk8kte22YhMzSo3H',	14,	'2018-02-24 15:57:47',	'2018-02-24 15:57:47'),
(23,	'8hGDgjXcIymMZiKCwseT4ALRbzGeXtAN',	14,	'2018-02-24 16:03:07',	'2018-02-24 16:03:07'),
(24,	'25NPYry3vp0lTrDBrk53ywfJYA12h5zP',	14,	'2018-02-24 16:04:15',	'2018-02-24 16:04:15'),
(25,	'jt7HqMLhMGoHL4X2WK46FPaWJfw9fUiq',	14,	'2018-02-24 16:07:13',	'2018-02-24 16:07:13'),
(26,	'n7V6dMJEP9argWtVmv2DlQXHsV0STBtx',	14,	'2018-02-24 16:10:36',	'2018-02-24 16:10:36'),
(27,	'H8L6QnZEQtQvl7AeRwyt24BICQMEggT9',	14,	'2018-02-24 16:14:38',	'2018-02-24 16:14:38'),
(28,	'gJHIutAw277ucFbuiFbqwxiWNGNS6ZPY',	14,	'2018-02-24 16:14:45',	'2018-02-24 16:14:45'),
(29,	'14ZNWGWHDoxrxaRtfbZSFR5YA5VA791d',	14,	'2018-02-24 16:17:50',	'2018-02-24 16:17:50'),
(30,	'II1EijDi5cUCl9qHmXTnLYAxD9I0O4ea',	14,	'2018-02-24 16:17:57',	'2018-02-24 16:17:57'),
(31,	'7I8UoEoE3Hli9kMQSt1qqDP4pX0nMExw',	14,	'2018-02-24 16:18:14',	'2018-02-24 16:18:14'),
(32,	'Ag4Tvvmn92lPja7uJzOVT7h7dYtGBGJd',	14,	'2018-02-24 16:36:35',	'2018-02-24 16:36:35'),
(33,	'rNDvh2KtUNYTuWgfDtGAebTQP6eOrh0r',	14,	'2018-02-24 16:45:09',	'2018-02-24 16:45:09'),
(34,	'qZyXb1Y3ru8ILwKcBwqagjyNxBrWOIPw',	14,	'2018-02-24 16:45:09',	'2018-02-24 16:45:09'),
(35,	'Mtl7lc1x4RcerJsi6nFWWs2Kvbms59mt',	14,	'2018-02-24 17:05:21',	'2018-02-24 17:05:21'),
(36,	'cJV76lTzCzN5dW5brU4jasFHtNClriqy',	14,	'2018-02-26 19:26:46',	'2018-02-26 19:26:46'),
(37,	'dLzQQikUrxiXEvBEyGY5RmXXXeAfPo90',	14,	'2018-02-26 19:37:12',	'2018-02-26 19:37:12'),
(38,	'zyruGHTLaDDIdxcwRGUySxdLYzTfbRbr',	14,	'2018-02-27 12:08:01',	'2018-02-27 12:08:01'),
(39,	'yj7MF2fHR3EA1i4L1E3CYEQVbNZpjGdx',	17,	'2018-02-27 13:17:43',	'2018-02-27 13:17:43'),
(40,	'cEaIP3pQEcONirX4uIHS7pp82kbL7y9Z',	17,	'2018-02-27 13:18:02',	'2018-02-27 13:18:02'),
(41,	'lVF17jmrN9Cw6Q0lp4xcON2mhbMtWc1Z',	17,	'2018-02-27 13:18:15',	'2018-02-27 13:18:15'),
(42,	'uNUF1E0GatIwGkovvtJ3gxEeKCglSo5C',	17,	'2018-02-27 13:32:43',	'2018-02-27 13:32:43'),
(43,	'RbD9b7ymAzFUUD3R8Ut3wt1GjTFt3kL3',	17,	'2018-02-27 13:33:00',	'2018-02-27 13:33:00'),
(44,	'PTgmcXkdaGeJsSepF8nvHtYlFwxUhjFV',	17,	'2018-02-27 13:33:06',	'2018-02-27 13:33:06'),
(45,	'LQLk6LgKtL5LOGuEVKO05A0lph6ZgyIv',	17,	'2018-02-27 20:44:41',	'2018-02-27 20:44:41'),
(46,	's6KbY8Pd6A2boNq6V9LaHeGMyC2QATpb',	17,	'2018-03-01 00:44:57',	'2018-03-01 00:44:57'),
(47,	'Lm5a2IlZLHqFzIj0mgULMQUxFwabADUx',	22,	'2018-03-01 12:55:43',	'2018-03-01 12:55:43'),
(48,	'edCZSoro2Uw6xNXL81XnHUgY02ttQk6v',	22,	'2018-03-01 12:56:32',	'2018-03-01 12:56:32'),
(49,	'wL4ImENEjEZjFwDqsfeQ13VYWq3ELl3C',	32,	'2018-03-10 20:51:03',	'2018-03-10 20:51:03'),
(50,	'yGNsSkaEykZUv2v2bFhDOco4xzlIxhSt',	32,	'2018-03-10 20:51:28',	'2018-03-10 20:51:28'),
(51,	'w6sQYKtRjowqWICawtq1RXhYqoU5OeL5',	36,	'2018-03-10 23:48:00',	'2018-03-10 23:48:00'),
(52,	'lUjn4XiZLlGFgYq9v7O7BWRs5JZH96cS',	25,	'2018-03-17 17:35:46',	'2018-03-17 17:35:46');

DROP TABLE IF EXISTS `upgrades`;
CREATE TABLE `upgrades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `refer_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `upgrades` (`id`, `user_id`, `refer_id`, `created_at`, `updated_at`) VALUES
(1,	2,	0,	'2017-11-13 10:59:05',	'2017-11-13 10:59:05');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `refid` int(11) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `balance` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bitcoin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) DEFAULT NULL,
  `docv` int(11) NOT NULL DEFAULT '0',
  `gtfa` int(9) NOT NULL DEFAULT '0',
  `tfav` int(9) NOT NULL DEFAULT '0',
  `paystatus` int(11) DEFAULT NULL,
  `trxpin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emailv` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smsv` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vsent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vercode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forgotcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secretcode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `refid`, `package_id`, `username`, `password`, `balance`, `bitcoin`, `firstname`, `lastname`, `date`, `address`, `city`, `postcode`, `country`, `mobile`, `email`, `status`, `docv`, `gtfa`, `tfav`, `paystatus`, `trxpin`, `emailv`, `smsv`, `vsent`, `vercode`, `forgotcode`, `secretcode`, `remember_token`, `created_at`, `updated_at`) VALUES
(24,	NULL,	NULL,	'a',	'$2y$10$NBnznh8z/VYxIuEvkffG0.bL/PdcZhUMrD/lg3PHU/ACxs7EAK2Cu',	'00',	'00',	'a',	'b',	NULL,	NULL,	'ram',	NULL,	'India',	'1234567980',	'vijgora@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	'0',	'0MOA2AhEGg',	NULL,	NULL,	'0U75V4oq45aH5b1HFKCNeqmNzrj3aTCIlucnAbVHG4boz2baIpKevq1WhZaV',	'2018-03-08 20:58:26',	'2018-03-08 21:00:04'),
(25,	NULL,	NULL,	'Asdasd',	'$2y$10$UOeY/oaPBd08jxmHO.RddOlQNKLhSo4Mk8442d7GE673W1iL2CDla',	'00',	'00',	'Asd',	'Asd',	NULL,	NULL,	'Ass',	NULL,	'Afghanistan',	'3333333333',	'asdasd12312as@hotmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	'0',	'4aoY71DZSy',	NULL,	NULL,	'eQLxsFXEZ9oBwsIdXwAkSxmPZdtIqSqyXcEnHK7dCqyt8cCt7IAPBN1QxcZi',	'2018-03-10 13:04:44',	'2018-03-10 13:06:15'),
(26,	NULL,	NULL,	'criptom',	'$2y$10$RjraXUca4QpoxB5iJ54kGOthF1PQGtondWj7IY.EUvEoydODD6KNa',	'00',	'00',	'luis',	'miguel',	NULL,	NULL,	'miami',	NULL,	'Argentina',	'8001234567',	'ctiptom1900@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	'0',	'k63PmUZZOv',	NULL,	NULL,	'r3GjqGTh6oXP2IOLecz9VQKBq6eUCVGV0pP6z82o38C0o0yJGNdlRjPBFB5z',	'2018-03-10 15:29:27',	'2018-03-13 20:44:04'),
(27,	NULL,	NULL,	'dknaskndansdk',	'$2y$10$r1UVcePfySWhpEYQevb0XOBPmUXrE7zIqjxcwuPMgUjh79Nt/VgV.',	'00',	'00',	'kankskand',	'asdasdsadsa',	NULL,	NULL,	'sadsadada',	NULL,	'Afghanistan',	'5156151561',	'ndkasnkdn@jksjbajdb.com',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-03-10 16:18:40',	'2018-03-10 16:18:40'),
(28,	NULL,	NULL,	'jhon',	'$2y$10$iMKDN4NeEHLff7kyTgQy5.Icux/9etQKTW8g.YMJ1qv9ig7O0p6WO',	'00',	'00',	'jhon',	'megger',	NULL,	'dfgdfgdfgdfgdfhd',	'london',	'111111',	'Afghanistan',	'88899999978',	'leigonphy@gmail.com',	0,	0,	0,	0,	NULL,	NULL,	'0',	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-03-10 16:30:47',	'2018-03-10 16:38:17'),
(29,	NULL,	NULL,	'jhonmegger',	'$2y$10$BI07zeoxg7zSCWkEzBK4SuTpEwM3pMQeq.IEeyNDtAdQvcEhAzF66',	'00',	'00',	'jhon',	'megger',	NULL,	NULL,	'london',	NULL,	'Afghanistan',	'88899999999',	'dsfsfsdfs@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	'0',	'y4aFssoPdR',	NULL,	NULL,	NULL,	'2018-03-10 16:31:43',	'2018-03-10 16:40:49'),
(30,	NULL,	NULL,	'cripto',	'$2y$10$uOboLXFyd7Q.mq15GRjzEu1oQLywaTHF1YkbO5BzjrYYqCJWVgVDy',	'00',	'00',	'luis',	'miguel',	NULL,	NULL,	'abdula',	NULL,	'Afghanistan',	'8001234567',	'criptom1900@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	'0',	'm1EKcpttp4',	NULL,	NULL,	NULL,	'2018-03-10 17:51:11',	'2018-03-10 17:55:07'),
(31,	NULL,	NULL,	'asd1',	'$2y$10$fnaEVn.A2Kusc06CD8g5heJOnXuUlJ7vYiE8cdDjNZE9ZXtVJKCS6',	'00',	'00',	'asd1',	'asd1',	NULL,	NULL,	'asd',	NULL,	'Afghanistan',	'1111111111',	'asda213124sd@homail.com',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	'1522155499',	'XgwumvSf',	NULL,	NULL,	NULL,	'2018-03-10 20:44:54',	'2018-03-27 12:58:19'),
(32,	NULL,	NULL,	'wkguille@gmail.com',	'$2y$10$Hhujw/gvYFPeCBrjdXlO.eQYYFEGIuJNhbGYxI/H8nlB5bkpSsJgS',	'00',	'00',	'qwe',	'qwe',	NULL,	'qweqwe se 223 23',	'asd',	'23232',	'Afghanistan',	'2222222222',	'sadsdsf@sdfsdfsd.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	'0',	'gu7fktN4Mq',	NULL,	NULL,	'isepHL3SuDcX514sURXZWOVhaKlhQkq1jkBtYgY5YOmAuhuRjL6KTUS8SjWH',	'2018-03-10 20:50:32',	'2018-03-27 14:51:38'),
(33,	NULL,	NULL,	'juanc',	'$2y$10$AhCOFFORTwy0VgV7iLfzde207o6eacaM0nzstqZsnPuiWY761haH.',	'00',	'00',	'juan',	'carlos',	NULL,	NULL,	'acapulco',	NULL,	'Afghanistan',	'3058975689',	'abril2010morales@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	'1520724762',	'5EMNRRJl',	NULL,	NULL,	NULL,	'2018-03-10 23:10:47',	'2018-03-10 23:32:42'),
(34,	NULL,	NULL,	'juanc1',	'$2y$10$wFZzDoF1DJC26uteLoZxMupWzbi9MlJUCZkty/6xmNkQLcp4w84/W',	'00',	'00',	'juan',	'carlos',	NULL,	NULL,	'zona norte',	NULL,	'Afghanistan',	'3058975896',	'diegoalvar2008@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	'1520723922',	'B2JwAvyZ',	NULL,	NULL,	NULL,	'2018-03-10 23:15:10',	'2018-03-10 23:18:42'),
(35,	NULL,	NULL,	'jjcc',	'$2y$10$RkEPJuFOVtV/.0mMaiOW8eC2zDPv.ccVQZeFnn73lte51bggzG8D6',	'00',	'00',	'juan',	'carlos',	NULL,	NULL,	'minesota',	NULL,	'Afghanistan',	'3057895623',	'laneaw134@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	'1520724659',	'iIii9gjd',	NULL,	NULL,	NULL,	'2018-03-10 23:30:12',	'2018-03-10 23:30:59'),
(36,	NULL,	NULL,	'andres',	'$2y$10$5EPD/CEpPpvGN8QFr/QkZOfHqTtLRxfLkR31R/CfXyfyUc.3niKce',	'00',	'00',	'andresss',	'palomares',	NULL,	'5656',	'miami',	'33445',	'Afghanistan',	'305456789',	'afcmarine@hotmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	'0',	'bs2oW4y5HS',	NULL,	NULL,	'YK5JiC0xkdYtCUGzfDanxpqP8p63iESLwbxloGR72orWUduNlLKS8v1c6ND2',	'2018-03-10 23:37:17',	'2018-03-29 18:20:41'),
(37,	NULL,	NULL,	'testing',	'$2y$10$x4NTmXg77mAUfY5FT69PtOHH9lTlRWZKZHuE89StptgCO.DOz.2WK',	'00',	'00',	'test',	'testing',	NULL,	NULL,	'jaipur',	NULL,	'India',	'123456789',	'rajman.test12@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	'kFEiSdFETqx63f2ZotY8ZZ9uSYsAbqIoLxzYJu4GqZVk6JGl57zzoSoJ4HwG',	'2018-03-13 06:03:56',	'2018-03-13 06:03:56'),
(38,	NULL,	NULL,	'qwert',	'$2y$10$XvCqXx4gAC7YL3hZs5D1Ouf5InlWUPoBJGO8kU9d7324FC8DnoYDC',	'00',	'00',	'qwert',	'qwert',	NULL,	NULL,	'asd',	NULL,	'Afghanistan',	'2223331111',	'asd12312asdas@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	'0',	'4Fellob41U',	NULL,	NULL,	'taOnBezR28BIX8FvWR6KlNeDU7uKd2VXixh4QgrmSsYrMpSXIhQJ9hZWTZ7k',	'2018-03-13 18:30:49',	'2018-03-13 18:33:00'),
(39,	NULL,	NULL,	'stornblade',	'$2y$10$e/8114DuWTEKwmzyMEhsh.J2GnzLTydmd4DcV1fmQJo1RSKEIe5LW',	'00',	'00',	'Jose',	'sarmiento',	NULL,	NULL,	'Caracas',	NULL,	'Venezuela',	'04242865454',	'josexpapax@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	'0',	'zr6NvBA9Kq',	NULL,	NULL,	NULL,	'2018-03-14 15:40:33',	'2018-03-14 15:54:59'),
(41,	NULL,	NULL,	'Abhishek',	'$2y$10$TxEPl/r/YSvxu5Mzuww2duj1APS0lMmasWLIQ8awC2y09EHXARIPm',	'00',	'00',	'Abhishek',	'saxean',	NULL,	'ddsdad',	'jaipur',	'25000',	'India',	'8442099375',	'abhishek.saxena@rajman.in',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	'0',	'UoJhenl83f',	NULL,	NULL,	NULL,	'2018-03-28 14:19:20',	'2018-03-29 05:20:19'),
(42,	NULL,	NULL,	'ravi',	'$2y$10$HKVApVyOaAKHM0Cqyb6cIOEswdhF.OASs9ERHBIjzbFT7YxHuA08i',	'00',	'00',	'Ravi',	'Rathore',	NULL,	NULL,	'jaipur',	NULL,	'Afghanistan',	'123456',	'ravirathour2010@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	'0',	'Ynuqx69sBW',	NULL,	NULL,	'LdaROBre3yTKdDpPGJ2El3wX3nmlL4WQEVqWMz7nLI97hKCimjefHnwXrFiv',	'2018-03-29 07:30:14',	'2018-03-29 07:32:25'),
(43,	NULL,	NULL,	'mukesh',	'$2y$10$jAbT4SXmyix.8ictbc/RzOkPmXwR7cSd2xxkzW7rm14X.zcNlZcci',	'00',	'00',	'mukesh',	'123456',	NULL,	NULL,	'dsfd',	NULL,	'Afghanistan',	'1',	'ukesh@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	'CC8RKGPVcLoqPRHme7CXvwGuJZJ2pYEdbR1KOozPwZOsJldxKyF2l08nNHYO',	'2018-03-29 10:10:33',	'2018-03-29 10:10:33'),
(44,	NULL,	NULL,	'yadav',	'$2y$10$rPGrhdaYnuuCAQ6Cqjx2JOkXX3N7Y4YnBaLatscBd0CMnaxNB9cTu',	'00',	'00',	'yadav',	'kmuar',	NULL,	NULL,	'yadav',	NULL,	'Afghanistan',	'123456',	'yadav@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-03-29 10:12:21',	'2018-03-29 10:12:21'),
(45,	NULL,	NULL,	'mainsh',	'$2y$10$Bk09ROZymgsV2dZIgx3bzuv4keq5.mAx6zM8Dw20zCqN/yw66LHAO',	'00',	'00',	'manish',	'soni',	NULL,	NULL,	'jaipur',	NULL,	'Afghanistan',	'1234545',	'manish@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	'9H54S6h06aLKxHeWbQePptpz906rmqnUBYJxWlAsfZXs29sStsN6Ma8fxDvh',	'2018-03-29 12:38:24',	'2018-03-29 12:38:24'),
(46,	NULL,	NULL,	'manison78',	'$2y$10$T5UqoBGddawPYH5O.RbMOe77M5pL9ic4IyzIo1.8LMg0HbJP5NYTK',	'00',	'00',	'Manish',	'Soni',	NULL,	NULL,	'JAIPUR CITY',	NULL,	'India',	'9785200735',	'manison78@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	'0',	'y2cbbx16nT',	NULL,	NULL,	'IeEvZ7PvTbhFPWIbsIuCkMEAkU0XzPqHA1oAQJMwvXP3WLFXogmQ0SujRdyR',	'2018-03-29 13:52:23',	'2018-03-29 13:53:19'),
(47,	NULL,	NULL,	'luis',	'$2y$10$tUwr80aGSu4n.XJAECGKCuM582of9UDCOVwUZxgbXRAjXVPIyV/9u',	'00',	'00',	'luis',	'alfonzo',	NULL,	NULL,	'home',	NULL,	'Afghanistan',	'3056587895',	'luisluisalfonzo@outlook.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	'1522353018',	'Yb1qCPr8',	NULL,	NULL,	'ELMn176LZr6XYXHDb6qfmfb5nmNGNmw7LsAHaRjsJxTIuvmmpcIP90wkTj5y',	'2018-03-29 19:46:28',	'2018-03-29 19:50:18'),
(48,	NULL,	NULL,	'sandy',	'$2y$10$wrC5QaIlPgGzMpufegm6nuysBvzGR5iqoCYntgITILIRXREnCGPN.',	'00',	'00',	'sandy',	'andy',	NULL,	NULL,	'coconut',	NULL,	'Afghanistan',	'3054896523',	'sandrabri61@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	'1522358028',	'6C5BBbDy',	NULL,	NULL,	NULL,	'2018-03-29 21:13:45',	'2018-03-29 21:13:48'),
(49,	NULL,	NULL,	'arthur',	'$2y$10$.1xrv59KrkT5T1e1v6Tqre68Zsj46Qq50quYVd5jPpS/GLL8lNe1O',	'00',	'00',	'arthur',	'nobre',	NULL,	NULL,	'miami',	NULL,	'Afghanistan',	'3058888825',	'aanobre637@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	'1522358185',	'a5ad69hJ',	NULL,	NULL,	NULL,	'2018-03-29 21:16:20',	'2018-03-29 21:16:25'),
(50,	NULL,	NULL,	'lublanco',	'$2y$10$Os7gAu2Lo6knzXQBzjr6nOmrTcCaedbrHErupQ/DYaF3TXOhQ.w0C',	'00',	'00',	'luis',	'blanco',	NULL,	NULL,	'guari',	NULL,	'Afghanistan',	'3057895462',	'luisab1313@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	'1522358327',	'f1Xhsn67',	NULL,	NULL,	NULL,	'2018-03-29 21:18:43',	'2018-03-29 21:18:47'),
(51,	NULL,	NULL,	'bimba',	'$2y$10$/H19kSE.Eqagmz.gUCbVHu5qLam4aZC6cz3wAAHZfL.mqkbQiWUwS',	'00',	'00',	'juan',	'bimba',	NULL,	'mansrover',	'jaipur',	'302020',	'Afghanistan',	'3057896541',	'juanh1518@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	'cXmfvfD3q77iROpVNxQIcomZCU7DfsU0nG4IYOkHQxptl8n7j2awOHacHGI5',	'2018-03-29 23:26:42',	'2018-04-02 10:47:08'),
(52,	NULL,	NULL,	'hayes',	'$2y$10$k3qTSxZKX//yxOoI9sujuOW.CZYez1wgY.fa1Vl2S7QmxzyxYNI92',	'00',	'00',	'allen',	'hayes',	NULL,	'sdfsf',	'wichita',	'sdfsdf',	'Afghanistan',	'789852654',	'mencar780@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	'RDI0BepCbdfYYnOzkoFFaRbHw6NfVFTnNwRTTpMSn52bTrGNPFvfgtFbRRDk',	'2018-03-30 04:10:48',	'2018-04-04 05:36:05'),
(53,	NULL,	NULL,	'agar',	'$2y$10$LoDugirM02HUiGWUaeXRPeidiTsB24qSttlNjpE1CA1D7/EmB80kG',	'00',	'00',	'henry',	'agar',	NULL,	'sdfdgfdgfdg',	'chino',	'fdgdgdgfdgd',	'Afghanistan',	'698231457',	'agarhenryagar@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	'GDQDHtY2STT54Rek1OaDoyrc5JWaLDIfkJVSIfbQ4WPWBbsN5KvodvUtquc8',	'2018-03-30 04:13:05',	'2018-04-02 12:22:14'),
(54,	NULL,	NULL,	'doe',	'$2y$10$.z6gekKb9CyFjfcl5RCOkeU1no4vgw47rjGnhb83efiRuIwXnVQXq',	'00',	'00',	'keneth',	'doe',	NULL,	NULL,	'brea',	NULL,	'Afghanistan',	'654789321',	'keneth54doe@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	'AXs2GIAFiFobuX2h55L76Tqs4rHRJWOic8rJ8odyM9WYL8dRVsudSEK7elfh',	'2018-03-30 04:15:49',	'2018-04-04 05:36:23'),
(55,	NULL,	NULL,	'testing123',	'$2y$10$7ZUgnM/cCxmgwYZV4bMri.8WNNjIEtF/6YY0fr2d9/TBtws/tW3dO',	'00',	'00',	'testingbro',	'testingbro',	NULL,	'dfsf',	'gdfg',	'sdfdf',	'Afghanistan',	'fdgfdg',	'testingbro@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	'6auQTO72B7WyKD6k1ILSD3AjDMJB8LO2rOFfQ0gQkXGnQl05FOy7brW2dUQT',	'2018-04-02 11:39:20',	'2018-04-02 13:17:30'),
(56,	NULL,	NULL,	'Asd123456',	'$2y$10$8RtakkV/pOq9gpbl5yd0sOeHh8LdTMZVQx1gjH2TbuEGUmV.i7G5C',	'00',	'00',	'Asddsa',	'Asd',	NULL,	'asdasd',	'Gg',	'23232',	'Afghanistan',	'3343444444',	'Thcfy@gffgh.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	'9OoUqUZC9P3Djkh2sM5cwDfGtRa7K1sViBaF09EOHgJhdtmbUe0C41l4Osdt',	'2018-04-02 13:17:24',	'2018-04-03 07:00:36'),
(57,	NULL,	NULL,	'dsa',	'$2y$10$s0iJl.yTbncd.jfCuaty.eqgkutfRm8HUxPXvUWl/YpAG.ibusfMK',	'00',	'00',	'dsa',	'dsa',	NULL,	'213123',	'asd',	'2131',	'Afghanistan',	'3232323232',	'dsa@das.com',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	NULL,	NULL,	NULL,	NULL,	'wcEtIYEXiUzE96DiBvdzt8d8cWMyGcgP046KlIul3NtOCZ5Aczsh2FmFMJmq',	'2018-04-02 13:24:29',	'2018-04-02 15:17:25'),
(58,	NULL,	NULL,	'dsadsa',	'$2y$10$nOZnMCceQP6t1nDK/l3LLOL4IJIWjJ/g671kOVCXX7ne/DgynpHD2',	'00',	'00',	'dsadsa',	'dsadsa',	NULL,	'123123',	'asdd',	'123123',	'Afghanistan',	'3333333333',	'dsadas@gdas.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	'nQuRj2NIUlUU0lmmPa7nL4dkYBoegooiRaDblFDjcAuAtIKmsT1qO98M5kFn',	'2018-04-02 13:29:53',	'2018-04-03 06:44:02'),
(59,	NULL,	NULL,	'dsa1',	'$2y$10$uy3xceFvJP4.vbdvM/VtLO0INnxidXh1m8jAx9/ILetVDYAU6yPoa',	'00',	'00',	'dsa1',	'asd',	NULL,	'jghjhg',	'asda',	'ghjghjh',	'Afghanistan',	'3333333333',	'asd@asd.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	'IWtIaiVkENp67PWlJboKOZsethr8ZArLRSgv1eMuaKLOXr6NFLU0amblYe7m',	'2018-04-02 13:32:19',	'2018-04-03 09:48:44'),
(60,	NULL,	NULL,	'karishma',	'$2y$10$VGHucH5t.jWFCN.U6/Bmle1H1r8Uq//2KDqk.k0uw.Mx7NOYbG38e',	'00',	'00',	'karishma',	'takl',	NULL,	NULL,	'jaipur',	NULL,	'India',	'9116719277',	'takkarishma07@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	'lFYHyFeVQGZd5RS9saSDSjtO3OUsr3QTULw6SKrh2V7j1m6oCSXBupeTCAuE',	'2018-04-03 05:29:05',	'2018-04-03 05:29:05'),
(61,	NULL,	NULL,	'karishma tak',	'$2y$10$87wZc8wf9ugEnX32Be4aYeQeC1AUoos18I3TMAPnq2Kpx5bWKIpHS',	'00',	'00',	'karishma',	'tak',	NULL,	NULL,	'jaipur',	NULL,	'Afghanistan',	'9116719277',	'takkarishma01@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	'1522753258',	'R6LuYk2J',	NULL,	NULL,	NULL,	'2018-04-03 11:00:20',	'2018-04-03 11:00:58'),
(62,	NULL,	NULL,	'Abcde',	'$2y$10$oFeJRX7fBPUt/dmGoJp4BeBw9vh2PNgGY8qnhzJPK3aFkpfek5Fjq',	'00',	'00',	'Hugh',	'Huug',	NULL,	NULL,	'Uii',	NULL,	'Afghanistan',	'6787656666',	'Tttt@tt.com',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-04-03 12:14:18',	'2018-04-03 12:14:18'),
(63,	NULL,	NULL,	'priya123',	'$2y$10$mX3FO12AO9B9jxtufRbd3.HOZTN8hY0rxSw3/W9ZQuLSpL7wRrJNm',	'00',	'00',	'priya',	'garg',	NULL,	'sdfdsf',	'jaipur',	'sdfsdf',	'Afghanistan',	'123456',	'priya.grag@rajman.in',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	'1522757924',	'Oc1uRBCi',	NULL,	NULL,	NULL,	'2018-04-03 12:18:38',	'2018-04-03 12:20:48'),
(64,	NULL,	NULL,	'karishma07',	'$2y$10$LoSWUBhd0odlVGIhWA3RXuAq8tNHYk2noZrw03JnZT074hqg4L1E.',	'00',	'00',	'karishma',	'tak',	NULL,	NULL,	'jaipur',	NULL,	'Afghanistan',	'9116719277',	'karishma.tak@rajman.in',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	'1522759769',	'GfU6I0mA',	NULL,	NULL,	NULL,	'2018-04-03 12:43:53',	'2018-04-03 12:49:29'),
(65,	NULL,	NULL,	'ravi456',	'$2y$10$VSujVlFi3x3UcxJaVJbJgOj302zqw/WWYQ8gi2fMeJ6NyGka2UUS2',	'00',	'00',	'ravi',	'rathore',	NULL,	'jaipur',	'jaipur',	'30200',	'Afghanistan',	'9783184955',	'ravijohn@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	't3LhR7Fn9LJzc9uWq2qfWKcGweTq1OSvXz8quyvr7w9juWHJjEDnLREdB36c',	'2018-04-03 13:38:45',	'2018-04-04 11:17:35'),
(66,	NULL,	NULL,	'john',	'$2y$10$SOJOP62fdBoSEWzDriiSP.nogj8etSmEYZBks9SoA5/Wuk6OqWzIa',	'00',	'00',	'mukesh',	'kumar',	NULL,	'jaipur',	'jaipur',	'32020',	'Afghanistan',	'9783184955',	'john@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	'lJTM6HRkyIdG3i9gB552gNB505KFsVMhd0K5anZKYgiBqM4hi4BuPlbEBgMJ',	'2018-04-03 13:41:45',	'2018-04-03 13:42:25'),
(67,	NULL,	NULL,	'wkwk',	'$2y$10$H1gRZuuPuqUoWEu4nlEc2OoD94mAuMoFwV9x/oqjVmSbJ3vOnfhYu',	'00',	'00',	'Jhon',	'Magger',	NULL,	'tampa',	'tampa',	'44542',	'Afghanistan',	'111',	'wkwk@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	'LWJ6W7QD4KLDQPII',	'NMpdARqozl7uolkG3JwCe5UIWQLtKCM30ijXWsFsJn1NbIsRDZzI7kkpuluj',	'2018-04-03 14:50:21',	'2018-04-29 17:14:12'),
(68,	NULL,	NULL,	'wkwk1',	'$2y$10$vkr8pBswX6rV3HDG5seuXe00DV2JWxe6sh/vXdIfdBXW01SKnfCqa',	'00',	'00',	'wkwk1',	'gg',	NULL,	'tampa',	'tampa',	'23321',	'Afghanistan',	'5452341199',	'wkwk@hotmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	'EXY5ZGOMCG4RI3SX',	'AJyIuzeDTuPMkFCSKF8STUZxx680M2XmXiuwf4YwifDzDUOERsa7Z6WoiIJ6',	'2018-04-03 14:51:58',	'2018-04-27 02:19:20'),
(69,	NULL,	NULL,	'wkwk2',	'$2y$10$r0Oe2VmOSfYjTASZtQvCLeqqyAvB35RulayClmxdlJg1Y3Da5K.oi',	'00',	'00',	'wkwk2',	'gg',	NULL,	'tampa',	'tampa',	'23223',	'Afghanistan',	'56745454',	'wkwk2@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	'd1LSTtBKa012mXsanLBbed6nnNxBHJh1cletdHzbkU1JzPLcOlv2763njbRj',	'2018-04-03 14:54:16',	'2018-04-27 01:50:28'),
(70,	NULL,	NULL,	'wkwk3',	'$2y$10$ahFDUYAfkoTQFFB37a48vu8DEfTdBtAosXNXRXVEFHs5.sVdZUuJm',	'00',	'00',	'wkwk3',	'gg',	NULL,	'tampa',	'tampa',	'32312',	'United States',	'2311231212',	'wkwk3@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	'FXKWIDLIX4XGDHPN',	'Sl5NH8Tq39BEFANVvazGPKd7DNApbnrgKXFaGFgDrbrXVRcZd6e2HmCM4f2v',	'2018-04-03 15:03:41',	'2018-04-29 03:24:08'),
(71,	NULL,	NULL,	'wkwk4',	'$2y$10$OHjgArvFPcLjI0ePVbLyMu3bGiERpkyweSupycIQfQ9.HTIbWsQTW',	'00',	'00',	'wkwk4',	'gg',	NULL,	'tampa',	'tampa',	'23112',	'Afghanistan',	'23412321',	'wkwk4@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	'W3bLkvwODbMhJ7SYk7gyyBKI2f6503cuUNPx2q8lkChZUeSptMoFZO1Vlt2q',	'2018-04-03 15:05:50',	'2018-04-17 00:40:53'),
(72,	NULL,	NULL,	'wkwk5',	'$2y$10$obdIC2PWNyR3L9/YyQf8z.gJL7dLT1xIQo8vAqW0U0ED5XKW0aWIC',	'00',	'00',	'asdasdasd',	's',	NULL,	'asd',	'qwe',	'23122',	'Afghanistan',	'3232321212',	'asd@dsaasdd.com',	1,	0,	1,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	'JUEJJBXMV7URHPHB',	'k0YC18dDraLSkBtqUVjRwvIks0NxwSAyihKG8M7rmyobRVw2h1nRJH8FTFa4',	'2018-04-04 16:29:55',	'2018-04-29 17:34:59'),
(73,	NULL,	NULL,	'qwertyuio',	'$2y$10$XRe.3TefJUZtm.IDDScWvu3Wyt/olx2mIE0qHMhGH9aSeUFp3llZW',	'00',	'00',	'asdasd',	'asdasd',	NULL,	NULL,	'asdasd',	NULL,	'Afghanistan',	'2342342323',	'asdasd@2131.com',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-04-08 13:53:57',	'2018-04-08 13:53:57'),
(74,	NULL,	NULL,	'wkwk10',	'$2y$10$Q0CSprBifYuedHGsKE4RyOMg/wDHmw7jSgfiRrXcTytPXMNFCpGMy',	'00',	'00',	'kakasd',	'lkasjd',	NULL,	'xczvzx',	'asdlkmjas',	'12311',	'Afghanistan',	'2222222222',	'alksjd@askdj.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	'icVpBDhEKwpOES4WlT7jWNFSnej9AwdjrG7GszCWbHaeFCGDiomCKbhHDQN5',	'2018-04-15 20:27:24',	'2018-04-15 20:28:22'),
(75,	NULL,	NULL,	'ihanimal',	'$2y$10$ZasBTthWXXGAiKO5l3enVeK0cO5u2bRZlpVmVzUds4N0QJgblyKsG',	'00',	'00',	'jorge',	'simoes',	NULL,	'asdasdas',	'asdasd',	'12312',	'Afghanistan',	'987987987',	'asdasdas@Asdasdas.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	'QFZWXZVDJ5TEG64V',	'xipzJQaknkcgQWYbnfr5eZtrz60oPiytddHZT127TNGuLMueKaW60LvJhTJn',	'2018-04-15 22:10:46',	'2018-04-29 21:47:57'),
(76,	NULL,	NULL,	'Wkwk11',	'$2y$10$Cm6smCjRd3qnzTof3eOIauhlqjkhpv01Hp70fMLhVLy71MLuf6RZa',	'00',	'00',	'Hhh',	'Hyh',	NULL,	'Gggg',	'Gg',	'3333',	'Afghanistan',	'3333333242',	'Hhh@hhh.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	'Gj9pjosqiIJwDI4UhrUksdmTSb73bvSWMY15pRL1LPMqSUrus6CCAiM9nU2S',	'2018-04-16 12:01:34',	'2018-04-16 12:02:45'),
(77,	NULL,	NULL,	'wkwk13',	'$2y$10$gwAqqLFmO.tRpy3uTDJno.B7TWak3KKpiz2UCnT5rng1oAoDsPeca',	'00',	'00',	'asd',	'asd',	NULL,	'234234',	'asxdasd',	'23423',	'Afghanistan',	'2222221212',	'asd@jkahskjhdkjasd.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	'1524010083',	'H73Ra4gz',	NULL,	NULL,	'HY5rzZ3bC34oxzGpUAfKg2GZ7M6kxxnfKoUfeniG66KTSbQ8iStGZtCIzVud',	'2018-04-18 00:07:56',	'2018-04-18 00:08:45'),
(78,	NULL,	NULL,	'wkwk14',	'$2y$10$IJL.pTMasozNnwPjmp4PDeNN2dbZvWtGrm2M5qpGcVqV1PkDh.LCK',	'00',	'00',	'qweqweqw',	'qweqwe',	NULL,	'gbghg',	'asdasdd',	'45454',	'Afghanistan',	'2222222222',	'qweqwe@qweqwe.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	'sztNeCg8Vnnsmy1066V7lLRpve35g6G6xVLY7ViL0882uFiHv97RL9hLwoAE',	'2018-04-18 00:12:00',	'2018-04-18 00:12:33'),
(79,	NULL,	NULL,	'lol1',	'$2y$10$9NgvxAuWgzkgP300zj3k7eCSgs5FBIbKbGHc90zUD2NKaNMn6iGZa',	'00',	'00',	'lol',	'lol',	NULL,	NULL,	'recoleta',	NULL,	'Argentina',	'3059958456',	'lollol20182018@outlook.es',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	'1524086018',	'vm2EUVLx',	NULL,	NULL,	NULL,	'2018-04-18 21:10:08',	'2018-04-18 21:13:38'),
(80,	NULL,	NULL,	'lol2',	'$2y$10$apnfFuEAiNzgj/eaufZP7uUMgDPGjzmLMuXp7GaJPIx6keoqVJA5S',	'00',	'00',	'lolo',	'lolo',	NULL,	NULL,	'andorra',	NULL,	'Andorra',	'3058968524',	'lollol20182018@yahoo.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	'0',	'zPpoS9Dk7n',	NULL,	NULL,	'gf0Ur15yTfRTqZynQIafALDLhNLYWCO2PeUPgHbJRU2mNlhItSU6ZrECIJcX',	'2018-04-18 21:32:08',	'2018-04-18 21:34:03'),
(81,	NULL,	NULL,	'jkjk',	'$2y$10$VvtTEPxp.NeQJnRnglIBgOvy4kT3vJPpr5dZf.IQHEfzkY0TglhjK',	'00',	'00',	'sdfsdfsd',	'fsdfsdfsdf',	NULL,	NULL,	'adasdsa',	NULL,	'Afghanistan',	'123123123',	'dasdasd@SAdfasda.com',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	'1524237054',	'z8XbyqKm',	NULL,	NULL,	NULL,	'2018-04-20 15:10:38',	'2018-04-20 15:10:54'),
(82,	NULL,	NULL,	'lala',	'$2y$10$bthqKW0.vw7CyYTmebVXS.eSXgeTyq4uOaPSLOQPzNCv3kICXqKD.',	'00',	'00',	'asfsdasd',	'asfasdasd',	NULL,	NULL,	'Santo Domingo',	NULL,	'Dominican Republic',	'8296414644',	'asdasdas@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-04-21 23:14:36',	'2018-04-21 23:14:36'),
(83,	NULL,	NULL,	'abc123',	'$2y$10$C1KFMJHLNnfuMJ.EGR9LA.nU/HoYLvFU18icSzx2eVIdq836uBukm',	'00',	'00',	'asfsdasd',	'asfasdasd',	NULL,	NULL,	'Santo Domingo',	NULL,	'Afghanistan',	'8296414644',	'jorgehsy@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	'0',	'SnIyKkifok',	NULL,	'VDM2LQN6G2TMNXD6',	'RYHwqD6UgQ2IjNN2j9KADDMoJbDNipVL5IHoNqqVCzXQW2IiEFJFs2TacGSM',	'2018-04-21 23:16:31',	'2018-04-29 17:21:14'),
(84,	NULL,	NULL,	'asdsf',	'$2y$10$hlZPhf3fwjtAZDmDXndjPO2ud1RNRWNVdl1uXmF.w3a6KTyDHbi06',	'00',	'00',	'asdasda',	'asdasda',	NULL,	NULL,	'dsfsdfs',	NULL,	'Afghanistan',	'123124123',	'ihanimal@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	'1525147617',	'fsBpJsMt',	NULL,	NULL,	NULL,	'2018-05-01 03:22:06',	'2018-05-01 04:06:57'),
(85,	NULL,	NULL,	'Nxu2018',	'$2y$10$DAWR0mQ2T3T1qBwTlDnDAOBfv93iVV4bHBFcpfg9cKTelgIex55xG',	'00',	'00',	'Guillermo',	'Gonzalez',	NULL,	NULL,	'London',	NULL,	'Afghanistan',	'6466463322',	'Wkguille@live.com',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	'1525147815',	'QWJUD13C',	NULL,	NULL,	NULL,	'2018-05-01 03:27:55',	'2018-05-01 04:10:15'),
(86,	NULL,	NULL,	'nxu123',	'$2y$10$6jpK0YYdJi9/1FsExucnpe7db2cmBSrXBtrkqtuXe8KbfMYwnUhse',	'00',	'00',	'guillermo',	'gonzalez',	NULL,	NULL,	'london',	NULL,	'United Kingdom',	'7867579240',	'wkguille@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'1',	'1',	'0',	'wyzaRE2Vew',	NULL,	'GGNTSJDLNYG4OK6P',	'GAYgKhDu7s5RR2rmGncSNEU9cA4QEVKujeUSQDN5DzOqnkWVGn51FeXbOkuy',	'2018-05-01 04:15:20',	'2018-05-01 04:36:43'),
(87,	NULL,	NULL,	'phong135',	'$2y$10$KswgF.OKxBigLtV9klxTyubmjsCL9ItGX7FC3BHwuPhCSUuZdGWQO',	'00',	'00',	'Duong',	'Dang',	NULL,	NULL,	'Ha Noi',	NULL,	'Vietnam',	'0961906728',	'duongdanghumg@gmail.com',	1,	0,	0,	0,	NULL,	NULL,	'0',	'1',	'1525201243',	'LcvVt5Sr',	NULL,	NULL,	NULL,	'2018-05-01 18:41:25',	'2018-05-01 19:00:43');

DROP TABLE IF EXISTS `uwdlogs`;
CREATE TABLE `uwdlogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `trxid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `balance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `toacc` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `charge` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `trxid` (`trxid`),
  KEY `uwdlogs_user_id_foreign` (`user_id`),
  CONSTRAINT `uwdlogs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `wdmethods`;
CREATE TABLE `wdmethods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prtime` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `minamo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `maxamo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chargefx` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chargepc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `wdmethods` (`id`, `name`, `logo`, `prtime`, `minamo`, `maxamo`, `chargefx`, `chargepc`, `status`, `created_at`, `updated_at`) VALUES
(1,	'Stripe',	'5a082cb13086d.png',	'2',	'10',	'10000',	'9',	'2',	1,	'2017-11-09 05:19:53',	'2017-11-13 11:36:12'),
(2,	'PayPall',	'5a043a2ddca83.jpg',	'1',	'50',	'50000',	'6',	'2',	1,	'2017-11-09 05:21:17',	'2017-11-09 05:21:17'),
(3,	'BitCoin',	'5a043a8a6fb93.png',	'2',	'10',	'50000',	'70',	'3',	1,	'2017-11-09 05:22:50',	'2017-11-13 11:37:43');

DROP TABLE IF EXISTS `withdraws`;
CREATE TABLE `withdraws` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wdid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `charge` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wdmethod_id` int(10) unsigned NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `withdraws_user_id_foreign` (`user_id`),
  KEY `withdraws_wdmethod_id_foreign` (`wdmethod_id`),
  CONSTRAINT `withdraws_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `withdraws_wdmethod_id_foreign` FOREIGN KEY (`wdmethod_id`) REFERENCES `wdmethods` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- 2018-05-02 01:07:51
